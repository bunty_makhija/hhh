:construction_worker: Hardhathub :construction:
=====

Dependencies & Setup
-----
- Ruby 2.1.0
- Rails 4.0.5
- Postgres

```sh
$ cp config/settings.yml.template config/settings.yml
$ cp config/database.yml.template config/database.yml
```

Settings.yml has a bunch of goodies in it, don't forget to add S3 Keys :)

External Services
-----
- [Hubspot](hubspot.com)
- LinkedIn API
- Amazon S3

This application makes heavy use of the [Hubspotter Gem](https://github.com/tablexi/hubspotter), written by our very own Dr. Gore. This gem is used to sync data with Hard Hat's blogging / inbound lead-gen service, Hubspot. Talk with Annie W. or Dr. Gore if you have any questions or need credentials for it.

Instances
-----
- Production: http://www.hardhathub.com/
- Blog: http://blog.hardhathub.com/
- Stage http://hardhathub.stage.tablexi.com/

Installing Postgres
-----

```sh
brew update
brew install postgres
```
Follow the post-install instructions to init a new DB and start it
Install the Postgres preferences pane for easy management (optional)
Create your DB. For me it was as easy as:
```sh
psql template1
CREATE DATABASE hardhathub;
GRANT ALL PRIVILEGES ON DATABASE hardhathub to <my user>;
\q
cp $PROJECT_ROOT/config/database.yml.template $PROJECT_ROOT/config/database.yml
```
Update your new database.yml
```sh
bundle install
rake db:migrate
```

That should be it, but you very well may run into an issue somewhere. In particular around step #5. I already had a user 'cstump' on my postgres and I forget how that user came to be. Homebrew very well may have set it up for me; hopefully that's the case for you.

Hubspot Integration
----
Hubspot is a data tracking utility that collects information about registered HardHat users through a browser cookie. Internally, it refers to these registered users as "Contacts". It is used by the HardHat team for a number of purposes:
- Creating and serving landing pages and forms tailored to contacts based on their user data (e.g. Engineers vs. Architects)
- Managing mass emails that are similarly tailored to contacts
- Provides analytics about how users are navigating through the site
- Allows HardHat to query the list of contacts based on various properties (How many Engineers are looking for a job in Pittsburgh?)

Since the users in the HardHat rails app should be exactly the same as the contacts in Hubspot, we have set up various syncing mechanisms between the two systems.

Syncing from Rails to Hubspot
----
Most data is directed from the rails app towards Hubspot. In order to correctly format the rails data into Hubspot data, we have created an object called HardhatContact. For example, Hubspot requires that dates be sent in the format 'mm/dd/yy', so date fields in HardhatContact will be returned in a formatted string. Another design note for HardhatContact is that its method names exactly correspond to the field names that exist in Hubspot. These field names are how we identify what fields we are sending over. That is why we have a method in HardhatContact called "referred_by_name_".

In order to actually update Hubspot, we mostly rely on its Forms API. For each rails form of interest, we set up a Form on the Hubspot account and a corresponding form model object on the rails side. A controller calls submit() on these form models to create a delayed_job that submits the field data to Hubspot. The forms in rails that will sync their data to Hubspot are:
- New Account Signup
- Login
- Professional Status
- Travel Preferences
- Schedule Preferences
- Salary Preferences
- Skills and Certifications
- Qualifications
- Work History
- Education History

Unless a Hubspot form field is marked as required, you do not have to send values for a field each time you submit to the form that it contains. Also of note is that if Hubspot does have a non-null value for a field (or Property, as Hubspot refers to it), and you send over a null value, Hubspot will NOT erase the existing value of that field. The one field that is always required is the user's email address, that is the primary key on Hubspot. If Hubspot receives a form with an email that it does not recognize, it will create a brand new contact with that email.

Syncing from Hubspot to Rails (a.k.a. Reverse Syncing)
----
Because many users come into HardHat through a landing page/form that is hosted by Hubspot, we have set up a process where people who sign up through Hubspot will automatically have an account created for them in the rails system. This is setup via a Hubspot feature called a Workflow. The steps of the process are:
1. User submits a Hubspot form
2. The Hubspot workflow is watching for newly registered contacts and when a contact comes in, it will send a POST with the contact's details to a rails endpoint that we set up at /webhook_users.
3. The WebhookUsersController creates a new user using the WebhookParser, storing only values from these selected fields: email, firstname, lastname, zip, preferred_industries, function, referred_by_name_
4. On User save, an email is sent to the new user with instructions about setting up a password and a link to the set password form. The link is valid for 6 hours, after which the user would currently have to go through the "Forgot Password?" process to set up their password.
5. The Hubspot workflow will set the Contact's property "hardhat_created_at" date to today's date.

The expectation is that once the new users have set up their passwords and logged in to rails, they will maintain their profiles and the normal, Form API-based syncing will kick in.

Special "600" Reverse Sync
----
By the time we put the Reverse syncing described above into production, we had a backlog of about 600 users that had already signed up through Hubspot but did not have a corresponding rails account. To handle this, we put the following process in place:

1. Ran a rake task hubspot:contacts:setup_reverse_sync, which generated a single-use, signed message token which contains the user's email for each of the 600 users. The task also updated the existing Hubspot contact records with the message token.
2. HardHat admins (Brad or Dustin, for example) create a series of nurturing emails that contain an "activate" url associated to the message token for each of the 600 users.
3. A "600" user receives one or more of these nurturing emails and decides at some time to click on the activate link
4. The activate link is an endpoint on the rails WebhookUsersController which will lookup the Hubspot contact associated with the given email and then create a new rails User from the contact data.
5. The same "set password" email as above will get sent out to the new user.

We didn't want to place a time limit on the message token that is used in step 2, because there could be several emails that go out to a user over a long period of time (daya, weeks, months...). But we did restrict it to single use, so if a user tries to click on the activate url again, they will see an error page.




