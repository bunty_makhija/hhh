class CreateAudienceProfile < ActiveRecord::Migration
  def change
    create_table :audience_profiles do |t|
      t.integer :audience_type_id
      t.boolean :featured, default: false
      t.string :title
      t.text :bio
      t.string :primary_image
      t.timestamps
    end

    add_index :audience_profiles, :audience_type_id
  end
end
