class AddTravelPreferences < ActiveRecord::Migration
  def change
    create_table :travel_preferences do |t|
      t.integer :candidate_profile_id,                 null: false
      t.boolean :will_travel,          default: false, null: false
      t.boolean :will_relocate,        default: false, null: false
      t.boolean :home_daily,           default: false, null: false
      t.boolean :home_weekly,          default: false, null: false
      t.boolean :home_biweekly,        default: false, null: false
      t.boolean :home_monthly,         default: false, null: false
      t.boolean :home_never,           default: false, null: false
      t.boolean :region1,              default: false, null: false
      t.boolean :region2,              default: false, null: false
      t.boolean :region3,              default: false, null: false
      t.boolean :region4,              default: false, null: false
      t.boolean :region5,              default: false, null: false
      t.boolean :region6,              default: false, null: false
      t.boolean :region7,              default: false, null: false
      t.boolean :region8,              default: false, null: false
      t.boolean :region9,              default: false, null: false
      t.boolean :region10,             default: false, null: false
      t.text    :other_concerns
    end

    add_index :travel_preferences, :candidate_profile_id
  end
end
