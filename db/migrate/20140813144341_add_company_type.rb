class AddCompanyType < ActiveRecord::Migration
  def change
    create_table :company_types do |t|
      t.integer :audience_type_id
      t.integer :company_profile_id
    end
  end
end
