class AlterProProfilesAddCompletedAt < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :completed_at, :datetime
  end
end
