class AddCategoryAndCategoryFunctionToProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :category_id, :integer, references: "categories"
    add_column :professional_profiles, :category_function_id, :integer, references: "category_functions"
  end
end
