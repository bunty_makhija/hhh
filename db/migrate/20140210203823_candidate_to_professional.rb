class CandidateToProfessional < ActiveRecord::Migration
  def change
      remove_index :candidate_profiles, column: :status
      remove_index :candidate_profiles, column: :user_id
      rename_table :candidate_profiles, :professional_profiles
      add_index    :professional_profiles, :status
      add_index    :professional_profiles, :user_id

      profile_component_tables = %i(
        engagement_preferences
        salary_preferences
        schedule_preferences
        qualifications
        travel_preferences
        work_histories
        education_histories )

      profile_component_tables.each do |table|
        remove_index  table, column: :candidate_profile_id
        rename_column table, :candidate_profile_id, :professional_profile_id
        add_index     table, :professional_profile_id
      end
  end
end
