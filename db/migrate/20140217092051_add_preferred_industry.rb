class AddPreferredIndustry < ActiveRecord::Migration
  def change
    change_table :professional_profiles do |t|
      t.boolean :civil
      t.boolean :energy
      t.boolean :building
      t.boolean :metals
      t.boolean :manufacturing
      t.boolean :anything
      t.string :industry_other
    end
  end
end
