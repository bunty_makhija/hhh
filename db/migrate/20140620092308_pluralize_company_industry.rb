class PluralizeCompanyIndustry < ActiveRecord::Migration
  def change
    rename_column :company_profiles, :industry, :industries
  end
end
