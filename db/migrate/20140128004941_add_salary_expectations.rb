class AddSalaryExpectations < ActiveRecord::Migration
  def change
    create_table :salary_preferences do |t|
      t.integer :candidate_profile_id, null: false
      t.string  :base_salary
      t.string  :hourly_rate
      t.string  :daily_travel_comp
      t.text    :other_concerns
    end

    add_index :salary_preferences, :candidate_profile_id

    User.all.each do |user|
      profile = user.candidate_profile
      profile.build_salary_preferences.save! if profile.salary_preferences.nil?
    end
  end
end
