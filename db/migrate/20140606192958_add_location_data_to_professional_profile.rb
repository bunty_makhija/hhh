class AddLocationDataToProfessionalProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :country, :string
    add_column :professional_profiles, :state, :string
    add_column :professional_profiles, :city, :string
  end
end
