class AlterCompanyProfilesAddFeaturedEmbedAddlImage < ActiveRecord::Migration
  def change
    change_table :company_profiles do |t|
      t.column :featured, :boolean
      t.column :primary_image, :string
      t.column :video_embed, :text
    end
  end
end
