class CreateAudienceTypes < ActiveRecord::Migration
  def change
    create_table :audience_types do |t|
      t.string :name
      t.text :description
      t.integer :position
      t.boolean :featured, default: false
      t.string :primary_image
      t.timestamps
    end
  end
end
