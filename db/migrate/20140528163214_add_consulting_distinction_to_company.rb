class AddConsultingDistinctionToCompany < ActiveRecord::Migration
  def change
    add_column :company_profiles, :consulting_partner, :boolean
    add_column :company_profiles, :employment_partner, :boolean
  end
end
