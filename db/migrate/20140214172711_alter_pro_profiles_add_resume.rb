class AlterProProfilesAddResume < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :resume, :string
  end
end
