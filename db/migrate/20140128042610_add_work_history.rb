class AddWorkHistory < ActiveRecord::Migration
  def change
    create_table :work_histories do |t|
      t.references :candidate_profile, null: false
      t.string :company
      t.string :industry, null:false, default: "anything"
      t.string :industry_other
      t.date :from_date
      t.date :to_date
      t.string :position
      t.text :summary
    end

    add_index :work_histories, :candidate_profile_id
  end
end
