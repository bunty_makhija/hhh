class AddSlugToAudienceTypes < ActiveRecord::Migration
  def up
    add_column :audience_types, :slug, :string
    add_index :audience_types, :slug, unique: true

    AudienceType.find_each(&:save)

    change_column :audience_types, :slug, :string, null: false
  end

  def down
    remove_index :audience_types, column: :slug
    remove_column :audience_types, :slug
  end
end
