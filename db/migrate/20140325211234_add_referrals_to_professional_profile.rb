class AddReferralsToProfessionalProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :referrals, :text
  end
end
