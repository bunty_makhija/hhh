class AddIndustryTypeToCompanyProfileAndNameToAudienceProfile < ActiveRecord::Migration
  def change
    add_column :company_profiles, :industry, :string
    add_column :audience_profiles, :name, :string
  end
end
