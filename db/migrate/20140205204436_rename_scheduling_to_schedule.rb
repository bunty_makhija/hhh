class RenameSchedulingToSchedule < ActiveRecord::Migration
  def change
    rename_table :scheduling_preferences, :schedule_preferences
  end
end
