class AddCategoryFunctions < ActiveRecord::Migration
  def change
    create_table :category_functions do |t|
      t.references :category
      t.string :name_key
    end
  end
end
