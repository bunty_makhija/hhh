class AddSlugToCompanyProfiles < ActiveRecord::Migration
  def change
    add_column :company_profiles, :slug, :string
    add_index :company_profiles, :slug, unique: true

    CompanyProfile.find_each(&:save)

    change_column :company_profiles, :slug, :string, null: false
  end
end
