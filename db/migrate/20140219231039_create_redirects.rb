class CreateRedirects < ActiveRecord::Migration
  def change
    create_table :redirects do |t|
      t.column :old_path, :string
      t.column :new_url, :string
    end
  end
end
