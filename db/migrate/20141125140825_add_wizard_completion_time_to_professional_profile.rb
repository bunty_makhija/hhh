class AddWizardCompletionTimeToProfessionalProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :wizard_completed_at, :datetime
  end
end
