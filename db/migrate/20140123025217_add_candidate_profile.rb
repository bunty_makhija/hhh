class AddCandidateProfile < ActiveRecord::Migration
  def change
    create_table :candidate_profiles do |t|
      t.integer :user_id, null: false
      t.string  :status,  null: false
    end

    add_index :candidate_profiles, :user_id
    add_index :candidate_profiles, :status
  end
end
