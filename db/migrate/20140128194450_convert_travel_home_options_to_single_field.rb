class ConvertTravelHomeOptionsToSingleField < ActiveRecord::Migration
  def up
    change_table :travel_preferences do |t|
      t.remove :home_daily
      t.remove :home_weekly
      t.remove :home_biweekly
      t.remove :home_monthly
      t.remove :home_never
      t.string :home_frequency, null: false, default: "daily"
    end
  end

  def down
    change_table :travel_preferences do |t|
      t.remove :home_frequency
      t.boolean :home_daily
      t.boolean :home_weekly
      t.boolean :home_biweekly
      t.boolean :home_monthly
      t.boolean :home_never
    end
  end
end
