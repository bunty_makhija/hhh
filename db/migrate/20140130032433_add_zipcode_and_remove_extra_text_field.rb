class AddZipcodeAndRemoveExtraTextField < ActiveRecord::Migration
  def change
    remove_column :engagement_preferences, :term_notes
    add_column :candidate_profiles, :zip_code, :string
  end
end
