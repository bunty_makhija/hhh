class AddPositionToCategoryFunctions < ActiveRecord::Migration
  def change
    add_column :category_functions, :position, :integer
  end
end
