class ChangeAudienceTypeFeaturedToPublished < ActiveRecord::Migration
  def change
    rename_column :audience_types, :featured, :published
  end
end
