class AddReverseSyncTokens < ActiveRecord::Migration
  def change
    create_table :reverse_sync_tokens do |t|
      t.string :token_digest
      t.datetime :retrieved_at
      t.timestamps
    end
  end
end
