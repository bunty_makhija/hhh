class AddAssociationsToProfessionalProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :associated_with, :text
  end
end
