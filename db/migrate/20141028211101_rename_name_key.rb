class RenameNameKey < ActiveRecord::Migration
  def change
    rename_column :categories, :name_key, :name
    rename_column :category_functions, :name_key, :name
  end
end
