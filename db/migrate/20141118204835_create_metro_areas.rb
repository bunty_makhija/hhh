class CreateMetroAreas < ActiveRecord::Migration
  def change
    create_table :metro_areas do |t|
      t.string :area_name
      t.string :city
      t.string :state

      t.timestamps
    end
  end
end
