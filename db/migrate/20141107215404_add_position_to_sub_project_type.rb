class AddPositionToSubProjectType < ActiveRecord::Migration
  def change
    add_column :sub_project_types, :position, :integer
  end
end
