class AddJoinTableForProjectTypes < ActiveRecord::Migration
  def change
    create_join_table :professional_profiles, :project_types do |t|
      t.index :professional_profile_id, name: 'index_profiles_project_types_on_profile_id'
    end
  end
end
