class AddTimestamps < ActiveRecord::Migration
  def change
    tables = %i(
      candidate_profiles
      engagement_preferences
      schedule_preferences
      salary_preferences
      travel_preferences
      work_histories
      education_histories
      company_profiles
      job_opportunities )

    tables.each { |name| add_timestamps name }
  end
end
