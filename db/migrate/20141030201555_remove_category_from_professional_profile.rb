class RemoveCategoryFromProfessionalProfile < ActiveRecord::Migration
  def change
    remove_column :professional_profiles, :category_id
    remove_column :professional_profiles, :category_function_id
  end
end
