class AddProfileFields < ActiveRecord::Migration
  def change
    add_column :engagement_preferences, :term_various, :boolean
    add_column :education_histories, :institution, :string
    add_column :work_histories, :current, :boolean
  end
end
