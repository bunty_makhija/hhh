class AlterCompanyProfilesAddLogoKeyFactsAddlInfo < ActiveRecord::Migration
  def change
    change_table :company_profiles do |t|
      t.column :logo, :string
      t.column :key_facts, :text
      t.column :additional_info, :text
    end
  end
end
