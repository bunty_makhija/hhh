class AddSchedulingPreferences < ActiveRecord::Migration
  def change
    create_table :scheduling_preferences do |t|
      t.integer :candidate_profile_id,                 null: false
      t.boolean :full_time,            default: false, null: false
      t.boolean :part_time,            default: false, null: false
      t.boolean :contract,             default: false, null: false
      t.text    :other_concerns
    end

    add_index :scheduling_preferences, :candidate_profile_id

    User.all.each do |user|
      profile = user.candidate_profile
      profile.build_schedule_preference.save! if profile.schedule_preference.nil?
    end
  end
end
