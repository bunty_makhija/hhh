class AddSubProjectTypes < ActiveRecord::Migration
  def change
    create_table :sub_project_types do |t|
      t.references :project_type
      t.string :name
      t.string :description
    end
  end
end
