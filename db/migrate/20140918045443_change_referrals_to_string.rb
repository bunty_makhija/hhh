class ChangeReferralsToString < ActiveRecord::Migration
  def change
    change_column :professional_profiles, :referrals, :string
  end
end
