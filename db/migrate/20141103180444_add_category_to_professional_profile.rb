class AddCategoryToProfessionalProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :category_id, :integer, references: "categories"
  end
end
