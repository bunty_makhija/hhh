class AddHubspotResumeUrlToProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :hubspot_resume_url, :string
  end
end
