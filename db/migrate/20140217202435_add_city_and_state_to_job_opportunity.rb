class AddCityAndStateToJobOpportunity < ActiveRecord::Migration
  def change
    add_column :job_opportunities, :city, :string
    add_column :job_opportunities, :state, :string
  end
end
