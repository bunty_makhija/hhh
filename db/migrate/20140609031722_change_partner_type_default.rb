class ChangePartnerTypeDefault < ActiveRecord::Migration
  def change
    change_column_default :company_profiles, :consulting_partner, false
    change_column_default :company_profiles, :employment_partner, false
  end
end
