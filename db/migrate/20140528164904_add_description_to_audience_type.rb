class AddDescriptionToAudienceType < ActiveRecord::Migration
  def change
    add_column :audience_types, :more_about, :text
  end
end
