class AddPublishedToAudienceProfileAndFixDefaults < ActiveRecord::Migration
  def change
    add_column :audience_profiles, :published, :boolean, default: false
    CompanyProfile.where(consulting_partner: nil).update_all(consulting_partner: false)
    CompanyProfile.where(employment_partner: nil).update_all(employment_partner: false)
    change_column_null :company_profiles, :consulting_partner, false
    change_column_null :company_profiles, :employment_partner, false
  end
end
