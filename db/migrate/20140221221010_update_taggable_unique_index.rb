class UpdateTaggableUniqueIndex < ActiveRecord::Migration
  def change
    remove_index "tags", [:name]
    add_index "tags", [:name, :original_context], unique: true
  end
end
