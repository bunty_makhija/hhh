class RemoveColumnsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :linkedin_token, :string
    remove_column :users, :linkedin_secret, :string
  end
end
