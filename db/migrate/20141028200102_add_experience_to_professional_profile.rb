class AddExperienceToProfessionalProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :experience, :string
    add_column :professional_profiles, :authorization, :boolean
    add_column :professional_profiles, :authorization_details, :text
  end
end
