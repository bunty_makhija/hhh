class AddJobOpportunities < ActiveRecord::Migration
  def change
    create_table :job_opportunities do |t|
      t.references  :company_profile
      t.string      :title,           null:false
      t.string      :industry
      t.string      :industry_other
      t.string      :schedule
      t.text        :job_description, null: false
      t.string      :status
    end

    add_index :job_opportunities, :status

    add_column :scheduling_preferences, :internship, :boolean
  end
end
