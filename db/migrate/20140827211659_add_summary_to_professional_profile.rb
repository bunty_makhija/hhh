class AddSummaryToProfessionalProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :summary, :text
  end
end
