class UpdateNameAndDescriptionToKeyAndDisplayName < ActiveRecord::Migration
  def change
    rename_column :categories,         :name,        :key
    rename_column :categories,         :description, :display_name
    rename_column :category_functions, :name,        :key
    rename_column :category_functions, :description, :display_name
    rename_column :project_types,      :name,        :key
    rename_column :project_types,      :description, :display_name
    rename_column :sub_project_types,  :name,        :key
    rename_column :sub_project_types,  :description, :display_name
  end
end
