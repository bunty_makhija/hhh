class CreateQualifications < ActiveRecord::Migration
  def change
    create_table :qualifications do |t|
      t.references :candidate_profile, index: true, null: false
      t.text :additional_skills
      t.text :additional_certs
      t.timestamps
    end
  end
end
