class AddDescriptionToCategoryFunction < ActiveRecord::Migration
  def change
    add_column :category_functions, :description, :string
  end
end
