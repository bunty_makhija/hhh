class AddDesiredRolePreferenceToProfessionalProfile < ActiveRecord::Migration
  def change
    create_table :role_preferences do |t|
      t.references :professional_profile, index: true, null: false
      t.references :audience_type, index: true, null: true
      t.timestamps
    end

    RolePreference.reset_column_information
    ProfessionalProfile.find_each do |profile|
      profile.create_role_preference unless profile.role_preference
    end
  end
end
