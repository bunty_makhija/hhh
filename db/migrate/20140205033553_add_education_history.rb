class AddEducationHistory < ActiveRecord::Migration
  def change
    create_table :education_histories do |t|
      t.references :candidate_profile, null: false
      t.string :achievement_level
      t.date :graduation_date
    end

    add_index :education_histories, :candidate_profile_id
  end
end
