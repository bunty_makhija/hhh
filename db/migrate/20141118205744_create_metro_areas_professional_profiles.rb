class CreateMetroAreasProfessionalProfiles < ActiveRecord::Migration
  def change
    create_join_table :metro_areas, :professional_profiles do |t|
      t.index :metro_area_id
    end
  end
end
