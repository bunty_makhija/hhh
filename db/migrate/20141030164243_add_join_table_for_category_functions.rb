class AddJoinTableForCategoryFunctions < ActiveRecord::Migration
  def change
    create_join_table :professional_profiles, :category_functions do |t|
      t.index :professional_profile_id, name: 'index_profiles_category_functions_on_profile_id'
    end
  end
end
