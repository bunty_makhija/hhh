class AddCompanyProfiles < ActiveRecord::Migration
  def change
    create_table :company_profiles do |t|
      t.string :company_name, null: false
      t.string :contact_name
      t.string :email,        null: false
      t.text   :about
    end
  end
end
