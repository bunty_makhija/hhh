class MigrateIndustryTypes < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :infrastructure, :boolean
    add_column :professional_profiles, :petroleum, :boolean

    ProfessionalProfile.where(civil: true).update_all(infrastructure: true)
    ProfessionalProfile.where(metals: true).update_all(petroleum: true)
    ProfessionalProfile.where(manufacturing: true).update_all(petroleum: true)

    [WorkHistory, JobOpportunity, CompanyProfile].each do |model|
      model.where(industry: :civil).update_all(industry: :infrastructure)
      model.where(industry: :metals).update_all(industry: :petroleum)
      model.where(industry: :manufacturing).update_all(industry: :petroleum)
    end

    remove_column :professional_profiles, :metals
    remove_column :professional_profiles, :manufacturing
  end
end
