class AddResumeOriginalFilenameToProfile < ActiveRecord::Migration
  def change
    add_column :professional_profiles, :resume_original_filename, :string
  end
end
