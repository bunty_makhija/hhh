class AddEngagementPreferences < ActiveRecord::Migration
  def change
    create_table :engagement_preferences do |t|
      t.integer :candidate_profile_id,                 null: false
      t.date    :starting_date
      t.boolean :term_quick,           default: false, null: false
      t.boolean :term_small,           default: false, null: false
      t.boolean :term_large,           default: false, null: false
      t.boolean :term_permanent,       default: false, null: false
      t.text    :term_notes
      t.text    :other_concerns
    end

    add_index :engagement_preferences, :candidate_profile_id
  end
end
