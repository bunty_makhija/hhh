class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    add_column :users, :linkedin_token, :string
    add_column :users, :linkedin_secret, :string
  end
end
