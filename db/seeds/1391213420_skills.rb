Dir[ File.expand_path('data/*skills.txt', __dir__) ].each do |file|
  skills = File.readlines file

  skills.each do |skill|
    params = { name: skill.strip, original_context: ActsAsTaggableOn::Tag::PREDEFINED_CONTEXTS[:skill] }
    ActsAsTaggableOn::Tag.where(params).first || ActsAsTaggableOn::Tag.create!(params)
  end
end
