roles = {
  profile_manager: %w(williamscj@me.com ahastings@hardhat.com klacy@hardhathub.com),
  profile_viewer: %w(chetan@hardhathub.com rvetter@hardhathub.com),
  admin: %w(
    mattgray645@gmail.com
    ajansen@hardhathub.com
    bkreiger@hardhathub.com
    kjacobson@hardhathub.com
    chris@tablexi.com
    matt.wagner@tablexi.com
    jon.fernandez@tablexi.com
    buckles@tablexi.com
  )
}

roles.each do |role, users|
  users.each do |email|
    usr = User.find_by_email email
    usr.add_role role unless usr.has_role?(role)
  end
end
