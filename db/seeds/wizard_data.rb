require 'csv'

category_csv = CSV.read(Rails.root.join('db/seeds/data/categories_and_functions.csv'), headers: true)
category_csv["category"].compact.each do |category|
  Category.where(key: category.tr("\s&/\s", "_").downcase.squeeze("_")).first_or_create(display_name: category, position: 100)
end

category_csv.each do |row|
  if row["category"].present?
    @category = Category.find_by(display_name: row["category"])
  end
  CategoryFunction.where(key: row["key"], category: @category).first_or_create(display_name: row["display name"], position: 100)
end

project_csv = CSV.read(Rails.root.join('db/seeds/data/project_sub_projects.csv'), headers: true)
project_csv["project type"].compact.each do |project_type|
  proj = ProjectType
  ProjectType.where(key: project_type.tr("\s&/\s", "_").downcase.squeeze("_")).first_or_create(display_name: project_type, position: 100)
end

project_csv.each do |row|
  if row["project type"].present?
    @project_type = ProjectType.find_by(display_name: row["project type"])
  end
  SubProjectType.where(key: row["sub key"], project_type: @project_type).first_or_create(display_name: row["sub project type"], position: 100)
end

metro_area_csv = CSV.read(Rails.root.join('db/seeds/data/metro_areas.csv'), headers: true)

metro_area_csv.each do |row|
  metro = MetroArea.where(area_name: row["area_name"])
    .first_or_create(city: row["city"], state: row["state"])
end
