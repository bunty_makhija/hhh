concentrations = <<-EOS
Accounting
Aerospace Engineering
Architectural Technology
Architecture
Biomedical Engineering
Chemical Engineering
Civil Engineering
Computer Engineering
Computer Science
Construction Management
Electrical Engineering
General Engineering
Geography
Environmental Engineering
Environmental Science
Finance
General Business
Geology
Mechanical Engineering
Safety/Occupational Health
Structural Engineering
Other
EOS

concentrations.split("\n").each do |name|
  params = { name: name, original_context: ActsAsTaggableOn::Tag::PREDEFINED_CONTEXTS[:concentration] }
  ActsAsTaggableOn::Tag.where(params).first || ActsAsTaggableOn::Tag.create!(params)
end
