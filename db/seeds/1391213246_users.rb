people = [
  User.new(email: 'mattgray645@gmail.com', first_name: 'Matt', last_name: 'Gray'),
  User.new(email: 'ajansen@hardhathub.com', first_name: 'Andy', last_name: 'Jansen'),
  User.new(email: 'bkreiger@hardhathub.com', first_name: 'Brad', last_name: 'Kreiger'),
  User.new(email: 'williamscj@me.com', first_name: 'Conor', last_name: 'Williams'),
  User.new(email: 'ahastings@hardhat.com', first_name: 'Allie', last_name: 'Hastings'),
  User.new(email: 'kjacobson@hardhathub.com', first_name: 'Kyle', last_name: 'Jacobson'),
  User.new(email: 'klacy@hardhathub.com', first_name: 'Kyle', last_name: 'Lacy'),
  User.new(email: 'chetan@hardhathub.com', first_name: 'Chetan', last_name: 'Kundavaram'),
  User.new(email: 'rvetter@hardhathub.com', first_name: 'Ricky', last_name: 'Vetter'),
  User.new(email: 'chris@tablexi.com', first_name: 'Chris', last_name: 'Stump'),
  User.new(email: 'matt.wagner@tablexi.com', first_name: 'Matt', last_name: 'Wagner'),
  User.new(email: 'jon.fernandez@tablexi.com', first_name: 'Jon', last_name: 'Fernandez'),
  User.new(email: 'buckles@tablexi.com', first_name: 'Matt', last_name: 'Reich'),
  User.new(email: 'test@tablexi.com', first_name: 'Regular', last_name: 'User')
]

people.each do |person|
  next if User.find_by_email(person.email)

  person.password = 'Changem3'
  person.password_confirmation = 'Changem3'
  person.confirm!
end
