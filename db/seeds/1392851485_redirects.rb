require 'csv'

CSV.foreach(Rails.root.join('db/seeds/data/redirects.csv'), {headers: :first_row}) do |row|
  Redirect.find_or_create_by! old_path: row[0], new_url: row[1]
end
