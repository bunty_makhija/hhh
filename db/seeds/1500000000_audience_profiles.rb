require 'json'

audience_profiles = JSON.parse(File.read('db/seeds/data/audience_profiles.json'))

audience_profiles.each do |audience_profile|
  result = AudienceProfile.where(name: audience_profile['name']).first_or_create!(audience_profile)
end
