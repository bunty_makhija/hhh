require 'json'

audience_types = JSON.parse(File.read('db/seeds/data/audience_types.json'))

audience_types.each do |audience_type|
  result = AudienceType.where(slug: audience_type['slug']).first_or_create!(audience_type)
end
