Dir[ File.expand_path('data/*certs.txt', __dir__) ].each do |file|
  certs = File.readlines file

  certs.each do |cert|
    params = { name: cert.strip, original_context: ActsAsTaggableOn::Tag::PREDEFINED_CONTEXTS[:cert] }
    ActsAsTaggableOn::Tag.where(params).first || ActsAsTaggableOn::Tag.create!(params)
  end
end
