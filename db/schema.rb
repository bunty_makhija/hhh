# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141125140825) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "audience_profiles", force: true do |t|
    t.integer  "audience_type_id"
    t.boolean  "featured",         default: false
    t.string   "title"
    t.text     "bio"
    t.string   "primary_image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "published",        default: false
    t.string   "name"
  end

  add_index "audience_profiles", ["audience_type_id"], name: "index_audience_profiles_on_audience_type_id", using: :btree

  create_table "audience_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "position"
    t.boolean  "published",     default: false
    t.string   "primary_image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "more_about"
    t.string   "slug",                          null: false
  end

  add_index "audience_types", ["slug"], name: "index_audience_types_on_slug", unique: true, using: :btree

  create_table "categories", force: true do |t|
    t.string  "key"
    t.string  "display_name"
    t.integer "position"
  end

  create_table "category_functions", force: true do |t|
    t.integer "category_id"
    t.string  "key"
    t.string  "display_name"
    t.integer "position"
  end

  create_table "category_functions_professional_profiles", id: false, force: true do |t|
    t.integer "professional_profile_id", null: false
    t.integer "category_function_id",    null: false
  end

  add_index "category_functions_professional_profiles", ["professional_profile_id"], name: "index_profiles_category_functions_on_profile_id", using: :btree

  create_table "company_profiles", force: true do |t|
    t.string   "company_name",                       null: false
    t.string   "contact_name"
    t.string   "email",                              null: false
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo"
    t.text     "key_facts"
    t.text     "additional_info"
    t.boolean  "featured"
    t.string   "primary_image"
    t.text     "video_embed"
    t.string   "slug",                               null: false
    t.boolean  "consulting_partner", default: false, null: false
    t.boolean  "employment_partner", default: false, null: false
    t.string   "industries"
  end

  add_index "company_profiles", ["slug"], name: "index_company_profiles_on_slug", unique: true, using: :btree

  create_table "company_types", force: true do |t|
    t.integer "audience_type_id"
    t.integer "company_profile_id"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "education_histories", force: true do |t|
    t.integer  "professional_profile_id", null: false
    t.string   "achievement_level"
    t.date     "graduation_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "institution"
  end

  add_index "education_histories", ["professional_profile_id"], name: "index_education_histories_on_professional_profile_id", using: :btree

  create_table "engagement_preferences", force: true do |t|
    t.integer  "professional_profile_id",                 null: false
    t.date     "starting_date"
    t.boolean  "term_quick",              default: false, null: false
    t.boolean  "term_small",              default: false, null: false
    t.boolean  "term_large",              default: false, null: false
    t.boolean  "term_permanent",          default: false, null: false
    t.text     "other_concerns"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "term_various"
  end

  add_index "engagement_preferences", ["professional_profile_id"], name: "index_engagement_preferences_on_professional_profile_id", using: :btree

  create_table "images", force: true do |t|
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.string   "attachment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "job_opportunities", force: true do |t|
    t.integer  "company_profile_id"
    t.string   "title",              null: false
    t.string   "industry"
    t.string   "industry_other"
    t.string   "schedule"
    t.text     "job_description",    null: false
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
    t.string   "state"
  end

  add_index "job_opportunities", ["status"], name: "index_job_opportunities_on_status", using: :btree

  create_table "metro_areas", force: true do |t|
    t.string   "area_name"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "metro_areas_professional_profiles", id: false, force: true do |t|
    t.integer "metro_area_id",           null: false
    t.integer "professional_profile_id", null: false
  end

  add_index "metro_areas_professional_profiles", ["metro_area_id"], name: "index_metro_areas_professional_profiles_on_metro_area_id", using: :btree

  create_table "professional_profiles", force: true do |t|
    t.integer  "user_id",                  null: false
    t.string   "status",                   null: false
    t.string   "zip_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "resume"
    t.boolean  "civil"
    t.boolean  "energy"
    t.boolean  "building"
    t.boolean  "anything"
    t.string   "industry_other"
    t.datetime "completed_at"
    t.text     "associated_with"
    t.string   "referrals"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.boolean  "infrastructure"
    t.boolean  "petroleum"
    t.text     "summary"
    t.string   "hubspot_resume_url"
    t.string   "resume_original_filename"
    t.string   "experience"
    t.boolean  "authorization"
    t.text     "authorization_details"
    t.integer  "category_id"
    t.datetime "wizard_completed_at"
  end

  add_index "professional_profiles", ["status"], name: "index_professional_profiles_on_status", using: :btree
  add_index "professional_profiles", ["user_id"], name: "index_professional_profiles_on_user_id", using: :btree

  create_table "professional_profiles_project_types", id: false, force: true do |t|
    t.integer "professional_profile_id", null: false
    t.integer "project_type_id",         null: false
  end

  add_index "professional_profiles_project_types", ["professional_profile_id"], name: "index_profiles_project_types_on_profile_id", using: :btree

  create_table "professional_profiles_sub_project_types", id: false, force: true do |t|
    t.integer "professional_profile_id", null: false
    t.integer "sub_project_type_id",     null: false
  end

  add_index "professional_profiles_sub_project_types", ["professional_profile_id"], name: "index_profiles_sub_project_types_on_profile_id", using: :btree

  create_table "project_types", force: true do |t|
    t.string  "key"
    t.string  "display_name"
    t.integer "position"
  end

  create_table "qualifications", force: true do |t|
    t.integer  "professional_profile_id", null: false
    t.text     "additional_skills"
    t.text     "additional_certs"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "qualifications", ["professional_profile_id"], name: "index_qualifications_on_professional_profile_id", using: :btree

  create_table "redirects", force: true do |t|
    t.string "old_path"
    t.string "new_url"
  end

  create_table "reverse_sync_tokens", force: true do |t|
    t.string   "token_digest"
    t.datetime "retrieved_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "role_preferences", force: true do |t|
    t.integer  "professional_profile_id", null: false
    t.integer  "audience_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "role_preferences", ["audience_type_id"], name: "index_role_preferences_on_audience_type_id", using: :btree
  add_index "role_preferences", ["professional_profile_id"], name: "index_role_preferences_on_professional_profile_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "salary_preferences", force: true do |t|
    t.integer  "professional_profile_id", null: false
    t.string   "base_salary"
    t.string   "hourly_rate"
    t.string   "daily_travel_comp"
    t.text     "other_concerns"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "salary_preferences", ["professional_profile_id"], name: "index_salary_preferences_on_professional_profile_id", using: :btree

  create_table "schedule_preferences", force: true do |t|
    t.integer  "professional_profile_id",                 null: false
    t.boolean  "full_time",               default: false, null: false
    t.boolean  "part_time",               default: false, null: false
    t.boolean  "contract",                default: false, null: false
    t.text     "other_concerns"
    t.boolean  "internship"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "schedule_preferences", ["professional_profile_id"], name: "index_schedule_preferences_on_professional_profile_id", using: :btree

  create_table "sub_project_types", force: true do |t|
    t.integer "project_type_id"
    t.string  "key"
    t.string  "display_name"
    t.integer "position"
  end

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree

  create_table "tags", force: true do |t|
    t.string "name"
    t.string "original_context", limit: 128
  end

  add_index "tags", ["name", "original_context"], name: "index_tags_on_name_and_original_context", unique: true, using: :btree

  create_table "travel_preferences", force: true do |t|
    t.integer  "professional_profile_id",                   null: false
    t.boolean  "will_travel",             default: false,   null: false
    t.boolean  "will_relocate",           default: false,   null: false
    t.boolean  "region1",                 default: false,   null: false
    t.boolean  "region2",                 default: false,   null: false
    t.boolean  "region3",                 default: false,   null: false
    t.boolean  "region4",                 default: false,   null: false
    t.boolean  "region5",                 default: false,   null: false
    t.boolean  "region6",                 default: false,   null: false
    t.boolean  "region7",                 default: false,   null: false
    t.boolean  "region8",                 default: false,   null: false
    t.boolean  "region9",                 default: false,   null: false
    t.boolean  "region10",                default: false,   null: false
    t.text     "other_concerns"
    t.string   "home_frequency",          default: "daily", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "travel_preferences", ["professional_profile_id"], name: "index_travel_preferences_on_professional_profile_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name",                          null: false
    t.string   "last_name",                           null: false
    t.string   "phone_number"
    t.string   "provider"
    t.string   "uid"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "work_histories", force: true do |t|
    t.integer  "professional_profile_id",                      null: false
    t.string   "company"
    t.string   "industry",                default: "anything", null: false
    t.string   "industry_other"
    t.date     "from_date"
    t.date     "to_date"
    t.string   "position"
    t.text     "summary"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "current"
  end

  add_index "work_histories", ["professional_profile_id"], name: "index_work_histories_on_professional_profile_id", using: :btree

end
