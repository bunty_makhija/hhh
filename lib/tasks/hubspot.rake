# Notes on csv_export task:
## Generates a CSV of all the contacts in the db with their full profiles.
## The last time this ran (2014-08-05) the file it created had over 20,000
## lines and was too big to import all at once to hubspot.  We had to split
## the file in half and import each half separately.  We don't anticipate
## doing this again, but it's something to know about if this task gets used
## again.

require File.expand_path('app/helpers/industry_helper')
require File.expand_path('lib/hubspot_import/contacts_file')
require File.expand_path('lib/hubspot_import/hubspot_client')

namespace :hubspot do
  namespace :contacts do

    desc "generate csv of contacts to import manually into hubspot"
    task csv_export: :environment do
      target_file = Rails.root.join("tmp", "contacts-#{Time.now.to_i}.csv")
      HubspotImport::ContactsFile.generate(target_file)
      puts "Generated csv to #{target_file}"
    end

    desc "setup reverse sync token for csv list of emails"
    task :setup_reverse_sync, [:filename] => [:environment] do |t, args|
      logger = File.new(Rails.root.join("log/rake_hubspot_contacts_setup_reverse_sync.log"), "w")
      File.open(args[:filename], 'r') do |f|
        f.each_line do |email|
          logger.puts("====== generating for #{email}")
          token = HubspotterInterface::ReverseSyncToken.generate(email.strip!)
          body =
            JSON.generate({ "properties" =>
              [{"property" => "hardhat_reset_token",
                "value" => URI.escape(token)}]
            })
          HubspotImport::HubspotClient.update_contact(email, body, logger)
        end
      end
      logger.close
    end
  end
end
