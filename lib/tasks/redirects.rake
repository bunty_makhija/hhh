namespace :hhh do
  namespace :redirects do

    desc "dumps all known redirects and reseeds them"
    task reseed: :environment do
      Redirect.destroy_all
      require File.expand_path("../../db/seeds/1392851485_redirects.rb", __dir__)
    end


    desc 'takes the CSV output of Integrity spider app and makes a new CSV of old_url,new_url'
    task :create_redirects, [:input_filename] => :environment do |t, args|
      require 'csv'

      input_file = args.input_filename
      output_file = File.join File.dirname(input_file), "new_#{File.basename(input_file)}"

      CSV.open(output_file, 'w') do |new_csv|
        new_csv << %w(old_url new_url)

        CSV.foreach(input_file).each do |row|
          old_url = row[1]
          new_csv << [ old_url_to_path(old_url), new_url_from(old_url) ] unless undesirable_url? old_url
        end
      end
    end


    def undesirable_url?(url)
      url !~ /\Ahttps:\/\/hardhathub.com/ || url.include?('wp-login.php')
    end


    def new_url_from(old_url)
      trim_url old_url.gsub('hardhathub.com', 'blog.hardhathub.com')
    end


    def old_url_to_path(old_url)
      trim_url old_url.gsub('https://hardhathub.com', '')
    end


    def trim_url(url)
      url = url[0..-2] if url.ends_with? '/'
      url
    end

  end
end
