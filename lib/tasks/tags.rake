namespace :hhh do
  namespace :tags do

    namespace :reseed do

      desc "dumps all known skills and reseeds them"
      task skills: :environment do
        ActsAsTaggableOn::Tag.skills.destroy_all
        require File.expand_path("../../db/seeds/1391213420_skills.rb", __dir__)
      end

      desc "dumps all known certs and reseeds them"
      task certs: :environment do
        ActsAsTaggableOn::Tag.certs.destroy_all
        require File.expand_path("../../db/seeds/1391213408_certs.rb", __dir__)
      end

    end

  end
end
