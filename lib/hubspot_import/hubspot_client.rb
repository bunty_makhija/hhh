module HubspotImport
  class HubspotClient
    def self.update_contact(email, properties_json, logger)
      update_contact_path = Hubspotter::Contact::BASE_PATH + \
                            URI::encode("/contact/createOrUpdate/email/#{email}")
      request = Hubspotter::Request.new(update_contact_path, :post, post_body: properties_json)
      response = request.send
      logger.puts response.to_yaml

      rescue Exception=>e
        logger.puts "Failed to update reset_token for #{email}"
        logger.puts "\t#{e.message}\n#{e.backtrace.join("\n")}"
    end

    def self.fetch_contact_with_email!(email)
      fetch_contact_path = Hubspotter::Contact::BASE_PATH + \
                            URI::encode("/contact/email/#{email}/profile")
      request = Hubspotter::Request.new(fetch_contact_path, :get)
      request.send.raw
    end

    def self.fetch_contact_with_cookie!(hutk_cookie)
      fetch_contact_path = Hubspotter::Contact::BASE_PATH + \
                            URI::encode("/contact/utk/#{hutk_cookie}/profile")
      request = Hubspotter::Request.new(fetch_contact_path, :get)
      request.send.raw
    end
  end
end
