require 'csv'

module HubspotImport
  class HubspotContactListIterator
    def initialize(filename)
      @csv_data = CSV.read(filename)
      header = @csv_data.shift
      find_columns(header)
    end

    def has_next?
      !@csv_data.empty?
    end

    def next!
      return nil if @csv_data.empty?
      line = @csv_data.shift
      properties = {"properties" => {
        "email" => {"value"=>line[@email]},
        "firstname" => {"value"=>line[@firstname].capitalize},
        "lastname" => {"value"=>line[@lastname].capitalize},
        "preferred_industries" => {"value"=>line[@preferred_industries]},
        "function" => {"value"=>line[@function]},
        "referred_by_name_" => {"value"=>line[@referred].try(:capitalize)}
      }}
      JSON.generate(properties)
    end

    private

    def find_columns(header)
      @email = header.index("Email")
      @firstname = header.index("First Name")
      @lastname = header.index("Last Name")
      @function = header.index("Function")
      @referred = header.index("Referred By (Name)")
      @preferred_industries = header.index("Preferred Industries")
    end
  end
end
