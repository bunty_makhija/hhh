require 'csv'

module HubspotImport
  class ContactsFile
    def self.generate(filename)
      FileUtils.touch(filename)

      CSV.open(filename, "wb") do |csv|
        csv <<
        ["hardhat_created_at", "hardhat_updated_at",
          "email", "firstname", "lastname", "phone_number",
          "postal_code", "country", "state", "city", "industry", "preferred_industries",
            "professional_status",
          "starting_date", "engagement_term", "availability_concerns",
          "function",
          "will_travel", "home_frequency", "will_relocate", "regions", "travel_concerns",
          "work_type", "schedule_concerns",
          "base_salary", "hourly_rate", "travel_per_diem", "salary_concerns",
          "skills", "additional_skills", "certifications", "additional_certifications",
          "work_history", "education_history"
        ]

        User.find_each do |user|
          csv << ContactRow.new(user).to_a
        end
      end
    end
  end

  class ContactRow
    attr_reader :contact, :user

    def initialize(user)
      @contact = ::HubspotterInterface::HardhatContact.new(user)
      @user = contact.user
    end

    def to_a
      row = hardhat_dates
      row.concat(user_data)
      row.concat(profile_data)
      row.concat(engagement_data)
      row.concat(role_data)
      row.concat(travel_data)
      row.concat(schedule_data)
      row.concat(salary_data)
      row.concat(skills_certifications_data)
      row.concat(history_data)
    end

    private

    def hardhat_dates
      [contact.hardhat_created_at, contact.hardhat_updated_at]
    end

    def user_data
      [user.email,
        user.first_name,
        user.last_name,
        user.phone_number
      ]
    end

    def profile_data
      profile = user.professional_profile
      [profile.zip_code,
        profile.country,
        profile.state,
        profile.city,
        profile.industry_other,
        contact.preferred_industries.join(","),
        profile.status
      ]
    end

    def engagement_data
      data = user.professional_profile.engagement_preference
      [contact.starting_date,
        contact.engagement_term.join(","),
        data.other_concerns
      ]
    end

    def role_data
      data = user.professional_profile.role_preference
      [data.desired_role.try(:name)]
    end

    def travel_data
      data = user.professional_profile.travel_preference
      [data.will_travel,
        data.home_frequency,
        data.will_relocate,
        contact.regions.join(","),
        data.other_concerns
      ]
    end

    def schedule_data
      data = user.professional_profile.schedule_preference
      [contact.work_type.join(","), data.other_concerns]
    end

    def salary_data
      data = user.professional_profile.salary_preference
      [data.base_salary,
        data.hourly_rate,
        data.daily_travel_comp,
        data.other_concerns
      ]
    end

    def skills_certifications_data
      data = user.professional_profile.qualification
      [contact.skills,
        data.additional_skills,
        contact.certifications,
        data.additional_certs
      ]
    end

    def history_data
      [contact.work_history, contact.education_history]
    end
  end
end
