class SeedGenerator < Rails::Generators::Base
  argument :file_name, type: :string, default: 'seed'

  def create_seed_file
    create_file File.join(Rails.root, 'db', 'seeds', "#{Time.now.to_i}_#{file_name}.rb")
  end
end
