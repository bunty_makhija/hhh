# This file is for IE8 specific scripts that need to load in the page head.

# VENDOR FOLDER - Comments and alterations documented at
# https://github.com/tablexi/gradesaver-website/wiki/Javascript-Overview
#
#= require html5shiv

#= require_self
