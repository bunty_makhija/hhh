$(document).ready ->

  $(".item__more").on "click", ->
    $(this).parent().toggleClass("hide")
    $(this).parent().siblings(".more").toggleClass("hide")

  $(".item__less").on "click", ->
    $(this).parent().toggleClass("hide")
    $(this).parent().siblings(".less").toggleClass("hide")
