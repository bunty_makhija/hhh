class window.ButtonWorkingAnimation  
  ellipsisCount: 0

  constructor: (@button) ->
    @button.click @perform
    @buttonText = @button.find(".button__text")

  perform: =>
    @addWorkingText()
    setInterval(@addEllipsis, 300)

  addEllipsis: =>
    @ellipsisCount++
    if @ellipsisCount <= 3
      @buttonText.append(".")

  addWorkingText: =>
    @buttonText.width(@buttonText.width())
    @buttonText.css("text-align", "center")
    @buttonText.text("Working")

$ ->
  new ButtonWorkingAnimation($('.js--buttonWorking'))
