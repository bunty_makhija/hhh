$ ->

  if $('.work_history_current').length
    closestToDate = ($check)->
      $check.closest('section').find('.work_history_to_date select')


    disableToDate = ($check)->
      disabled = true

      closestToDate($check).each ->
        disabled &= !$(this).is(':disabled')
        $(this).prop 'disabled', disabled

      disabled


    setToDateToNow = ($check)->
      now = new Date()

      closestToDate($check).each ->
        $select = $(this)
        optionSelector = "option[value=#{now.getMonth()}]"
        optionSelector = "option[value=#{now.getFullYear()}]" if $select.val().match(/\d{4}/)
        $select.children(optionSelector).prop 'selected', true


    $check = $('.js-current-history')
    disableToDate() if $check.is ':checked'
    $check.click -> setToDateToNow($(this)) if disableToDate($(this))
