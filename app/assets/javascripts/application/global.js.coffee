$ ->

  $(document).foundation()

  $(".js-chosen").chosen({ width: '100%' })

  $(".js-sticky").stick_in_parent()

  # Removes animation class to block re-animation as a result of js-sticky use
  $(window).scroll ->
      $('.stretchRight').removeClass 'stretchRight'

  # handle un/select all
  $(".js-select-all").click (elem) ->
    for input in $(elem.target).closest('section').find("input.boolean[type='checkbox']")
      $(input).click() if input.checked != elem.target.checked

  # Show complete profile modal if it exists
  $("#completed-modal").foundation 'reveal', 'open' if $("#completed-modal").length

  # Make the user aware when they are about to leave a form that that have modified but not saved
  $(".js-dirty-check").areYouSure({
    message: "The changes you have made have not been saved. Click the Update Preferences button to save your changes."
  })


  # Turbolinks kills the beforeunload event that the areYouSure plugin relies upon.
  # Turbolinks offers page:before-load as a replacement, but it doesn't work as well.
  # Disabling Turbolinks when a form is marked dirty is reliable.
  $("form").on "dirty.areYouSure", -> $("body").attr("data-no-turbolink", "true")
  $("form").on "clean.areYouSure", -> $("body").removeAttr("data-no-turbolink")
