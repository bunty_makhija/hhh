$ ->
  if _gaq?
    listeners =
      click:
        'click-twitter':
          category: 'marketing-social'
          label: 'visitor clicks twitter link'
        'click-facebook':
          category: 'marketing-social'
          label: 'visitor clicks facebook link'
        'click-join-team':
          category: 'marketing'
          label: 'visitor clicks on join HHH team'
        'click-get-started':
          category: 'marketing-lead'
          label: 'visitor clicks on professionals get started'
        'click-partner-program':
          category: 'marketing-lead'
          label:  'visitor clicks on partner program learn more'
        'click-internship':
          category: 'marketing-lead'
          label: 'visitor clicks on internships link'
        'add-education':
          category: 'user-profile'
          label: 'add education history'
        'add-work':
          category: 'user-profile'
          label: 'add work history'
        'add-resume':
          category: 'user-profile'
          lable: 'add resume to profile'
        'cancel-account':
          category: 'user-settings'
          label: 'user cancels account'
        'sign-out':
          category: 'user-settings'
          label: 'user signs out of application'
      submit:
        'sign-in':
          category: 'user-settings'
          label: 'user signs in'
        'reset-password':
          category: 'user-settings'
          label: 'user resets password'
        'resend-confirmation':
          category: 'user-settings'
          label: 'user re-sends confirmation instructions'
        'submit-professional':
          category: 'user-profile'
          label: 'updates professional status'
        'submit-engagement':
          category: 'user-profile'
          label: 'updates engagement preferences'
        'submit-travel':
          category: 'user-profile'
          label: 'updates travel preferences'
        'submit-schedule':
          category: 'user-profile'
          label: 'updates schedule preferences'
        'submit-salary':
          category: 'user-profile'
          label: 'updates salary preferences'
        'submit-qualifications':
          category: 'user-profile'
          label: 'updates user qualifications'
        'submit-education':
          category: 'user-profile'
          label: 'updates user education'
        'submit-work':
          category: 'user-profile'
          label: 'updates work history'
        'submit-settings':
          category: 'user-profile'
          label: 'updates account settings'

    $.each listeners, (action) ->
      $("body").on action, (e) ->
        $target = $(e.target)

        $.each listeners[action], (selector) ->
          $filtered = $target.filter("[data-analytics='#{ selector }']")

          if $filtered.length
            params = listeners[action][selector]
            _gaq.push(['_trackEvent', params.category, selector, params.label, 1, 'true'])

    $creation     = $("span[data-analytics='create-profile']")
    $confirmation = $("span[data-analytics='confirm-profile']")

    if $creation.length > 0
        _gaq.push(['_trackEvent', 'user-sign-up', 'create-profile', 'user creates profile', 1, 'true' ])

    if $confirmation.length > 0
        _gaq.push(['_trackEvent', 'user-sign-up', 'confirm-profile', 'user confirms profile', 1, 'true' ])
