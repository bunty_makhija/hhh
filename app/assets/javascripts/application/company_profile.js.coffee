$ ->
    $(".js--slides").slidesjs
        width: 400
        height: 255
        pagination:
          active: false

        navigation:
          active: false

        play:
          active: true
          effect: "fade"
          interval: 6000
          auto: true
          swap: true
          pauseOnHover: false
          restartDelay: 2500
