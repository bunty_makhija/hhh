class window.StartDateManager
  startDateSelector: '.professional_profile_form_engagement_preference_starting_date'

  constructor: ->
    @showDateFields() if @activelyLooking()

    $("input[name='professional_profile_form[status]']").click =>
      if @activelyLooking() then @showDateFields() else @hideDateFields()

  showDateFields: ->
    $(@startDateSelector).parent().show()

  hideDateFields: ->
    $(@startDateSelector).parent().hide()

  activelyLooking: ->
    $("input[name='professional_profile_form[status]']:checked").val() == 'active'

$ ->
  new StartDateManager() if $(StartDateManager::startDateSelector).length
