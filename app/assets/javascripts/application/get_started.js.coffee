class window.GetStarted
  constructor: (pageContainerElement)->
    # check on initial page load if the details should be displayed
    # and if the submit button should be enabled.
    @page = $(pageContainerElement)
    @showAuthDetails()
    @checkFormInput()
    @addListeners()
    @checkOverflow()

  showAuthDetails: =>
    $authDetails = @page.find("#authorization_details")
    if @page.find(".wizard__reveal:checked").val() == "false"
      $authDetails.removeClass("wizard__hiddenField")
    else
      $authDetails.addClass("wizard__hiddenField")

  checkFormInput: =>
    if @page.find("input:checked").length > 0 then @enableSubmit() else @disableSubmit()

  disableSubmit: =>
    @page.find(".js--wizardNext").attr("disabled", true)

  enableSubmit: =>
    @page.find(".js--wizardNext").attr("disabled", false)

  checkPasswordFields: =>
    if @passwordExistsAndMatches() then @addSubmitHighlighting() else @removeSubmitHighlighting()

  passwordExistsAndMatches: =>
    $password             = @page.find(".js--wizardPassword").val()
    $passwordConfirmation = @page.find(".js--wizardPasswordConfirmation").val()
    return ($password == $passwordConfirmation && $password.length > 0)

  addSubmitHighlighting: =>
    @page.find(".js--skippable").addClass("button--wizardNext")

  removeSubmitHighlighting: =>
    @page.find(".js--skippable").removeClass("button--wizardNext")

  updateResumeUploadButton: =>
    @page.find(".js--resumeLabel").text("change resume").addClass("button--resume__labelInactive")
    @page.find(".js--resumeTitleWizard").html("<p>#{$('#get_started_last_thing_step_resume').get(0).files[0].name}</p>")
    @page.find(".js--resumeLinkWizard").hide()
    @page.find(".js--resumeLaterWizard").hide()
    @addSubmitHighlighting()

  checkOverflow: =>
    $wizardFormOverflow = @page.find(".wizard__formOverflow")
    $wizardScrollIndicator = @page.find(".wizard__scrollIndicator")
    if $wizardFormOverflow.prop("scrollHeight") > $wizardFormOverflow.prop("clientHeight")
      $wizardFormOverflow.addClass("wizard__formOverflowBorder")
      $wizardScrollIndicator.show()
    else
      $wizardFormOverflow.removeClass("wizard__formOverflowBorder")

  addListeners: =>
    @page.find(".wizard__reveal").change                  @showAuthDetails
    @page.find("input.required, input.optional").change   @checkFormInput
    @page.find(".js--wizardPassword,
      .js--wizardPasswordConfirmation").on keyup:         @checkPasswordFields
    @page.find('.js--wizardResume').on change:            @updateResumeUploadButton
    $(window).on resize:                                  @checkOverflow

$ ->
  $(".js--wizard__form").each ->
    new GetStarted(this)
