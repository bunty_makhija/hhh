$ ->
  options =
    filter: $('.js--filter')
    section: $('.js--filter__item')

  filter = (options) ->

    options.filter.on 'click', 'a', (e) ->
      e.preventDefault()
      elem = $(@)
      show = elem.attr('href').split('#')[1]

      $('.js--filter a').removeClass('is--active')
      elem.addClass('is--active')

      options.section.removeClass('is--filtered')
      options.section.not("[data-#{show}=true]").addClass('is--filtered') unless show is 'all'


  filter(options)
