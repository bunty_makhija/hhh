#= require active_admin/base
#= require tinymce-jquery
#= require chosen-jquery


$ ->

  tinyMCE.init {
    mode : "specific_textareas",
    editor_selector : "tinymce-editor"
  }

  $(".js-chosen").chosen()

  # enhanced, stern warning for deleting of company profiles
  $('#index_table_company_profiles .delete_link').each (i, e)->
    newMessage = "#{$(e).data('confirm')}\n\nDOING SO WILL DELETE ALL ASSOCIATED JOB OPPORTUNITIES!"
    $(e).data('confirm', newMessage)
