ActiveAdmin.register ActsAsTaggableOn::Tag, as: 'Skill' do
  permit_params :name
  config.sort_order = 'name_asc'
  actions :all, except: %i(show)

  scope_to do
    ActsAsTaggableOn::Tag.skills
  end

  index do
    selectable_column
    column :name
    default_actions
  end

  filter :name

  show do
    attributes_table do
      row :name
    end
  end

  form do |f|
    f.inputs 'Skill Details' do
      f.input :name
    end

    f.actions
  end

  controller do
    def create
      params[:skill].merge! original_context: ActsAsTaggableOn::Tag::PREDEFINED_CONTEXTS[:skill]
      create!
    end
  end

end
