ActiveAdmin.register ActsAsTaggableOn::Tag, as: 'Certification' do
  permit_params :name
  config.sort_order = 'name_asc'
  actions :all, except: %i(show)

  scope_to do
    ActsAsTaggableOn::Tag.certs
  end

  index do
    selectable_column
    column :name
    default_actions
  end

  filter :name

  show do
    attributes_table do
      row :name
    end
  end

  form do |f|
    f.inputs I18n.t('views.admin.cert.edit.heading') do
      f.input :name
    end

    f.actions
  end

  controller do
    def create
      params[:certification].merge! original_context: ActsAsTaggableOn::Tag::PREDEFINED_CONTEXTS[:cert]
      create!
    end

    def scoped_collection
      ActsAsTaggableOn::Tag.certs
    end
  end

end
