include Admin::DisplayHelper

ActiveAdmin.register AudienceProfile do
  permit_params(
    :audience_type_id,
    :title,
    :bio,
    :featured,
    :primary_image,
    :primary_image_cache,
    :remove_primary_image,
    :published,
    skill_list: [],
    cert_list: []
  )


  config.batch_actions = false


  index do
    column :audience_type, sortable: 'audience_types.name' do |profile|
      profile.audience_type.name
    end
    column :title
    column(:featured) { |profile| profile.featured? ? I18n.t('affirmative') : I18n.t('negative') }
    column(:bio) {|profile| profile.bio.try :html_safe }
    default_actions
  end


  filter :audience_type
  filter :title


  show title: :title do |profile|
    attributes_table do
      row(:audience_type) { profile.audience_type.name }
      row :title
      row(:featured) { profile.featured? ? I18n.t('affirmative') : I18n.t('negative') }
      row(:published) { profile.published? ? I18n.t('affirmative') : I18n.t('negative') }
      safe_html_text_row :bio, profile
      row(:primary_image) { image_tag profile.primary_image_url if profile.primary_image? }
      row :cert_list
      row :skill_list
    end
  end


  form do |f|
    f.inputs "Audience Profile", multipart: true do
      f.input :audience_type
      f.input :featured, as: :boolean
      f.input :published, as: :boolean
      f.input :title
      image_input :primary_image, f
      f.input(
        :cert_list,
        label: I18n.t('simple_form.labels.qualifications.cert_list'),
        collection: cert_options,
        input_html: { multiple: true, class: 'js-chosen' }
      )
      f.input(
        :skill_list,
        label: I18n.t('simple_form.labels.qualifications.skill_list'),
        collection: skill_options,
        input_html: { multiple: true, class: 'js-chosen' }
       )
      f.input :bio
    end

    f.actions
  end


  controller do
    # allows sorting by audience type name on index page
    def scoped_collection
      super.includes(:audience_type)
    end
  end


  csv do
    column(:audience_type) {|profile| profile.audience_type.name }
    column :title
    column :featured
    column :published
    column :bio
    column(:created_at) {|company| export_date company.created_at }
  end
end
