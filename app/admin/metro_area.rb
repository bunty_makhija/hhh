include Admin::DisplayHelper

ActiveAdmin.register MetroArea do

  permit_params(
    :area_name,
    :city,
    :state
  )

  index do
    selectable_column
    column :area_name
    column :city
    column :state
    default_actions
  end

  filter :area_name
  filter :city
  filter :state

  form do |f|
    f.inputs "Metro Area" do
      f.input :area_name
      f.input :city
      f.input :state
    end

    f.actions
  end

  csv do
    column :area_name
    column :city
    column :state
  end
end
