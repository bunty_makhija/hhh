include QualificationsHelper
include Admin::DisplayHelper
include Admin::JobOpportunityHelper

ActiveAdmin.register JobOpportunity do
  permit_params(
    :company_profile_id,
    :title,
    :industry,
    :industry_other,
    :schedule,
    :city,
    :state,
    :job_description,
    { skill_list: [] },
    { cert_list: [] },
    :status )

  config.batch_actions = false

  index do
    column :company_profile, sortable: 'company_profiles.company_name' do |jobop|
      jobop.company_profile.company_name
    end
    column :title
    column :city
    column :state
    column(:status) { |jobop| jobop.status_text }
    column :schedule, sortable: :schedule do |jobop|
      jobop.schedule_text
    end
    default_actions
  end

  filter :company_profile
  filter :title
  filter :industry, as: :select, collection: -> { localized_collection(JobOpportunity.industry) }
  filter :schedule, as: :select, collection: -> { localized_collection(JobOpportunity.schedule) }
  filter :state
  filter :status, as: :select, collection: -> { localized_collection(JobOpportunity.status) }
  filter :company_profile

  show do |jobop|
    attributes_table do
      row(:company_profile) { jobop.company_profile.company_name }
      row :title
      row(:status) { jobop.schedule_text }
      row(:schedule) { jobop.schedule_text }
      row(:industry) { jobop.industry_text }
      row :industry_other
      row :city
      row :state
      row :cert_list
      row :skill_list
      safe_html_text_row :job_description, jobop
    end
  end

  form do |f|
    f.inputs do
      f.input :status
      f.input :company_profile
      f.input :title
      f.input :industry
      f.input :industry_other
      f.input :city
      f.input :state, collection: Carmen::Country.named("United States").subregions.map(&:code)
      f.input :schedule

      f.input(
        :cert_list,
        label: I18n.t('views.admin.taggable.cert_list'),
        collection: cert_options,
        input_html: { multiple: true, class: 'js-chosen' }
      )

      f.input(
        :skill_list,
        label: I18n.t('views.admin.taggable.skill_list'),
        collection: skill_options,
        input_html: { multiple: true, class: 'js-chosen' }
      )

      f.input :job_description, input_html: { class: 'tinymce-editor' }
    end

    f.actions
  end

  controller do
    # allows sorting by company name on index page
    def scoped_collection
      super.includes(:company_profile)
    end
  end

  csv do
    column(:company) {|opp| opp.company_profile.company_name }
    column :title
    column(:industry) {|opp| opp.industry.text }
    column :industry_other
    column(:schedule) {|opp| opp.schedule.text }
    column :job_description
    column(:status) {|opp| opp.status.text }
    column :city
    column :state
    column(:wanted_skills) {|opp| export_job_skills opp }
    column(:wanted_certifications) {|opp| export_job_certs opp }
    column(:created_at) {|opp| export_date opp.created_at }
  end
end
