include Admin::CsvHelper

ActiveAdmin.register User do
  permit_params :email, :first_name, :last_name, :phone_number, :password, :password_confirmation

  config.batch_actions = false

  index do
    column :email
    column :first_name
    column :last_name
    column :phone_number
    column :sign_in_count
    column :last_sign_in_at
    default_actions
  end

  filter :email
  filter :first_name
  filter :last_name

  show do
    attributes_table do
      row(ProfessionalProfile.model_name.human) do |user|
        link_to user.professional_profile.status_text, admin_professional_profile_path(user.professional_profile.id)
      end
      row(:role) {|user| user.roles.first.try(:name).try(:humanize) }
      row :email
      row :first_name
      row :last_name
      row :phone_number
      row :created_at
      row :confirmed_at
      row :sign_in_count
      row :last_sign_in_at
      row :last_sign_in_ip
      row :confirmation_sent_at
    end
  end

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :phone_number
      f.input :password
      f.input :password_confirmation
      f.input(
        :roles,
        label: 'Role',
        input_html: { multiple: false },
        collection: options_for_select({
        'None' => nil,
        'Admin' => :admin,
        'Profile Manager' => :profile_manager,
        'Profile Viewer' => :profile_viewer
       }, f.object.roles.first.try(:name))
      )
    end
    f.actions
  end

  csv do
    column :email
    column :first_name
    column :last_name
    column :phone_number
    column(:created_at) {|user| export_date user.created_at }
    column(:confirmed_at) {|user| export_date user.confirmed_at }
    column :sign_in_count
    column(:last_sign_in_at) {|user| export_date user.last_sign_in_at }
    column(:confirmation_sent_at) {|user| export_date user.confirmation_sent_at}
  end

  controller do
    before_action :strip_password_if_empty, only: :update
    after_action :assign_role, only: [ :update, :create ]

    private

    def strip_password_if_empty
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
          params[:user].delete(:password)
          params[:user].delete(:password_confirmation)
      end
    end

    def assign_role
      return if resource.errors.present?
      roles = params[:user][:role_ids].delete_if{|role| role.blank? }
      resource.roles = Role.where(name: roles).all
    end
  end

end
