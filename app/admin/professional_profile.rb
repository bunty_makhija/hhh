include QualificationsHelper
include Admin::ProfessionalProfileHelper

ActiveAdmin.register ProfessionalProfile do
  permit_params(
    :status,
    :country,
    :state,
    :city,
    :zip_code,
    :resume,
    :remove_resume,
    :resume_cache,
    :civil,
    :infrastructure,
    :energy,
    :building,
    :petroleum,
    :anything,
    :associated_with,
    :referrals,
    :experience,
    :authorization,
    :authorization_details,
    metro_area_ids: [],
    sub_project_type_ids: [],
    category_function_ids: [],
    role_preference_attributes: component_attribute_names(RolePreference),
    engagement_preference_attributes: component_attribute_names(EngagementPreference),
    travel_preference_attributes: component_attribute_names(TravelPreference),
    schedule_preference_attributes: component_attribute_names(SchedulePreference),
    salary_preference_attributes: component_attribute_names(SalaryPreference),
    qualification_attributes: component_attribute_names(Qualification, [{ skill_list: [] }, { cert_list: [] }] ),
    work_histories_attributes: component_attribute_names(WorkHistory, [:id, :_destroy]),
    education_histories_attributes: component_attribute_names(EducationHistory, [:id, :_destroy])
  )

  config.batch_actions = false


  index do
    column User.model_name.human do |profile|
      profile.user.full_name
    end
    column User.human_attribute_name(:email) do |profile|
      profile.user.email
    end
    column(:resume) { |profile| profile.resume? || profile.hubspot_resume_url.present? ? I18n.t('affirmative') : '' }
    column(:associated_with) { |profile| profile.associated_with? ? I18n.t('affirmative') : '' }
    column("Employment Status") { |profile| profile.status_text }
    column :country
    column :zip_code
    default_actions
  end


  filter :user_last_name, as: :string
  filter :user_email, as: :string
  filter :created_at
  filter :status, as: :select, collection: -> { localized_collection(ProfessionalProfile.status) }
  filter :country
  filter :zip_code
  filter :engagement_preference_term_quick, as: :boolean, label: -> { I18n.t('views.admin.professional_status.label.term_quick') }
  filter :engagement_preference_term_small, as: :boolean, label: -> { I18n.t('views.admin.professional_status.label.term_small') }
  filter :engagement_preference_term_large, as: :boolean, label: -> { I18n.t('views.admin.professional_status.label.term_large') }
  filter :engagement_preference_term_permanent, as: :boolean, label: -> { I18n.t('views.admin.professional_status.label.term_permanent') }
  filter :engagement_preference_term_various, as: :boolean, label: -> { I18n.t('views.admin.professional_status.label.term_various') }
  filter :role_preference_desired_role_name, label: -> { I18n.t('views.admin.professional_status.label.role_preference') }, as: :select, collection: -> { AudienceType.all.map(&:name) }
  filter :civil
  filter :energy
  filter :building
  filter :infrastructure
  filter :petroleum
  filter :experience
  filter :authorization
  filter :anything


  show do |profile|
    attributes_table do
      row(:status) { |profile| profile.status_text }
      row :country
      row :state
      row :city
      row :zip_code
      row :category
      row :category_functions do
        profile.category_functions.map(&:key).join(", ")
      end
      row "Project Types" do |profile|
        profile.project_types.map(&:display_name).join(", ")
      end
      row "Sub Project Type" do |profile|
        profile.sub_project_types.map(&:display_name).join(", ")
      end
      IndustryHelper::TYPES_WITH_ANYTHING.each do |industry|
        row(industry) { |profile| profile.send(industry) ? I18n.t('interested') : I18n.t('not_interested') }
      end
      row :metro_areas do
        profile.metro_areas.map(&:area_name).join(", ")
      end
      row :experience
      row :authorization
      row :authorization_details
      row :associated_with
      row :referrals
      row(:resume) { link_to "#{profile.resume.filename} (original filename: #{profile.resume_display_name})",
        profile.resume_url if profile.resume? }
      row :hubspot_resume_url
    end
    panel "#{RolePreference.model_name.human} Details" do
      role_preference = profile.role_preference
      attributes_table_for role_preference do
        row :audience_type_id do |role_preference|
          role_preference.name
        end
      end
    end
    show_panel_for profile, :engagement_preference
    show_panel_for profile, :travel_preference
    show_panel_for profile, :schedule_preference
    show_panel_for profile, :salary_preference
    panel "#{Qualification.model_name.human} Details" do
      qualification = profile.qualification
      attributes_table_for qualification do
        row :cert_list
        row :skill_list
        text_row qualification, :additional_certs
        text_row qualification, :additional_skills
      end
    end
    show_collection_for profile, :work_histories
    show_collection_for profile, :education_histories
  end


  form do |f|
    f.inputs ProfessionalProfile.model_name.human do
      f.input :status
      f.input :country, as: :select, collection: countries_list
      f.input :state, as: :select, collection: subregions_list
      f.input :city
      f.input :zip_code
      IndustryHelper::TYPES_WITH_ANYTHING.each { |industry| f.input industry }
      f.input :associated_with
      f.input :referrals
      f.input :experience
      f.input :authorization
      f.input :authorization_details
      file_input :resume, f
    end

    f.inputs SubProjectType.model_name.human do
      ProjectType.all.each do |project_type|
        f.fields_for :sub_project_type do |sub_project_type|
          f.input :sub_project_types, as: :check_boxes, collection: project_type.sub_project_types, label: project_type.display_name
        end
      end
    end

    f.inputs CategoryFunction.model_name.human do
      Category.all.each do |category|
        f.input :category_functions, as: :check_boxes, collection: category.category_functions, label: category.display_name
      end
    end

    f.inputs MetroArea.model_name.human do
      f.fields_for :metro_area do |area|
        area.input :metro_area_id, as: :check_boxes, collection: MetroArea.all.map(&:area_name)
      end
    end

    f.inputs RolePreference.model_name.human do
      f.fields_for :role_preference do |role_preference|
        role_preference.input :audience_type_id, as: :select, collection: AudienceType.all
      end
    end

    edit_panel_for(f, :engagement_preference) do |opts|
      opts.merge! start_year: Time.now.year if opts.has_key? :start_year
      opts.merge! end_year: (Time.now + 3.years).year if opts.has_key? :end_year
      opts
    end

    edit_panel_for(f, :travel_preference)
    edit_panel_for(f, :schedule_preference)
    edit_panel_for(f, :salary_preference)
    f.inputs Qualification.model_name.human, for: :qualification do |f|
      f.input(
        :cert_list,
        label: I18n.t('views.admin.taggable.cert_list'),
        collection: cert_options,
        input_html: { multiple: true, class: 'js-chosen' }
      )
      f.input(
        :skill_list,
        label: I18n.t('views.admin.taggable.skill_list'),
        collection: skill_options,
        input_html: { multiple: true, class: 'js-chosen' }
       )
      f.input :additional_certs, input_html: { class: 'tinymce-editor' }
      f.input :additional_skills, input_html: { class: 'tinymce-editor' }
    end
    f.inputs WorkHistory.model_name.human do
      f.has_many :work_histories, heading: false, allow_destroy: true do |wh|
        component_attribute_names(WorkHistory).each do |attr|
          wh.input attr, input_options(WorkHistory, attr)
        end
      end
    end
    f.inputs EducationHistory.model_name.human do
      f.has_many :education_histories, heading: false, allow_destroy: true do |eh|
        component_attribute_names(EducationHistory).each do |attr|
          eh.input attr, input_options(EducationHistory, attr)
        end
      end
    end
    f.actions
  end


  csv do
    column(:first_name) {|profile| profile.user.first_name}
    column(:last_name) {|profile| profile.user.last_name}
    column(:email) {|profile| profile.user.email}
    column(:phone) {|profile| profile.user.phone_number}
    column(:seek_status) {|profile| profile.status.text }
    column :country
    column :state
    column :city
    column :zip_code
    column :associated_with
    column :experience
    column :authorization
    column :authorization_details
    column :referrals
    column(:industry_preference) {|profile| export_industries profile }
    column(:desired_role) {|profile| profile.role_preference.name }
    column(:start_date) {|profile| profile.engagement_preference.starting_date }
    column(:engagement_preferences) {|profile| export_engagement_preferences profile }
    column(:engagement_concerns) {|profile| profile.engagement_preference.other_concerns }
    column(:will_travel) {|profile| profile.travel_preference.will_travel }
    column(:will_relocate) {|profile| profile.travel_preference.will_relocate }
    column(:travel_regions) {|profile| export_travel_regions profile }
    column(:travel_concerns) {|profile| profile.travel_preference.other_concerns }
    column(:home_frequency) {|profile| profile.travel_preference.home_frequency.text }
    column(:schedule_preferences) {|profile| export_schedule_preferences profile }
    column(:schedule_concerns) {|profile| profile.schedule_preference.other_concerns }
    column(:base_salary) {|profile| profile.salary_preference.base_salary }
    column(:hourly_rate) {|profile| profile.salary_preference.hourly_rate }
    column(:daily_travel_comp) {|profile| profile.salary_preference.daily_travel_comp }
    column(:salary_concerns) {|profile| profile.salary_preference.other_concerns }
    column(:skills) {|profile| export_skills profile }
    column(:additional_skills) {|profile| profile.qualification.additional_skills }
    column(:certifications) {|profile| export_certs profile }
    column(:additional_certifications) {|profile| profile.qualification.additional_certs }
    column(:education) {|profile| export_education profile }
    column(:work_history) {|profile| export_work_history profile }
    column :resume
    column(:updated_at) {|profile| export_date profile.updated_at }
    column(:created_at) {|profile| export_date profile.created_at }
  end

  controller do
    def scoped_collection
      ProfessionalProfile.includes(
        :user,
        :engagement_preference,
        :travel_preference,
        :schedule_preference,
        :salary_preference,
        { qualification: [:skills, :certs] },
        :work_histories,
        { education_histories: :concentrations } )
    end
  end

end
