include Admin::DisplayHelper

ActiveAdmin.register CompanyProfile do
  permit_params(
    :company_name,
    :contact_name,
    :email,
    :about,
    :logo,
    :logo_cache,
    :remove_logo,
    :key_facts,
    :additional_info,
    :video_embed,
    :featured,
    :consulting_partner,
    :employment_partner,
    images_attributes:
      [
       :id,
       :attachment,
       :attachment_cache, 
       :imageable_id,
       :imageable_type,
       :_destroy
      ],
    industries: [],
    audience_type_ids: [] )


  batch_action :destroy, false
  batch_action :feature do |ids|
    CompanyProfile.where(id: ids).each do |profile|
      profile.update_attribute :featured, !profile.featured
    end

    redirect_to collection_path
  end


  index do
    selectable_column
    column :company_name
    column(:featured) { |profile| profile.featured? ? I18n.t('affirmative') : I18n.t('negative') }
    column :contact_name
    column :email
    column(:logo) { |profile| profile.logo? ? I18n.t('affirmative') : I18n.t('negative') }
    default_actions
  end


  scope :consulting_partners
  scope :employment_partners

  filter :company_name
  filter :contact_name
  filter :email


  show title: :company_name do |profile|
    attributes_table do
      row :company_name
      row(:featured) { profile.featured? ? I18n.t('affirmative') : I18n.t('negative') }
      row(:consulting_partner) { profile.consulting_partner? ? I18n.t('affirmative') : I18n.t('negative') }
      row(:employment_partner) { profile.employment_partner? ? I18n.t('affirmative') : I18n.t('negative') }
      row(:industries) { localize_industry_list(profile.industries) }
      row(:audience_types) { profile.audience_types.map(&:name).sort.to_sentence }
      row :contact_name
      row :email
      safe_html_text_row :about, profile
      safe_html_text_row :key_facts, profile
      safe_html_text_row :additional_info, profile
      row(:logo) { image_tag profile.logo_url if profile.logo? }
      row 'Images' do
        ul do
          profile.images.each do |image| 
            li do
              image_tag image.attachment.url
            end
          end
        end
      end
      row :video_embed
    end
  end


  form do |f|
    f.inputs "Company Profile", multipart: true do
      f.input :company_name
      f.input :featured, as: :boolean
      f.input :consulting_partner, as: :boolean
      f.input :employment_partner, as: :boolean
      f.input(
        :industries,
        as: :check_boxes,
        collection: IndustryHelper::TYPES.map{ |type| [I18n.t(type, scope: 'enumerize.industry_types'), type.to_s] } )
      f.input :audience_types, as: :check_boxes,
          collection: AudienceType.order("name ASC")
      f.input :contact_name
      f.input :email
      image_input :logo, f
      f.has_many :images, allow_destroy: true do |ff|
        image_input :attachment, ff
      end
      f.input :video_embed
      f.input :about, input_html: { class: 'tinymce-editor' }
      f.input :key_facts, input_html: { class: 'tinymce-editor' }
      f.input :additional_info, input_html: { class: 'tinymce-editor' }
    end

    f.actions
  end

  controller do
    defaults :finder => :find_by_slug
  end

  csv do
    column :company_name
    column :featured
    column :consulting_partner
    column :employment_partner
    column :contact_name
    column :email
    column :about
    column :key_facts
    column :additional_info
    column(:created_at) {|company| export_date company.created_at }
  end
end
