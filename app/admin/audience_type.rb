include Admin::DisplayHelper

ActiveAdmin.register AudienceType do
  permit_params(*%i[
    name
    description
    more_about
    position
    published
    primary_image
    primary_image_cache
    remove_primary_image
  ])

  controller do
    defaults :finder => :find_by_slug
  end

  batch_action :destroy, false
  batch_action :feature do |ids|
    AudienceType.where(id: ids).each do |type|
      type.update_attribute :published, !type.published
    end

    redirect_to collection_path
  end


  index do
    selectable_column
    column :name
    column(:published) { |type| type.published? ? I18n.t('affirmative') : I18n.t('negative') }
    column :position
    column(:description) {|type| type.description.try :html_safe }
    column(:more_about) {|type| type.more_about.try :html_safe }
    default_actions
  end


  filter :name
  filter :published


  show title: :name do |type|
    attributes_table do
      row :name
      row(:published) { type.published? ? I18n.t('affirmative') : I18n.t('negative') }
      row :position
      safe_html_text_row :description, type
      safe_html_text_row :more_about, type
      row(:primary_image) { image_tag type.primary_image_url if type.primary_image? }
    end
  end


  form do |f|
    f.inputs "Audience Type", multipart: true do
      f.input :name
      f.input :published, as: :boolean
      f.input :position, as: :select, collection: AudienceType::POSITIONS
      image_input :primary_image, f
      f.input :description
      f.input :more_about
    end

    f.actions
  end


  csv do
    column :name
    column :published
    column :position
    column :description
    column :more_about
    column(:created_at) {|company| export_date company.created_at }
  end
end
