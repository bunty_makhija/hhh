include Admin::DisplayHelper

ActiveAdmin.register CategoryFunction do

  permit_params(
    :key,
    :display_name,
    :position,
    :category_id
  )

  index do
    selectable_column
    column :key
    column :display_name
    column :category
    column :position
    default_actions
  end

  filter :display_name
  filter :key

  form do |f|
    f.inputs "Category function" do
      f.input :key
      f.input :display_name
      f.input :position
      f.input :category_id, as: :select, collection: Category.all
    end

    f.actions
  end

  csv do
    column("Category Key")          { |cf| cf.category.key }
    column("Category Display Name") { |cf| cf.category.display_name }
    column :key
    column :display_name
  end
end
