include Admin::DisplayHelper

ActiveAdmin.register SubProjectType do

  permit_params(
    :key,
    :display_name,
    :project_type_id,
    :position
  )

  index do
    selectable_column
    column :key
    column :display_name
    column :position
    column :project_type
    default_actions
  end

  filter :display_name
  filter :key

  form do |f|
    f.inputs SubProjectType.model_name.human do
      f.input :key
      f.input :display_name
      f.input :position
      f.input :project_type_id, as: :select, collection: ProjectType.all
    end

    f.actions
  end

  csv do
    column("ProjectType Key")          { |spt| spt.project_type.key }
    column("ProjectType Display Name") { |spt| spt.project_type.display_name }
    column :key
    column :display_name
  end
end
