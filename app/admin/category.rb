include Admin::DisplayHelper

ActiveAdmin.register Category do

  permit_params(
    :key,
    :display_name,
    :position
  )

  index do
    selectable_column
    column :key
    column :display_name
    column :position
    default_actions
  end

  filter :display_name
  filter :key

  form do |f|
    f.inputs "Category" do
      f.input :key
      f.input :display_name
      f.input :position
    end

    f.actions
  end

  csv do
    column :key
    column :display_name
  end
end
