class GetStarted::WizardStep
  include ActiveModel::Model

  attr_accessor :professional_profile, :params

  def self.reflect_on_association(*args)
    ProfessionalProfile.reflect_on_association(*args)
  end

  def self.slug
    # This method must return the slug used by wizard
    self.name.demodulize.chomp('Step').underscore.to_sym
  end
  delegate :slug, to: :class

  def self.to_param
    slug
  end
  delegate :to_param, to: :class

  def initialize(user, params=ActionController::Parameters.new)
    @user = user
    @professional_profile = user.professional_profile
    @params = params
  end

  def save
    ProfessionalProfile.transaction do
      apply_attributes

      if valid?
        professional_profile.save(validate: false)
        hubspot_form = HubspotterInterface::WizardForm.new(@user)
        hubspot_form.submit
      else
        raise ActiveRecord::Rollback
      end
    end
  end

  def skip?
    force_skip? || step_complete?
  end

  def force_skip?
    # Return true to skip this step
    false
  end

  def step_complete?
    valid?
  end

  private

  def apply_attributes
    raise NotImplementedError
  end

  def safe_params
    raise NotImplementedError
  end
end
