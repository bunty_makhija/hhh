class GetStarted::FunctionStep < GetStarted::WizardStep
  validates :category_functions, presence: true

  delegate :category_functions, :category_function_ids, to: :professional_profile

  def valid_category_function_ids
    professional_profile.category.category_functions
  end

  private

  def apply_attributes
    professional_profile.category_functions = CategoryFunction.where(id: safe_params[:category_function_ids])
  end

  def safe_params
    params.fetch(:get_started_function_step, {}).permit(category_function_ids: [])
  end
end
