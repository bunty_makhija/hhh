class GetStarted::IndustryStep < GetStarted::WizardStep
  include ActionView::Helpers::TranslationHelper

  validate :industry_present?

  delegate *IndustryHelper::TYPES, to: :professional_profile

  def industry_present?
    unless IndustryHelper::TYPES.map { |type_name| send(type_name) }.any?
      errors.add(:professional_profile, t('views.get_started/industry.none_selected'))
    end
  end

  def force_skip?
    GetStarted::IndustrySkipLogic.new(@user).skip_page?
  end

  private

  def apply_attributes
    professional_profile.assign_attributes(safe_params)
  end

  def safe_params
    params.fetch(:get_started_industry_step, {}).permit(*IndustryHelper::TYPES)
  end
end
