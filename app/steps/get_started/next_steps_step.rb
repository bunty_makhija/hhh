class GetStarted::NextStepsStep < GetStarted::WizardStep

  def preferred_industry
    IndustryHelper::TYPES.inject do |preferred_industry, industry|
      if @professional_profile.send(industry)
        preferred_industry = industry
      end
      preferred_industry
    end
  end

  def industry_blog_link
    I18n.t("views.get_started.next_steps.base_blog_link") + I18n.t(preferred_industry, scope: "views.get_started.next_steps")
  end

  private

  def apply_attributes
  end

  def safe_params
    {}
  end
end
