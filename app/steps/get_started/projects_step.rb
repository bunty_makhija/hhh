class GetStarted::ProjectsStep < GetStarted::WizardStep
  validates :project_types, presence: true

  delegate :project_types, :project_type_ids, to: :professional_profile

  def force_skip?
    GetStarted::ProjectSkipLogic.new(@user).skip_page?
  end

  private

  def apply_attributes
    professional_profile.project_type_ids = safe_params[:project_type_ids]
  end

  def safe_params
    params.fetch(:get_started_projects_step, {}).permit(project_type_ids: [])
  end
end
