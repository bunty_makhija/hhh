class GetStarted::WelcomeStep < GetStarted::WizardStep
  validates :category_id, presence: true

  delegate :category_id, to: :professional_profile

  private

  def apply_attributes
    professional_profile.assign_attributes(safe_params)
    GetStarted::ProjectSkipLogic.new(@user).attach_project_type_if_needed
  end

  def safe_params
    params.fetch(:get_started_welcome_step, {}).permit(:category_id)
  end
end
