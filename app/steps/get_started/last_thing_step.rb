class GetStarted::LastThingStep < GetStarted::WizardStep
  delegate :resume, :resume_cache, to: :professional_profile

  private

  def apply_attributes
    professional_profile.assign_attributes(safe_params)
  end

  def safe_params
    params.fetch(:get_started_last_thing_step, {}).permit(:resume, :resume_cache)
  end
end
