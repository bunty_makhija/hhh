class GetStarted::SetPasswordStep < GetStarted::WizardStep
  attr_reader :user

  delegate :password, :password_confirmation, to: :user

  def valid?
    errors.empty?
  end

  def save
    if no_password_provided?
      true
    elsif reset_password!
      true
    else
      user.errors.each {|att, msg| errors.add(att, msg)}
      false
    end
  end

  private

  def no_password_provided?
    [safe_params[:password], safe_params[:password_confirmation]].all?(&:empty?)
  end

  def reset_password!
    user.reset_password!(safe_params[:password], safe_params[:password_confirmation])
  end

  def apply_attributes
  end

  def safe_params
    @safe_params ||= params.fetch(:get_started_set_password_step, {}).permit(:password, :password_confirmation)
  end
end
