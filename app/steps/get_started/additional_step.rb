class GetStarted::AdditionalStep < GetStarted::WizardStep
  validates :experience, presence: true
  validates :authorization, inclusion: { in: [true, false] }

  delegate :metro_area_ids, :experience, :authorization, :authorization_details, to: :professional_profile

  private

  def apply_attributes
    professional_profile.assign_attributes(safe_params)
  end

  def safe_params
    params.fetch(:get_started_additional_step, {}).permit(:experience, :authorization, :authorization_details, metro_area_ids: [])
  end
end
