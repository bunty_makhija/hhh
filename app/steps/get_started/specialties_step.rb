class GetStarted::SpecialtiesStep < GetStarted::WizardStep
  validates :sub_project_types, presence: true

  delegate :sub_project_types, :sub_project_type_ids, to: :professional_profile

  def possible_sub_projects
    SubProjectType.where(project_type_id: professional_profile.project_types)
  end

  private

  def apply_attributes
    professional_profile.sub_project_types = SubProjectType.where(id: safe_params[:sub_project_type_ids])
  end

  def safe_params
    params.fetch(:get_started_specialties_step, {}).permit(sub_project_type_ids: [])
  end
end
