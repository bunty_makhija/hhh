module HubspotterInterface
  def self.message_verifier
    ActiveSupport::MessageVerifier.new(Settings.message_token)
  end
end
