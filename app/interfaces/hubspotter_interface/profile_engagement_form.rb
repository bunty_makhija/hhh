module HubspotterInterface
  class ProfileEngagementForm < ProfileForm
    def form_fields
      [:hardhat_updated_at, :firstname, :lastname, :email, :phone,
        :zip, :country, :state, :city, :preferred_industries, :industry,
        :professional_status, :starting_date, :engagement_term,
      :availability_concerns, :function, :resume]
    end
  end
end
