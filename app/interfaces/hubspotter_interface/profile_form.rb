module HubspotterInterface
  class ProfileForm < Form
    attr_accessor :contact

    def initialize(user, token, ip)
      super
      @contact = HardhatContact.new(user)
    end

    def form_fields
      []
    end

    def form_data
      form_fields.inject({}) do |data, field|
        data[field] = contact.send(field)
        data
      end
    end

    def form_token
      token_name = self.class.name.split("::")[1].gsub(/Form/,'').underscore
      Settings.hubspot.form_tokens.send(token_name)
    end
  end
end
