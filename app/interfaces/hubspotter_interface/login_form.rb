module HubspotterInterface
  class LoginForm < Form
    def initialize(email, token, ip)
      @email = email
      @token = token
      @ip = ip
    end

    def form_data
      { email: @email }
    end

    def form_token
      Settings.hubspot.form_tokens.login
    end
  end
end
