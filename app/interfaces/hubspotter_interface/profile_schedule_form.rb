module HubspotterInterface
  class ProfileScheduleForm < ProfileForm
    def form_fields
      [:hardhat_updated_at, :email, :work_type, :schedule_concerns]
    end
  end
end
