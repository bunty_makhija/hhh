module HubspotterInterface
  class ProfileWorkForm < ProfileForm
    def form_fields
      [:hardhat_updated_at, :email, :work_history]
    end
  end
end
