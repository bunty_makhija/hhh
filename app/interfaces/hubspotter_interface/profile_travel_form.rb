module HubspotterInterface
  class ProfileTravelForm < ProfileForm
    def form_fields
      [:hardhat_updated_at, :email, :will_travel, :home_frequency,
        :will_relocate, :regions, :travel_concerns]
    end
  end
end
