module HubspotterInterface
  class SignupForm < ProfileForm
    def form_fields
      [:hardhat_created_at, :firstname, :lastname, :email, :zip,
        :referred_by_name_, :function, :preferred_industries, :resume]
    end
  end
end
