module HubspotterInterface
  class HardhatContact
    HUBSPOT_DATE_FORMAT = "%m/%d/%Y"
    MONTH_YEAR = "%m/%Y"

    include IndustryHelper
    attr_reader :user, :profile, :salary_data, :qualifications

    delegate :email, to: :user
    delegate :country, :state, :city, :industry_other, :authorization_details, to: :profile
    delegate :base_salary, :hourly_rate, to: :salary_data
    delegate :additional_skills, to: :qualifications

    def initialize(user)
      @user = user
      @profile = user.professional_profile
      @salary_data = @profile.salary_preference
      @qualifications = @profile.qualification
    end

    def date_field(date, format)
      return nil if date.blank?
      date.strftime(format)
    end

    def hardhat_created_at
      date_field(user.created_at, HUBSPOT_DATE_FORMAT)
    end

    def hardhat_updated_at
      date_field(user.updated_at, HUBSPOT_DATE_FORMAT)
    end

    def firstname
      user.first_name
    end

    def lastname
      user.last_name
    end

    def zip
      profile.zip_code
    end

    def phone
      user.phone_number
    end

    def resume
      profile.resume_url
    end

    def professional_status
      profile.status
    end

    def availability_concerns
      profile.engagement_preference.other_concerns
    end

    def function
      profile.role_preference.desired_role.try(:name)
    end

    def regions
      regions = []
      travel = user.professional_profile.travel_preference
      [*1..10].each do |i|
        if travel.send("region#{i}")
          regions << I18n.t("hubspot.travel_preference.region#{i}")
        end
      end
      regions
    end

    def industry
      industry_other
    end

    def referred_by_name_
      profile.referrals
    end

    def preferred_industries
      localize_user_industries(user)
    end

    def category
      profile.category.try(:display_name)
    end

    def category_function
      profile.category_functions.map {|f| f.display_name}
    end

    def project_type
      profile.project_types.map {|p| p.display_name}
    end

    def sub_project_type
      profile.sub_project_types.map {|s| s.display_name}
    end

    def total_work_experience
      profile.experience
    end

    def are_you_authorized_to_work_in_the_u_s_
      return "" if profile.authorization.nil?
      profile.authorization ? I18n.t('affirmative') : I18n.t('negative')
    end

    def other_metro_areas
      return "" if profile.metro_areas.nil?
      profile.metro_areas.map {|a| a.area_name}.join(", ")
    end

    def engagement_term
      terms = []
      engagement = user.professional_profile.engagement_preference
      ["term_quick", "term_small",
        "term_large", "term_permanent", "term_various"].each do |term|
        if engagement.send(term)
          terms << I18n.t("activerecord.attributes.engagement_preference.#{term}")
        end
      end
      terms
    end

    def work_type
      types = []
      schedule = user.professional_profile.schedule_preference
      ["full_time", "part_time", "contract", "internship"].each do |type|
        if schedule.send(type)
          types << I18n.t("activerecord.attributes.schedule_preference.#{type}")
        end
      end
      types
    end

    def schedule_concerns
      user.professional_profile.schedule_preference.other_concerns
    end

    def starting_date
      date_field(user.professional_profile.engagement_preference.starting_date,
        HUBSPOT_DATE_FORMAT)
    end

    def will_travel
      profile.travel_preference.will_travel
    end

    def home_frequency
      profile.travel_preference.home_frequency
    end

    def will_relocate
      profile.travel_preference.will_relocate
    end

    def travel_concerns
      profile.travel_preference.other_concerns
    end

    def travel_per_diem
      salary_data.daily_travel_comp
    end

    def salary_concerns
      salary_data.other_concerns
    end

    def skills
      skills = qualifications.skills
      return nil unless skills.present?
      skills.join(", ")
    end

    def certifications
      certs = qualifications.certs
      return nil unless certs.present?
      certs.join(", ")
    end

    def additional_certifications
      qualifications.additional_certs
    end

    def work_history
      work = []
      user.professional_profile.work_histories.order("from_date desc").each do |h|
        to_date = h.to_date.present? ? date_field(h.to_date, MONTH_YEAR) : "present"
        work << "#{date_field(h.from_date, MONTH_YEAR)} - #{to_date}"
        work << "#{h.company}" unless h.company.blank?
        work << "#{localize_industry(h.industry)}" unless h.industry.blank?
        work << "#{h.industry_other}" unless h.industry_other.blank?
        work << "#{h.position}" unless h.position.blank?
        work << "#{h.summary}" unless h.summary.blank?
        work << "===="
      end
      work.join("\n")
    end

    def education_history
      edu = []
      user.professional_profile.education_histories
        .order("graduation_date desc").each do |h|

        edu << "#{date_field(h.graduation_date, MONTH_YEAR)}"
        edu << "#{h.institution}" unless h.institution.blank?
        edu << "#{h.achievement_level}" unless h.achievement_level.blank?
        edu << "#{h.concentrations.join(', ')}" unless h.concentrations.blank?
        edu << "===="
      end
      edu.join("\n")
    end
  end
end
