module HubspotterInterface
  class ProfileSalaryForm < ProfileForm
    def form_fields
      [:hardhat_updated_at, :email, :base_salary, :hourly_rate,
        :travel_per_diem, :salary_concerns]
    end
  end
end
