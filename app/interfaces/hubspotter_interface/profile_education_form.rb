module HubspotterInterface
  class ProfileEducationForm < ProfileForm
    def form_fields
      [:hardhat_updated_at, :email, :education_history]
    end
  end
end
