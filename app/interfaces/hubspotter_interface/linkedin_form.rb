module HubspotterInterface
  class LinkedinForm < ProfileForm
    def initialize(user)
      super(user, nil, nil)
    end

    def form_fields
      [:hardhat_created_at, :hardhat_updated_at, :email, :firstname,
        :lastname, :work_history, :education_history, :country, :zip]
    end
  end
end
