# This is the token that helps us sync past users that signed up through a
# hubspot form before the webhook was in place and now need to be more
# manually synced

module HubspotterInterface
  class ReverseSyncToken < ActiveRecord::Base
    validates :token_digest, presence: true

    def self.generate(email)
      salt = SecureRandom.hex(10)
      token_digest = digest(email, salt)
      ReverseSyncToken.create({token_digest: token_digest})
      HubspotterInterface.message_verifier.generate([email, salt])
    end

    def self.retrieve_email(given_value)
      given_email, salt = HubspotterInterface.message_verifier.verify(given_value)
      token = find_by_token_digest(digest(given_email, salt))
      return nil unless token
      token.update_attribute :retrieved_at, Time.now
      return given_email

      rescue ActiveSupport::MessageVerifier::InvalidSignature => e
        Rails.logger.error "Invalid ReverseSyncToken received: #{e.message}" + \
          e.backtrace.join("\n")
        return nil
    end

    private

    def self.digest(email, salt)
      digest = Digest::SHA256.hexdigest("#{email}:#{salt}")
    end

    def self.find_by_token_digest(digest)
       ReverseSyncToken.where(token_digest: digest, retrieved_at: nil).first
    end
  end
end
