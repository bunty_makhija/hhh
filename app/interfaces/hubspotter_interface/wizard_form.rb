module HubspotterInterface
  class WizardForm < ProfileForm
    def initialize(user)
      super(user, nil, nil)
    end

    def form_fields
      [:hardhat_created_at, :hardhat_updated_at, :email, :category,
        :category_function, :preferred_industries, :project_type,
        :sub_project_type, :total_work_experience,
        :are_you_authorized_to_work_in_the_u_s_,
        :authorization_details, :other_metro_areas
      ]
    end
  end
end
