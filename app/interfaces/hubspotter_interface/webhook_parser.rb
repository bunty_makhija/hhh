module HubspotterInterface
  class WebhookParser
    attr_reader :contact_properties

    def initialize(payload)
      @contact_properties = JSON.parse(payload)["properties"]
    end

    def parse_and_find_or_create_user
      user = User.where(email: user_hash[:email]).first_or_initialize(user_hash)
      validate_and_save_user(user)
    end

    def parse_and_create_user
      validate_and_save_user(User.new(user_hash))
    end

    def validate_and_save_user(user)
      unless user.valid?
        Rails.logger.error("Failed to create user from Hubspot: " + \
          "#{user.errors.full_messages.join("\n")}")
        return nil
      end
      user.skip_confirmation!
      user.save
      set_role_preference(user.professional_profile)
      user
    end

    private

    def user_hash
      return {} unless contact_properties.has_key?("firstname")
      temporary_password = SecureRandom.base64(10)
      {
        first_name: parse_first_name,
        last_name: parse_last_name,
        email: contact_properties["email"]["value"],
        password: temporary_password,
        password_confirmation: temporary_password,
        professional_profile_attributes: profile_hash
      }
    end

    def parse_first_name
      contact_properties["firstname"] ? contact_properties["firstname"]["value"] : nil
    end

    def parse_last_name
      contact_properties["lastname"] ? contact_properties["lastname"]["value"] : nil
    end

    def profile_hash
      target = { zip_code: parse_zip_code }
      add_industry_to_profile(target)
      add_referrals_to_profile(target)
      add_hubspot_resume_to_profile(target)
      target
    end

    def parse_zip_code
      contact_properties["zip"] ? contact_properties["zip"]["value"] : nil
    end

    def add_industry_to_profile(profile_hash)
      chosen_industry = contact_properties["preferred_industries"].try(:[], "value")
      return if chosen_industry.blank?
      all_industries = I18n.t("enumerize.industry_types").flatten
      chosen_industry.split(";").each do |industry|
        key_index = all_industries.index(industry.strip)
        profile_hash[all_industries[key_index-1]] = true if key_index
      end
    end

    def add_referrals_to_profile(profile_hash)
      referred_by = contact_properties["referred_by_name_"]
      return unless referred_by
      profile_hash["referrals"] = referred_by["value"]
    end

    def add_hubspot_resume_to_profile(profile_hash)
      hubspot_resume = contact_properties["resume"]
      return unless hubspot_resume
      profile_hash["hubspot_resume_url"] = URI.escape(hubspot_resume["value"])
    end

    def set_role_preference(profile)
      chosen_function = contact_properties["function"]
      return unless chosen_function
      audience_type = AudienceType.find_by_name(chosen_function["value"])
      if audience_type
        profile.update_attribute(:role_preference_attributes, {audience_type_id: audience_type.id})
      end
    end
  end
end
