module HubspotterInterface
  class Form
    def initialize(user, token, ip)
      @user  = user
      @token = token
      @ip    = ip
    end

    def submit
      Hubspotter::Form.submit(
        form_token,
        form_data:    form_data,
        context_data: context_data)
    end
    handle_asynchronously :submit

    private

    def context_data
      { hutk: @token, ipAddress: @ip }
    end

    def form_data
      raise "Override by child"
    end

    def form_token
      raise "Override by child"
    end
  end
end
