module HubspotterInterface
  class ProfileWebhookForm < ProfileForm
    def initialize(user)
      super(user, nil, nil)
    end

    def form_fields
      [:hardhat_created_at, :hardhat_updated_at, :email]
    end
  end
end
