module HubspotterInterface
  class ProfileQualificationsForm < ProfileForm
    def form_fields
      [:hardhat_updated_at, :email, :skills, :additional_skills,
        :certifications, :additional_certifications]
    end
  end
end
