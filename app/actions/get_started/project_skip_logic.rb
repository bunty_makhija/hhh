class GetStarted::ProjectSkipLogic
  def initialize(user)
    @user = user
  end

  def attach_project_type_if_needed
    if category_without_project_type?
      project_types << buildings_facilities unless project_types.include?(buildings_facilities)
    end
  end

  def skip_page?
    exactly_this_category_and_this_project_type?
  end

  private

  def category_without_project_type?
    category == facilities_management && project_types.none? { |pt| pt == buildings_facilities }
  end

  def exactly_this_category_and_this_project_type?
    category == facilities_management && project_types == [buildings_facilities]
  end

  def project_types
    @user.professional_profile.project_types
  end

  def category
    @user.professional_profile.category
  end

  def facilities_management
    @facilities_management ||= Category.find_by(key: 'facilities_management')
  end

  def buildings_facilities
    @buildings_facilities ||= ProjectType.find_by(key: 'buildings_facilities')
  end
end
