module GetStarted
  class StepManager
    attr_reader :params

    def initialize(user, step_name=nil, params=nil)
      @user = user
      @params = params
      @requested_step = step_for(step_name).try(:new, user, params)
    end

    def self.step_for(step_name)
      if step_names.include? step_name
        "GetStarted::#{step_name.to_s.camelize}Step".constantize
      else
        nil
      end
    end
    delegate :step_for, to: :class

    def self.step_names
      step_list.map(&:slug)
    end

    def self.step_list
      [ WelcomeStep,
        IndustryStep,
        FunctionStep,
        ProjectsStep,
        SpecialtiesStep,
        AdditionalStep,
        LastThingStep,
        SetPasswordStep,
        NextStepsStep ]
    end
    delegate :step_list, to: :class

    def wizard_complete?
      @user.professional_profile.wizard_completed_at.present?
    end

    def skip_current_step?
      current_step.force_skip?
    end

    def invalid_current_step?
      any_incomplete_in?(steps_preceding(@requested_step))
    end

    def starting_step
      @starting_step ||= (first_incomplete_and_allowed_in(step_list) || step_list.last.new(@user))
    end

    def current_step
      @current_step ||= @requested_step ? @requested_step : starting_step
    end

    def previous_valid_step
      @previous_valid_step ||= first_allowed_in(steps_preceding(current_step))
    end

    def next_valid_step
      @next_valid_step ||= first_allowed_in(steps_following(current_step))
    end

    def last_step?
      current_step.class == step_list.last
    end

    private

    def steps_preceding(step)
      step_list.split(step.class).first.reverse
    end

    def steps_following(step)
      step_list.split(step.class).last
    end

    def first_incomplete_and_allowed_in(list)
      return nil if list.empty?
      first = list.find { |step| !step.new(@user).skip? }
      first ? first.new(@user) : nil
    end

    def first_allowed_in(list)
      return nil if list.empty?
      first = list.find { |step| !step.new(@user).force_skip? }
      first ? first.new(@user) : nil
    end

    def any_incomplete_in?(list)
      list.any? { |step| !step.new(@user).step_complete? }
    end
  end
end
