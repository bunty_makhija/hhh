class GetStarted::IndustrySkipLogic
  def initialize(user)
    @user = user
  end

  def skip_page?
    has_industry_but_not_linkedin?
  end

  private

  def has_industry_but_not_linkedin?
    @user.professional_profile.preferred_industries.present? && @user.provider.nil?
  end
end
