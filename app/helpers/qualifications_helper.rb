module QualificationsHelper

  def skill_options
    ActsAsTaggableOn::Tag.skills.pluck(:name).sort
  end


  def cert_options
    ActsAsTaggableOn::Tag.certs.pluck(:name).sort
  end

end
