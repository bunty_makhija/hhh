require 'uri'

module MailerHelper

  def self.tracking_param_hash(utm_source, utm_medium, utm_campaign, params = {})
    { utm_source: utm_source,
      utm_medium: utm_medium,
      utm_campaign: utm_campaign
      }.merge(params)
  end

  def self.email_tracking_params(utm_campaign, options = {})
    tracking_param_hash("hhh", "email", utm_campaign, options)
  end

  def self.email_tracking_param_string(params = {})
    URI.encode_www_form(params)
  end

end
