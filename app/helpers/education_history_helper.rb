module EducationHistoryHelper
  def concentration_options
    ActsAsTaggableOn::Tag.concentrations.pluck(:name).sort
  end
end
