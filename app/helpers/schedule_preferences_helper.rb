module SchedulePreferencesHelper

  def work_type_heading
    t("views.schedule_preferences.type_title#{@professional_profile.active? ? '' : '_inactive'}")
  end

end
