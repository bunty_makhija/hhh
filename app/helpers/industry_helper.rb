module IndustryHelper
  TYPES = %i(civil infrastructure energy building petroleum)
  TYPES_WITH_ANYTHING = %i(civil infrastructure energy building petroleum anything)
  DEFAULT = :civil

  def industries_to_data_value(industries)
    return '' unless industries
    Hash[industries.map{ |industry| [industry, true] }]
  end

  def localize_industry_list(industries)
    return '' unless industries
    industries.map{ |industry| I18n.t(industry, scope: 'enumerize.industry_types') }.join(', ')
  end

  def localize_user_industries(user)
    industries = []
    TYPES_WITH_ANYTHING.each do |industry|
      if user.professional_profile.public_send(industry)
        industries << I18n.t(industry, scope: 'activerecord.attributes.professional_profile')
      end
    end
    industries
  end

  def localize_industry(industry)
    I18n.t(industry, scope: 'activerecord.attributes.professional_profile')
  end
end
