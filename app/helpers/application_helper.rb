module ApplicationHelper

  def page_title
    content_for(:page_title) || I18n.t('site_name')
  end

  def flash_class_for(flash_key)
    (flash_key == :error || flash_key == :alert) ? 'alert' : 'success'
  end

  def featured_profiles
    CompanyProfile.featured
  end

  def enum_to_i18n_select_options(list, key_base, existing_value)
    result = list.map { |x| [I18n.t("enumerize.#{key_base}.#{x}"), x] }
    if existing_value && !list.include?(existing_value.to_sym)
      result << [existing_value, existing_value]
    end
    result
  end

end
