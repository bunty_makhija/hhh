module ProgressHelper
  def progress_width(component)
    return "width: 0%" if component.blank?
    "width: #{component.percent_complete * 100}%"
  end

  def most_complete_member(collection)
    collection.sort_by{ |member| member.percent_complete }.last
  end
end
