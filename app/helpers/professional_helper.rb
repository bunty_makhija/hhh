module ProfessionalHelper
  def edit_profile_component_path(component)
    edit_polymorphic_path [component.professional_profile, component]
  end


  def update_profile_component_path(component)
    [component.professional_profile, component]
  end

  def wizard_or_profile_link(user)
    manager = GetStarted::StepManager.new(user)
    if manager.wizard_complete?
      link_to t("navigation.profile"), edit_professional_profile_path(user.professional_profile)
    else
      link_to t("navigation.wizard_incomplete"), get_started_path(manager.current_step)
    end
  end


  def sidebar_classes_for(a_controller_name)
    controller_name == a_controller_name.to_s ? 'active' : ''
  end


  def profile_just_completed?(pro_profile)
    pro_profile.completed_at && pro_profile.completed_at.change(sec: 0) == Time.now.change(sec: 0)
  end


  def show_profile_complete_modal?(pro_profile)
    if profile_just_completed? pro_profile
      return false if session[:saw_completed_msg]
      session[:saw_completed_msg] = 1
      return true
    end

    session[:saw_completed_msg] = nil
    false
  end

  def supported_countries
    %w(US AU CA MX)
  end

  def countries_list
    Carmen::Country.all.map(&:alpha_3_code)
  end

  def supported_subregions
    country_region_lists = supported_countries.map do |country|
      Carmen::Country.coded(country).subregions.map(&:name)
    end
    country_region_lists.flatten
  end

  def subregions_list
    country_region_lists = supported_countries.map do |country|
      [ "---- #{Carmen::Country.coded(country).name} ----",
        Carmen::Country.coded(country).subregions.map(&:name) ]
    end
    country_region_lists.flatten
  end

end
