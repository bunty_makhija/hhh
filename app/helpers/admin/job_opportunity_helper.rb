module Admin::JobOpportunityHelper
  include Admin::CsvHelper

  def export_job_skills(job)
    job.skills.map(&:name).join MULTI_VALUE_SEPARATOR
  end


  def export_job_certs(profile)
    profile.certs.map(&:name).join MULTI_VALUE_SEPARATOR
  end

end
