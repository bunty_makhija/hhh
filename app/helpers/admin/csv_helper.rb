module Admin::CsvHelper
  MULTI_VALUE_SEPARATOR = ' | '


  def export_date(date)
    I18n.l date.localtime if date
  end


  def export_collection(collection)
    new_collection = []

    collection.each do |member|
      new_collection << yield(member)
    end

    new_collection.compact.join MULTI_VALUE_SEPARATOR
  end
end
