module Admin::DisplayHelper

  def safe_html_text_row(attribute, object)
    row(attribute) { object.send(attribute).try :html_safe }
  end


  def image_input(attribute, form)
    opts = { as: :file }
    object = form.object
    opts.merge! hint: form.template.image_tag(object.send("#{attribute}_url")) if object.send "#{attribute}?"

    form.input attribute, opts
    form.input "#{attribute}_cache".to_sym, as: :hidden
    form.input "remove_#{attribute}".to_sym, as: :boolean
  end


  def file_input(attribute, form)
    object = form.object
    has_attr = object.send "#{attribute}?"
    opts = { as: :file }
    opts.merge!(hint: link_to(object.send(attribute).filename, object.send(attribute).url)) if has_attr
    form.input attribute, opts

    if has_attr
      form.input "#{attribute}_cache".to_sym, as: :hidden
      form.input "remove_#{attribute}".to_sym, as: :boolean
    end
  end

  def localized_collection(enum_attr)
    Hash[enum_attr.values.map { |val| [val.text, val] }]
  end

end
