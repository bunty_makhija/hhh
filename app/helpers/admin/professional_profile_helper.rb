module Admin::ProfessionalProfileHelper
  include Admin::ProfessionalProfileExporter

  def component_attribute_names(component_class, extras=[])
    do_not_display = %i(id professional_profile_id created_at updated_at)
    names = component_class.attribute_names.map(&:to_sym)
    names.delete_if{|name| do_not_display.include? name }
    names + extras
  end

  def show_panel_for(profile, component_name)
    component = profile.send(component_name)
    panel "#{component.class.model_name.human} Details" do
      component_table_for(component)
    end
  end

  def show_collection_for(profile, collection_name)
    collection = profile.send(collection_name)
    component_class = as_class collection_name
    panel "#{component_class.model_name.human} Details" do
      collection.each { |component| component_table_for(component) }
    end
  end

  def edit_panel_for(form, component_name)
    component_class = as_class component_name
    form.inputs component_class.model_name.human, for: component_name do |f|
      component_attribute_names(component_class).each do |attr|
        opts = input_options component_class, attr
        opts = yield(opts) if block_given?
        f.input attr, opts
      end
    end
  end

  def component_table_for(component)
    component_class = component.class
    attributes_table_for component_class do
      component_attribute_names(component_class).each do |attr|
        if text_attribute?(component_class, attr)
          text_row component, attr
        else
          row(component_class.human_attribute_name(attr)) { component.send(attr) }
        end
      end
    end
  end

  def input_options(model, attribute)
    opts = {}
    opts.merge!(input_html: { class: 'tinymce-editor' }) if text_attribute? model, attribute
    opts.merge!(start_year: Time.now.year - 60, end_year: Time.now.year) if date_attribute? model, attribute
    opts
  end

  def text_attribute?(component_class, attribute)
    attribute_type(component_class, attribute) == :text
  end

  def date_attribute?(component_class, attribute)
    attribute_type(component_class, attribute) == :date
  end

  def attribute_type(component_class, attribute)
    component_class.columns_hash[attribute.to_s].type
  end

  def text_row(component, attr)
    if component.respond_to? attr
      row(attr) { component.send(attr).try :html_safe }
    else
      row attr
    end
  end

  def as_class(component_name)
    component_name.to_s.classify.constantize
  end

end
