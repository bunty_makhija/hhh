module Admin::ProfessionalProfileExporter
  include Admin::CsvHelper


  def export_skills(profile)
    profile.qualification.skills.map(&:name).join(MULTI_VALUE_SEPARATOR) if profile.qualification
  end


  def export_certs(profile)
    profile.qualification.certs.map(&:name).join(MULTI_VALUE_SEPARATOR) if profile.qualification
  end


  def export_education(profile)
    histories = []
    profile.education_histories.each {|edu| histories << edu.to_s }
    histories.join MULTI_VALUE_SEPARATOR
  end


  def export_work_history(profile)
    histories = []
    profile.work_histories.each{|wrk| histories << wrk.to_s }
    histories.join MULTI_VALUE_SEPARATOR
  end


  def export_industries(profile)
    collection = IndustryHelper::TYPES

    export_collection collection do |member|
      ProfessionalProfile.human_attribute_name(member) if profile.send("#{member}?")
    end
  end


  def export_engagement_preferences(profile)
    collection = %i(term_quick term_small term_large term_permanent term_various)

    export_collection collection do |member|
      EngagementPreference.human_attribute_name(member) if profile.engagement_preference.send("#{member}?")
    end
  end


  def export_travel_regions(profile)
    export_collection (1..10) do |i|
      attr = "region#{i}"
      TravelPreference.human_attribute_name(attr) if profile.travel_preference.send("#{attr}?")
    end
  end


  def export_schedule_preferences(profile)
    collection = %i(full_time part_time contract internship)

    export_collection collection do |member|
      SchedulePreference.human_attribute_name(member) if profile.schedule_preference.send("#{member}?")
    end
  end

end
