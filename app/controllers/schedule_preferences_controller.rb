class SchedulePreferencesController < ProfessionalController
  include ProfessionalProfileComponentController

  def update
    super
    @hubspot_form = HubspotterInterface::ProfileScheduleForm.new(current_user,
      current_user_hubspot_token, current_user_ip)
  end

  resource_params %i(
    part_time
    full_time
    contract
    internship
    other_concerns )
end
