class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def linkedin
    if current_user
      updater = UserLinkedinUpdater.new(
          current_user, request.env["omniauth.auth"], limit_to)
      if updater.update
        flash[:success] = t("linkedin.success")
      else
        flash[:error] = t("linkedin.duplicate")
      end
      redirect_to redirect_path_from_limit
    else
      linkedin_user = UserLinkedinUpdater.create_from_omniauth(
        request.env["omniauth.auth"])
      update_hubspot(linkedin_user) if linkedin_user.persisted?
      post_create_behavior(linkedin_user)
    end
  end

  def failure
    flash[:error] = t('linkedin.failure')
    if current_user
      redirect_to redirect_path_from_limit
    else
      redirect_to new_user_registration_path
    end
  end

  private

  def limit_to
    request.env["omniauth.params"]["limit_to"]
  end

  def redirect_path_from_limit
    case limit_to
    when "education"
      professional_profile_education_histories_path(current_user.professional_profile.id)
    when "work_history"
      professional_profile_work_histories_path(current_user.professional_profile.id)
    when "skills"
      edit_professional_profile_qualification_path(current_user.professional_profile.id, current_user.professional_profile.id)
    when "wizard"
      get_started_path(:next_steps)
    else
      edit_professional_profile_path(current_user.id)
    end
  end

  def update_hubspot(user)
    form = HubspotterInterface::LinkedinForm.new(user)
    form.submit
  end

  def post_create_behavior(new_user)
    if new_user.confirmed?
      sign_in_and_redirect new_user, :event => :authentication #this will throw if @user is not activated
    elsif new_user.persisted?
      sign_in new_user
      redirect_to get_started_index_path, flash: { render_conversion_scripts: true }
    else
      flash[:error] = new_user.errors.full_messages.first
      redirect_to new_user_registration_url
    end
  end

end
