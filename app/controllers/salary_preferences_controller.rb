class SalaryPreferencesController < ProfessionalController
  include ProfessionalProfileComponentController

  def update
    super
    @hubspot_form = HubspotterInterface::ProfileSalaryForm.new(current_user,
      current_user_hubspot_token, current_user_ip)
  end

  resource_params %i(
    base_salary
    hourly_rate
    daily_travel_comp
    other_concerns )
end
