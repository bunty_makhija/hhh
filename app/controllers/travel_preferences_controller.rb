class TravelPreferencesController < ProfessionalController
  include ProfessionalProfileComponentController

  def update
    super
    @hubspot_form = HubspotterInterface::ProfileTravelForm.new(current_user,
      current_user_hubspot_token, current_user_ip)
  end

  resource_params %i(
    will_travel
    will_relocate
    home_frequency
    region1
    region2
    region3
    region4
    region5
    region6
    region7
    region8
    region9
    region10
    other_concerns )
end
