class ProfessionalProfilesController < ProfessionalController
  before_action :redirect_if_wizard_not_completed, only: :edit

  def edit
    @professional_profile_form = ProfessionalProfileForm.new(current_user.professional_profile)
  end

  def update
    @hubspot_form = HubspotterInterface::ProfileEngagementForm.new(
      current_user, current_user_hubspot_token, current_user_ip)
    @professional_profile_form = ProfessionalProfileForm.new(
      current_user.professional_profile, permitted_params)
    @professional_profile_form.process
    if @professional_profile_form.errors.present?
      flash[:error] = @professional_profile_form.errors.full_messages.to_sentence
    else
      flash[:success] = t("views.professional_profiles.updated")
    end
    redirect_to edit_professional_profile_path(@professional_profile)
  end

  def resume
    if @professional_profile.resume?
      resume = @professional_profile.resume
      download_name = @professional_profile.resume_original_filename
      download_name = download_name.blank? ? resume.path.split("/").last : download_name
      send_data(resume.read, filename: download_name)
    else
      render nothing: true
    end
  end

  def send_confirmation
    flash[:success] = t("devise.confirmations.send_instructions")
    current_user.resend_confirmation_instructions
    redirect_to edit_professional_profile_path(current_user)
  end

  private

  def permitted_params
    engagement_pref_attrs = %i(starting_date
                               term_quick
                               term_small
                               term_large
                               term_permanent
                               term_various
                               other_concerns)

    params.require(:professional_profile_form).permit(
      :status,
      :country,
      :state,
      :city,
      :zip_code,
      :civil,
      :infrastructure,
      :energy,
      :building,
      :petroleum,
      :anything,
      :industry_other,
      :resume,
      :resume_cache,
      :remove_resume,
      :first_name,
      :last_name,
      :email,
      :phone_number,
      :password,
      :password_confirmation,
      role_preference_attributes: [ :audience_type_id ],
      engagement_preference_attributes: engagement_pref_attrs)
  end

  private

  def redirect_if_wizard_not_completed
    step_manager = GetStarted::StepManager.new(current_user)
    redirect_to get_started_path(step_manager.starting_step) unless step_manager.wizard_complete?
  end
end
