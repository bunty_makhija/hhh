module ProfessionalProfileComponentController
  extend ActiveSupport::Concern

  def update
    super do |format|
      format.html { redirect_to edit_polymorphic_path([@professional_profile, resource]) }
    end
  end

  module ClassMethods
    def resource_params(*attrs)
      define_method :build_resource_params do
        [params.fetch(controller_name.singularize.to_sym, {}).permit(attrs)]
      end
    end
  end

  protected

  def resource
    resource_name = controller_name.singularize
    instance_variable_set("@#{resource_name}", current_user.professional_profile.send(resource_name))
  end
end
