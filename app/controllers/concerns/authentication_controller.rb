module AuthenticationController
  extend ActiveSupport::Concern

  included do
    before_filter :configure_permitted_parameters, if: :devise_controller?
  end


  def after_sign_in_path_for(resource)
    stored_location_for(resource) || after_sign_in_path_by_role(resource)
  end


  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).tap do |list|
      list << :first_name
      list << :last_name
      list << :phone_number
    end
  end


  def after_sign_in_path_by_role(resource)
    return admin_root_path if resource.has_any_role?
    get_started_index_path
    # TODO: check for employer type/role
  end
end
