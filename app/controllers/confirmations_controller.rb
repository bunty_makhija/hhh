class ConfirmationsController < Devise::ConfirmationsController
  after_action :send_welcome_email, only: :show

  private

  def send_welcome_email
    ProfessionalMailer.welcome_user(resource).deliver if resource.confirmed?
  end
end
