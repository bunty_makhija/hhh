class WorkHistoriesController < ProfessionalController

  # Skip because resource loading because CanCan does not
  # play nice with InheritedResources' collection loading.
  # https://github.com/ryanb/cancan/wiki/Inherited-Resources
  skip_load_resource
  after_filter :submit_hubspot_form, only: [:update, :destroy]

  def create
    @professional_profile.work_histories.create!(build_resource_params)
    redirect_to professional_profile_work_histories_path
  end

  def update
    super do |format|
      format.html { redirect_to professional_profile_work_histories_path }
    end
  end

  def destroy
    super do |format|
      format.html { redirect_to professional_profile_work_histories_path }
    end
  end

  def submit_hubspot_form
    @hubspot_form = HubspotterInterface::ProfileWorkForm.new(current_user,
      current_user_hubspot_token, current_user_ip)
    super
  end

  private

  def resource
    @work_history ||= current_user.professional_profile.work_histories.find(params[:id])
  end

  def collection
    @work_histories ||= current_user.professional_profile.work_histories.order(:created_at)
  end

  def build_resource_params
    [params.fetch(:work_history, {}).permit(%i(
      company
      industry
      industry_other
      from_date
      to_date
      current
      position
      summary ))]
  end
end
