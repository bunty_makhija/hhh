# This controller is set up to receive requests from the Hubspot system
# and serves to sync up user data in the direction of Hubspot -> rails

require 'hubspot_import/hubspot_client'

class WebhookUsersController < ApplicationController
  skip_before_filter :verify_authenticity_token
  layout 'application'

  # Landing pages managed by Hubspot that register users are hosted at
  # hardhathub.com and will redirect to here on form submit.
  # Hubspot landing pages drop a cookie onto the browser that links back to
  # the registered user, so we use the cookie value to look up the new user
  # data on Hubspot and create them on the rails side. We then log them in and
  # drop them into the wizard.
  def register
    hutk = current_user_hubspot_token
    contact_payload = hubspot_contact(hutk, :cookie) if hutk
    user = contact_payload ? find_or_create_user(contact_payload) : nil
    prepare_for_wizard(user)
  end

  # Landing pages managed by Unbounce are hosted at hardhathub.net or hardhathub.com
  # and they will POST to here on form submit.
  def register_from_post
    render status: 400, nothing: true and return unless valid_post?
    user = find_or_create_user_from_post
    prepare_for_wizard(user)
  end

  # Used by Hubspot Reverse Sync Workflow, keeping around so that we have
  # a hook in place if necessary
  def create
    params.require("properties")
    render status: 400, nothing: true and return unless valid_request?

    parser = HubspotterInterface::WebhookParser.new(request.body.read)
    user = parser.parse_and_create_user
    render status: 400, nothing: true and return unless user

    user.send_reset_password_instructions
    set_hardhat_dates_in_hubspot(user)
    render nothing: true
  end

  # Used to sync users signed up previous to activation of Hubspot Workflow.
  # This endpoint should be removed at some point in the future
  def activate
    token_value = URI.unescape(params[:token])
    email = HubspotterInterface::ReverseSyncToken.retrieve_email(token_value)
    render 'activate_error' and return unless email

    contact_payload = hubspot_contact(email, :email)
    user = user_from_contact(contact_payload)
    if user
      user.send_reset_password_instructions
      set_hardhat_dates_in_hubspot(user)
      render 'activate_success'
    else
      render 'activate_error'
    end
  end

  private

  def valid_request?
    good_agent = Rails.env.development? ? true : request.user_agent.start_with?("HubSpot Connect")
    good_agent && params["portal-id"].eql?(Settings.hubspot.portal_id)
  end

  # Since we're allowing a POST from a different domain, we need to put some
  # safeguards in to verify that we trust the request.
  def valid_post?
    referrer = URI.parse(request.referrer)
    pageId = params[:pageId]
    (referrer.host.end_with?(".hardhathub.net") ||
      referrer.host.end_with?(".hardhathub.com")) &&
        pageId.try(:match, /[a-z0-9]{8}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{12}/)
  end

  # We tried fetching the user from hubspot but we can't guarantee that the
  # contact will have been created by the time we get here.
  def find_or_create_user_from_post
    profile = { zip_code: params[:postal_code] }
    set_industry_flag(profile)
    temporary_password = SecureRandom.base64(10)
    user_hash = {
      first_name: params[:first_name],
      last_name: params[:last_name],
      email: params[:email],
      password: temporary_password,
      password_confirmation: temporary_password,
      professional_profile_attributes: profile
    }
    user = User.where(email: user_hash[:email]).first_or_initialize(user_hash)
    user.skip_confirmation!
    user.save
    user
  end

  def set_industry_flag(profile_hash)
    industry = params[:which_industry_are_you_in] ? params[:which_industry_are_you_in] : params[:industry]
    if industry
      all_industries = I18n.t("enumerize.industry_types").flatten
      key_index = all_industries.index(industry.strip)
      profile_hash[all_industries[key_index-1]] = true if key_index
    end
  end

  def prepare_for_wizard(user)
    if user.present? && user.professional_profile.wizard_completed_at.nil?
      sign_in user
      redirect_to get_started_index_path, flash: { render_conversion_scripts: true }
    elsif user.present?
      redirect_to new_user_session_path
    else
      flash[:error] = I18n.t('hubspot.registration.failed')
      redirect_to root_path
    end
  end

  def set_hardhat_dates_in_hubspot(user)
    form = HubspotterInterface::ProfileWebhookForm.new(user)
    form.submit
  end

  def find_or_create_user(payload)
    parser = HubspotterInterface::WebhookParser.new(payload)
    parser.parse_and_find_or_create_user
  end

  def user_from_contact(payload)
    parser = HubspotterInterface::WebhookParser.new(payload)
    parser.parse_and_create_user
  end

  def hubspot_contact(search_term, term_type)
    begin
      response = HubspotImport::HubspotClient.send("fetch_contact_with_#{term_type}!", search_term)
      response.body.force_encoding("UTF-8")
    rescue Exception=>e
      Rails.logger.error "Could not retrieve contact from Hubspot using #{search_term}: #{e.message}\n" + \
        "#{e.backtrace.join("\n")}"
      return nil
    end
  end
end
