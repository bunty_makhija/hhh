class SessionsController < Devise::SessionsController
  after_action :send_to_hubspot, :only => [:create]

  private

  def send_to_hubspot
    if current_user
      hs_form = HubspotterInterface::LoginForm.new(current_user.email,
                                                    current_user_hubspot_token,
                                                    current_user_ip)
      hs_form.submit
    end
  end
end
