class EducationHistoriesController < ProfessionalController

  # Skip because resource loading because CanCan does not
  # play nice with InheritedResources' collection loading.
  # https://github.com/ryanb/cancan/wiki/Inherited-Resources
  skip_load_resource
  after_filter :submit_hubspot_form, only: [:update, :destroy]

  def create
    @professional_profile.education_histories.create!(build_resource_params)
    redirect_to professional_profile_education_histories_path
  end

  def update
    resource.concentration_list = params[:education_history][:concentration_list]
    super do |format|
      format.html { redirect_to professional_profile_education_histories_path }
    end
  end

  def destroy
    super do |format|
      format.html { redirect_to professional_profile_education_histories_path }
    end
  end

  def submit_hubspot_form
    @hubspot_form = HubspotterInterface::ProfileEducationForm.new(current_user,
      current_user_hubspot_token, current_user_ip)
    super
  end

  private

  def resource
    @education_history ||= current_user.professional_profile.education_histories.find(params[:id])
  end

  def collection
    @education_histories ||= current_user.professional_profile.education_histories.order(:created_at)
  end

  def build_resource_params
    [params.fetch(:education_history, {}).permit(%i(
      achievement_level
      graduation_date
      institution
      collection_list ))]
  end
end
