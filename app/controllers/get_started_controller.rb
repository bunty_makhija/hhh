require 'hubspot_import/hubspot_client'

class GetStartedController < ApplicationController
  include Wicked::Wizard
  steps *GetStarted::StepManager.step_names

  before_filter :authenticate_user!
  before_filter :load_step_and_manager
  before_filter :skip_wizard_if_completed
  before_filter :redirect_to_valid_step, only: :show
  before_filter :skip_step_unless_required, only: :show
  before_filter :set_completed, only: :show
  layout "wizard"

  def index
    redirect_to get_started_path(@step_manager.starting_step)
  end

  def show
    render_wizard
  end

  def update
    if @current_step.slug == :set_password
      handle_password_step
    else
      save_and_move_on
    end
  end

  private

  def save_and_move_on
    if params[:submit_prev]
      @current_step.save
      redirect_to wizard_path(@step_manager.previous_valid_step)
    else
      render_wizard @current_step
    end
  end

  def handle_password_step
    @wizard_user = current_user
    save_and_move_on
    sign_in @wizard_user.reload, bypass: true
  end

  def load_step_and_manager
    @step_manager = GetStarted::StepManager.new(current_user, step, params)
    @current_step = @step_manager.current_step
  end

  def skip_wizard_if_completed
    redirect_to edit_professional_profile_path(current_user) if @step_manager.wizard_complete?
  end

  def skip_step_unless_required
    redirect_to wizard_path(@step_manager.next_valid_step) if @step_manager.skip_current_step?
  end

  def redirect_to_valid_step
    redirect_to get_started_index_path if @step_manager.invalid_current_step?
  end

  def set_completed
    current_user.professional_profile.touch(:wizard_completed_at) if @step_manager.last_step?
  end

end
