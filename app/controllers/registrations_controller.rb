class RegistrationsController < Devise::RegistrationsController
  #
  # By default Devise doesn't allow logged in users to visit the
  # registration or sign in pages. Since our home page is set to
  # the registration page we don't want that behavior. By wiring
  # up our own RegistrationsController we can override the
  # before_filter that causes it.
  prepend_before_filter :require_no_authentication, only: :none

  before_filter :load_professional_profile, only: [:edit, :update]

  def new
    @user_registration_form = UserRegistrationForm.new
  end

  def create
    @user_registration_form = UserRegistrationForm.new(
      params,
      hubspot_user_token: current_user_hubspot_token,
      ip: current_user_ip)
    if @user_registration_form.save
      sign_in @user_registration_form.user
      redirect_to get_started_index_path, flash: { render_conversion_scripts: true }
    else
      flash.now[:error] = t("views.shared.invalid_form_fields")
      render :new
    end
  end

  def update
    @user = current_user

    if linkedin_user?(@user)
      params[:user].delete(:current_password)
      @user.update_without_password(devise_parameter_sanitizer.sanitize(:account_update))
    else
      @user.update_with_password(devise_parameter_sanitizer.sanitize(:account_update))
    end

    if @user.errors.empty?
      set_flash_message :notice, :updated_email
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end

  private

  def linkedin_user?(user)
    user.uid.present?
  end

  def load_professional_profile
    @professional_profile = current_user.professional_profile
  end

  def after_update_path_for(resource)
    edit_user_registration_path || root_path
  end

end
