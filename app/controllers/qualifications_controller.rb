class QualificationsController < ProfessionalController
  include ProfessionalProfileComponentController

  def update
    @hubspot_form = HubspotterInterface::ProfileQualificationsForm.new(current_user,
      current_user_hubspot_token, current_user_ip)
    resource.skill_list = params[:qualification][:skill_list]
    resource.cert_list = params[:qualification][:cert_list]
    super
  end

  resource_params %i(
    cert_list
    skill_list
    additional_certs
    additional_skills
  )
end
