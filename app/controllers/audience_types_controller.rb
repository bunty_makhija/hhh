class AudienceTypesController < ApplicationController
  def index
    @audience_types = AudienceType.published.positioned
    @featured_companies = CompanyProfile.featured
  end

  def show
    @audience_type = AudienceType.published.friendly.find(params[:id])
    @user_registration_form = UserRegistrationForm.new
    @featured_audience_profiles = AudienceProfile.published.featured.where(audience_type: @audience_type)
    @unfeatured_audience_profiles = AudienceProfile.published.unfeatured.where(audience_type: @audience_type)
    @featured_companies = CompanyProfile.consulting_partners_of_type(@audience_type.id)
  end

end
