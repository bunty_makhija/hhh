class ApplicationController < ActionController::Base
  include AuthenticationController

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def log_error(exception)
    logger.error exception

    begin
      NewRelic::Agent.agent.error_collector.notice_error exception
    rescue ArgumentError
    end
  end

  def self.declare_rescue_behavior
    rescue_from Exception, RuntimeError, with: :render_500
    rescue_from CanCan::AccessDenied, with: :access_denied
    rescue_from ActionView::MissingTemplate, with: :render_500
    rescue_from ActionController::UnknownController, with: -> { render_error(404) }
    rescue_from ActionController::RoutingError, with: -> { render_error(404) }
    rescue_from ActiveRecord::RecordNotFound, with: -> { render_error(404) }
    rescue_from Wicked::Wizard::InvalidStepError, with: -> { render_error(404) }
  end
  declare_rescue_behavior unless Rails.application.config.consider_all_requests_local

  def render_500(exception)
    log_error exception
    render_error 500
  end

  def render_error(status)
    render(
      template: 'error',
      status: status,
      layout: 'application',
      content_type: Mime::HTML,
      formats: [:html],
      handlers: [:haml]
    )
  end

  def access_denied(exception)
    sign_out current_user
    redirect_to new_user_session_path, alert: exception.message
  end

  def handle_unknown_url
    redirect_to Redirect.find_by_old_path!(request.fullpath).new_url, status: 301
  rescue
    render_error 404
  end

  def current_user_hubspot_token
    cookies[:hubspotutk]
  end

  def current_user_ip
    request.remote_ip
  end
end
