class ProfessionalController < InheritedResources::Base
  before_filter :authenticate_user!, :load_professional_profile
  after_filter :submit_hubspot_form, only: [:update]

  load_and_authorize_resource

  def load_professional_profile
    @professional_profile = current_user.professional_profile
  end

  def submit_hubspot_form
    @hubspot_form.try(:submit)
  end
end
