class CompanyProfilesController < ApplicationController
  before_action :find_profile, only: :show

  def index
  end

  def show
  end


  private

  def find_profile
    id = params[:id]
    @company_profile = CompanyProfile.friendly.find id
    redirect_to @company_profile if id =~ /\d+/
  end
end
