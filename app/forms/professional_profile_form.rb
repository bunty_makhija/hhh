class ProfessionalProfileForm
  extend Enumerize
  include ActiveModel::Model

  enumerize :status,
    in: %i(active passive inactive)

  validates :first_name, :last_name, :email, presence: true
  validates :first_name, :last_name, :phone_number, length: { maximum: 255 }
  validates :country, :state, :city, :zip_code, :industry_other, length: { maximum: 255 }

  attr_accessor :status, :country, :state, :city, :zip_code, :industry_other, :resume
  attr_accessor :civil, :infrastructure, :energy, :building, :petroleum, :anything
  attr_accessor :first_name, :last_name, :email, :phone_number, :password, :password_confirmation
  attr_accessor :professional_profile, :user

  delegate :status, :role_preference, :engagement_preference, :industry_other, :resume, :remove_resume, :resume_cache, to: :professional_profile
  delegate :civil, :infrastructure, :energy, :building, :petroleum, :anything, to: :professional_profile
  delegate :country, :state, :city, :zip_code, to: :professional_profile
  delegate :first_name, :last_name, :email, :phone_number, :password, :password_confirmation, to: :user

  def initialize(profile, params={})
    @professional_profile = profile
    @user = professional_profile.user
    @params = params
  end

  def process
    professional_profile.update(profile_params)

    if @params['remove_resume'] == '1'
      professional_profile.remove_resume = true
      professional_profile.save
    end

    unless user.update(user_params)
      errors.messages.merge!(professional_profile.errors.messages)
      errors.messages.merge!(user.errors.messages)
    end
  end

  private

  def profile_params
    @params.slice(
      :status,
      :country,
      :state,
      :city,
      :zip_code,
      :civil,
      :energy,
      :building,
      :infrastructure,
      :petroleum,
      :anything,
      :industry_other,
      :resume,
      :role_preference_attributes,
      :engagement_preference_attributes).to_hash
  end

  def user_params
    attrs = [:first_name, :last_name, :email, :phone_number]
    attrs += [:password, :password_confirmation] if @params[:password].present?
    @params.slice(*attrs)
  end
end
