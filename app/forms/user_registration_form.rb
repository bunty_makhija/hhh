class UserRegistrationForm
  include ActiveModel::Model
  include ActiveRecord::AttributeAssignment

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :zip_code, presence: true
  validates :country, presence: true
  validates :preferred_industries, presence: true

  attr_accessor :user
  PERMITTED_USER_PARAMS = %i(first_name last_name email)

  attr_accessor *PERMITTED_USER_PARAMS
  attr_accessor *IndustryHelper::TYPES
  attr_accessor :zip_code, :country

  def initialize(params={}, hubspot_user_token=nil, ip=nil)
    @params = params

    assign_attributes user_params
    assign_attributes profile_params

    @hs_user_token = hubspot_user_token
    @ip = ip
  end

  def save
    valid? ? create_user_and_profile : false
  end

  private

  def create_user_and_profile
    User.transaction do
      @user = User.new(build_user_params).tap do |new_user|
        new_user.skip_confirmation_notification!
      end

      if @user.save && @user.professional_profile.persisted?
        send_to_hubspot
      else
        raise ActiveRecord::Rollback
      end
    end

    collect_errors
    @user.persisted?
  end

  def collect_errors
    errors.messages.merge! user.errors.messages
    errors.messages.merge! user.professional_profile.errors.messages
  end

  def build_user_params
    uparams = user_params.merge!({professional_profile_attributes: profile_params})
    temporary_password = SecureRandom.base64(10)
    uparams.merge!({password: temporary_password, password_confirmation: temporary_password})
  end

  def safe_params
    @safe_params ||= @params.fetch(:user_registration_form, ActionController::Parameters.new)
  end

  def user_params
    safe_params.permit(*PERMITTED_USER_PARAMS)
  end

  def profile_params
    safe_params.permit(*IndustryHelper::TYPES, :zip_code, :country)
  end

  def send_to_hubspot
    hs_form = HubspotterInterface::SignupForm.new(@user, @hs_user_token, @ip)
    hs_form.submit
  end

  def preferred_industries
    IndustryHelper::TYPES.select { |industry| send(industry) != "0" }
  end
end
