class ResumeUploader < CarrierWave::Uploader::Base
  include BaseUploader
  before :cache, :save_original_filename

  def extension_white_list
    %w(doc docx pdf rtf txt odt html)
  end

  def filename
    if file.present?
      "#{unique_token}.#{file.extension}"
    end
  end

  def fog_public
    true
  end

  protected

  def save_original_filename(file)
    model.resume_original_filename = file.original_filename if file.respond_to?(:original_filename)
  end

  def unique_token
    Digest::MD5.hexdigest("#{model.id}-#{model.created_at}-#{model.resume_original_filename}")
  end
end
