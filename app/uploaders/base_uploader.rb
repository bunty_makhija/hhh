module BaseUploader
  extend ActiveSupport::Concern

  included do
    storage Settings.send(Rails.env).file_storage.to_sym
  end
end
