class ImageUploader < CarrierWave::Uploader::Base
  include BaseUploader

  def extension_white_list
     %w(jpg jpeg gif png)
  end
end
