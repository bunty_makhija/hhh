class Image < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true

  mount_uploader :attachment, ImageUploader

  validates :attachment, presence: true
end

# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  imageable_type :string(255)
#  imageable_id   :integer
#  attachment     :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#
