class UserLinkedinUpdater < SimpleDelegator

  attr_accessor :auth, :limit_to

  def self.create_from_omniauth(auth)
    User.where(auth.slice(:provider, :uid)).first_or_create do |user|
      user.basic_linkedin_data(auth)
      user.skip_confirmation!
      if user.save
        user.create_professional_profile
        UserLinkedinUpdater.new(user, auth).update
      end
    end
  end

  def initialize(user, auth, limit_to = nil)
    super(user)
    @auth = auth
    @limit_to = limit_to
  end

  def raw_info
    auth.extra.raw_info
  end

  def update
    return false unless update_to_linkedin_user
    update_education
    update_work_history
    update_skills
    update_summary
    update_phone_number
    return true
  end

  def update_for?(feature)
    return true if limit_to.nil? || limit_to == "wizard"
    limit_to == feature
  end

  def update_education
    return unless update_for?("education")
    education = raw_info.educations.values_at("values").first
    self.professional_profile.education_history_from_linkedin(education)
  end

  def update_work_history
    return unless update_for?("work_history")
    work_history = raw_info.positions.values_at("values").first
    self.professional_profile.work_history_from_linkedin(work_history)
  end

  def update_skills
    return unless update_for?("skills")
    professional_profile.add_skills(raw_info.skills)
    professional_profile.add_certifications(raw_info.certifications)
  end

  def update_summary
    professional_profile.update_attributes(summary: auth.extra.raw_info.summary)
  end

  def update_phone_number
    phone_hash = auth.extra.raw_info.phoneNumbers
    if phone_hash.present? && phone_hash[:_total] > 0
      professional_profile.user.update_attributes(
        phone_number: phone_hash.values_at("values").first.first.phoneNumber)
    end
  end

  def update_to_linkedin_user
    professional_profile.user.update(provider: auth.provider, uid: auth.uid)
  end
end
