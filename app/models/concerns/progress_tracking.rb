module ProgressTracking
  extend ActiveSupport::Concern

  def percent_complete
    progress_conditions_completed.fdiv progress_conditions.length
  end

  def complete?
    progress_conditions_completed == progress_conditions.length
  end

  private

  def progress_conditions_completed
    progress_conditions.count(true)
  end
end
