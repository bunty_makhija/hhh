module ProfessionalProfileComponent
  extend ActiveSupport::Concern

  included do
    belongs_to :professional_profile
    validates :professional_profile, presence: true
    after_save :check_completeness
  end

  def user
    professional_profile.user
  end


  private

  def check_completeness
    if professional_profile.completed_at.nil? && professional_profile.complete?
      professional_profile.update_attribute :completed_at, Time.now
    end
  end
end
