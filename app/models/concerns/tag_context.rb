#
# Augments +ActsAsTaggableOn::Tag+ to provide predefined context support
# Is mixed in via initializer
module TagContext
  extend ActiveSupport::Concern

  PREDEFINED_CONTEXTS = {
    skill: 'skill',
    cert: 'cert',
    concentration: 'concentration'
  }

  module ClassMethods
    ::TagContext::PREDEFINED_CONTEXTS.keys.each do |key|
      define_method key.to_s.pluralize do
        where(original_context: ::TagContext::PREDEFINED_CONTEXTS[key])
      end
    end
  end
end
