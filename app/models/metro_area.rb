class MetroArea < ActiveRecord::Base
  has_and_belongs_to_many :professional_profiles

  validates :area_name, presence: true, uniqueness: true
  validates :city, presence: true
  validates :state, presence: true

end

# == Schema Information
#
# Table name: metro_areas
#
#  id         :integer          not null, primary key
#  area_name  :string(255)
#  city       :string(255)
#  state      :string(255)
#  created_at :datetime
#  updated_at :datetime
#
