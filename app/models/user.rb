class User < ActiveRecord::Base
  rolify

  has_one :professional_profile, dependent: :destroy, inverse_of: :user

  delegate :role_preference, to: :professional_profile

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable,
         omniauth_providers: [:linkedin]

  validates :first_name, presence: true
  validates :last_name,  presence: true
  validates :uid,        uniqueness: true, allow_nil: true

  accepts_nested_attributes_for :professional_profile

  after_create :add_professional_profile

  def full_name
    "#{first_name} #{last_name}"
  end

  def basic_linkedin_data(auth)
    raw_info        = auth.extra.raw_info
    self.email      = raw_info.emailAddress
    self.password   = Devise.friendly_token[0, 20]
    self.first_name = raw_info.firstName
    self.last_name  = raw_info.lastName
  end

  def hubspot_created?
    confirmed? == true && confirmation_token.nil? == true && sign_in_count == 0
  end

  private

  def add_professional_profile
    build_professional_profile.save! if professional_profile.nil?
  end

end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  first_name             :string(255)      not null
#  last_name              :string(255)      not null
#  phone_number           :string(255)
#  provider               :string(255)
#  uid                    :string(255)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
