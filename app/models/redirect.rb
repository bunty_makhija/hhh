class Redirect < ActiveRecord::Base
  validates :old_path, :new_url, presence: true

  before_save :downcase_uris


  private

  def downcase_uris
    old_path.downcase!
    new_url.downcase!
  end
end

# == Schema Information
#
# Table name: redirects
#
#  id       :integer          not null, primary key
#  old_path :string(255)
#  new_url  :string(255)
#
