module UserLinkedinClient

  def self.get
    @client ||= LinkedIn::Client.new(Settings.linkedin_consumer_key,
      Settings.linkedin_consumer_secret) #LinkedIn Gem Client
  end

  def self.industry_for_company(company_id)
    return "" unless company_id
    company = get.company(id: company_id, fields: ["name", "industries"])
    company["industries"]["all"].first["name"]
  rescue LinkedIn::Errors::NotFoundError
    Rails.logger.error("LinkedIn::Errors::NotFoundError company_id: #{company_id}")
    "" #Satisfies non-null db constraint for industry
  end

end
