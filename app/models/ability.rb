#
# Permissions defined at
# https://docs.google.com/a/tablexi.com/spreadsheet/ccc?key=0AvvUJBEhQKkedHloWFhCOGg5OVU2R20xazhHbFU0alE&usp=drive_web#gid=0
class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    if user.is_admin?
      can :manage, :all
    elsif user.is_profile_viewer?
      grant_profile_viewing_rights
    elsif user.is_profile_manager?
      grant_profile_management_rights
    else
      grant_professional_rights user
    end
  end


  def grant_basic_admin_rights
    can :read, User
    can :read, ActiveAdmin::Page, name: 'Dashboard'
  end


  def grant_profile_management_rights
    grant_basic_admin_rights
    can :manage, professional_resources + company_resources
  end


  def grant_profile_viewing_rights
    grant_basic_admin_rights
    can :read, professional_resources + company_resources
    can :resume, ProfessionalProfile
  end


  def grant_professional_rights(user)
    can %i(read update), ProfessionalProfile, user_id: user.id
    can %i(resume send_confirmation), ProfessionalProfile, user_id: user.id
    can %i(read update), professional_profile_components, professional_profile_id: user.professional_profile.id
    can %i(create destroy), professional_profile_collection_components, professional_profile_id: user.professional_profile.id
  end


  def company_resources
    [ CompanyProfile, JobOpportunity ]
  end


  def professional_resources
    professional_profile_components + [ ProfessionalProfile, ActsAsTaggableOn::Tag ]
  end


  def professional_profile_components
    professional_profile_member_components + professional_profile_collection_components
  end


  def professional_profile_member_components
    [
      EngagementPreference,
      Qualification,
      SalaryPreference,
      SchedulePreference,
      TravelPreference,
    ]
  end


  def professional_profile_collection_components
    [ EducationHistory, WorkHistory ]
  end

end
