class AudienceProfile < ActiveRecord::Base
  belongs_to :audience_type

  validates :audience_type_id, :title, :bio, presence: true

  mount_uploader :primary_image, ImageUploader

  acts_as_taggable_on :skills, :certs

  def self.published
    where(published: true)
  end

  def self.unfeatured
    where(featured: false)
  end

  def self.featured
    where(featured: true)
  end

  def all_tags
    skills + certs
  end
end

# == Schema Information
#
# Table name: audience_profiles
#
#  id               :integer          not null, primary key
#  audience_type_id :integer
#  featured         :boolean          default(FALSE)
#  title            :string(255)
#  bio              :text
#  primary_image    :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  published        :boolean          default(FALSE)
#  name             :string(255)
#
# Indexes
#
#  index_audience_profiles_on_audience_type_id  (audience_type_id)
#
