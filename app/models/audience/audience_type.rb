class AudienceType < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  POSITIONS = 1..Settings.audience_type_positions

  has_many :audience_profiles

  validates :name, :description, presence: true
  validates :position, inclusion: { in: POSITIONS }
  validates :slug, presence: true, uniqueness: true

  mount_uploader :primary_image, ImageUploader

  def self.published
    where(published: true)
  end

  def self.positioned
    order(:position)
  end

  def is_project_manager?
    self.name == "Project Managers & Superintendents"
  end

  def is_facility_manager?
    self.name == "Facility Managers & Building Engineers"
  end
end

# == Schema Information
#
# Table name: audience_types
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  description   :text
#  position      :integer
#  published     :boolean          default(FALSE)
#  primary_image :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  more_about    :text
#  slug          :string(255)      not null
#
# Indexes
#
#  index_audience_types_on_slug  (slug) UNIQUE
#
