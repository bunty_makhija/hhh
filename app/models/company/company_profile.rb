class CompanyProfile < ActiveRecord::Base
  include Imageable
  extend FriendlyId
  friendly_id :company_name, use: :slugged

  has_many :job_opportunities, dependent: :destroy
  has_many :company_types
  has_many :audience_types, through: :company_types

  validates :company_name, presence: true
  validates :email, presence: true
  validates :slug, presence: true, uniqueness: true

  mount_uploader :logo, ImageUploader

  serialize :industries

  def industries=(industry_list)
    super(industry_list & IndustryHelper::TYPES.map(&:to_s))
  end

  def self.consulting_partners
    where(consulting_partner: true)
  end

  def self.employment_partners
    where(employment_partner: true)
  end

  def to_s
    company_name
  end

  def self.consulting_partners
    where(consulting_partner: true)
  end

  def self.featured
    where(featured: true)
  end

  def self.consulting_partners_of_type(audience_type_id)
    consulting_partners.featured.includes(:audience_types)
        .where("audience_types.id" => audience_type_id)
  end
end

# == Schema Information
#
# Table name: company_profiles
#
#  id                 :integer          not null, primary key
#  company_name       :string(255)      not null
#  contact_name       :string(255)
#  email              :string(255)      not null
#  about              :text
#  created_at         :datetime
#  updated_at         :datetime
#  logo               :string(255)
#  key_facts          :text
#  additional_info    :text
#  featured           :boolean
#  primary_image      :string(255)
#  video_embed        :text
#  slug               :string(255)      not null
#  consulting_partner :boolean          default(FALSE), not null
#  employment_partner :boolean          default(FALSE), not null
#  industries         :string(255)
#
# Indexes
#
#  index_company_profiles_on_slug  (slug) UNIQUE
#
