class JobOpportunity < ActiveRecord::Base
  extend Enumerize

  belongs_to :company_profile

  acts_as_taggable_on :skills, :certs

  enumerize :industry,
    in: IndustryHelper::TYPES,
    default: IndustryHelper::DEFAULT
  enumerize :schedule,
    in: %i(full_time part_time contract internship)
  enumerize :status, in: %i(
    urgent
    open
    hold
    matched
    submitted
    not_a_match
    active
    match_success
    cancelled
    closed )

  validates :company_profile, presence: true
  validates :title, presence: true
  validates :job_description, presence: true
end

# == Schema Information
#
# Table name: job_opportunities
#
#  id                 :integer          not null, primary key
#  company_profile_id :integer
#  title              :string(255)      not null
#  industry           :string(255)
#  industry_other     :string(255)
#  schedule           :string(255)
#  job_description    :text             not null
#  status             :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  city               :string(255)
#  state              :string(255)
#
# Indexes
#
#  index_job_opportunities_on_status  (status)
#
