class CompanyType < ActiveRecord::Base
  belongs_to :audience_type
  belongs_to :company_profile
end

# == Schema Information
#
# Table name: company_types
#
#  id                 :integer          not null, primary key
#  audience_type_id   :integer
#  company_profile_id :integer
#
