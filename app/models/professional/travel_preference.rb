class TravelPreference < ActiveRecord::Base
  extend Enumerize
  include ProfessionalProfileComponent
  include ProgressTracking

  enumerize :home_frequency,
    in: %i(daily weekly biweekly monthly never),
    default: :daily

  validates :home_frequency, presence: true

  def progress_conditions
    [ !will_travel.nil?,
      home_frequency.present?,
      !will_relocate.nil?,
      [ region1,
        region2,
        region3,
        region4,
        region5,
        region6,
        region7,
        region8,
        region9,
        region10 ].any? ]
  end
end

# == Schema Information
#
# Table name: travel_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  will_travel             :boolean          default(FALSE), not null
#  will_relocate           :boolean          default(FALSE), not null
#  region1                 :boolean          default(FALSE), not null
#  region2                 :boolean          default(FALSE), not null
#  region3                 :boolean          default(FALSE), not null
#  region4                 :boolean          default(FALSE), not null
#  region5                 :boolean          default(FALSE), not null
#  region6                 :boolean          default(FALSE), not null
#  region7                 :boolean          default(FALSE), not null
#  region8                 :boolean          default(FALSE), not null
#  region9                 :boolean          default(FALSE), not null
#  region10                :boolean          default(FALSE), not null
#  other_concerns          :text
#  home_frequency          :string(255)      default("daily"), not null
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_travel_preferences_on_professional_profile_id  (professional_profile_id)
#
