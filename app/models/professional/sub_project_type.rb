class SubProjectType < ActiveRecord::Base

  default_scope { order('position') }

  belongs_to :project_type
  has_and_belongs_to_many :professional_profiles

  validates :key,          presence: true, format: { with: /\A\w*\z/ },
                           uniqueness: { scope: :project_type }
  validates :display_name, presence: true
  validates :project_type, presence: true
  validates :position,     presence: true, numericality: { greater_than_or_equal_to: 0 }

end

# == Schema Information
#
# Table name: sub_project_types
#
#  id              :integer          not null, primary key
#  project_type_id :integer
#  key             :string(255)
#  display_name    :string(255)
#  position        :integer
#
