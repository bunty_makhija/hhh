class EducationHistory < ActiveRecord::Base
  extend Enumerize
  include ProfessionalProfileComponent
  include ProgressTracking

  belongs_to :professional_profile

  acts_as_taggable_on :concentrations

  ACHIEVEMENT_LEVELS = %i(high_school associates bachelors masters jd phd)

  def self.from_linkedin(history)
    options = {}
    options[:graduation_date] = Date.new(history.endDate.year) if history.endDate
    options[:institution] = history.schoolName
    options[:achievement_level] = history.degree
    options[:concentration_list] = history.fieldOfStudy
    create(options)
  end

  def progress_conditions
    [achievement_level.present?,
        graduation_date.present?,
        institution.present?,
        concentrations.present?]
  end

  def to_s
    desc = []
    desc << "#{achievement_level} degree" if achievement_level
    desc << "from #{institution}" if institution
    desc << "in #{concentration_list.join(', ')}" if concentration_list.present?
    desc << "(#{graduation_date.to_date.strftime('%B %Y')})" if graduation_date
    desc.join ' '
  end
end

# == Schema Information
#
# Table name: education_histories
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  achievement_level       :string(255)
#  graduation_date         :date
#  created_at              :datetime
#  updated_at              :datetime
#  institution             :string(255)
#
# Indexes
#
#  index_education_histories_on_professional_profile_id  (professional_profile_id)
#
