class RolePreference < ActiveRecord::Base
  include ProfessionalProfileComponent
  include ProgressTracking

  belongs_to :desired_role, class_name: AudienceType, foreign_key: :audience_type_id

  def progress_conditions
    [ desired_role.present? ]
  end

  def name
    desired_role.present? ? desired_role.name : ""
  end
end

# == Schema Information
#
# Table name: role_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  audience_type_id        :integer
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_role_preferences_on_audience_type_id         (audience_type_id)
#  index_role_preferences_on_professional_profile_id  (professional_profile_id)
#
