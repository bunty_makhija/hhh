class CategoryFunction < ActiveRecord::Base

  default_scope { order('position') }

  belongs_to :category
  has_and_belongs_to_many :professional_profiles

  validates :category, presence: true
  validates :key,          presence: true, format: { with: /\A\w*\z/ }, uniqueness: { scope: :category_id }
  validates :display_name, presence: true
  validates :position,     presence: true, numericality: { greater_than_or_equal_to: 0 }

end

# == Schema Information
#
# Table name: category_functions
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  key          :string(255)
#  display_name :string(255)
#  position     :integer
#
