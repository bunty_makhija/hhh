class Category < ActiveRecord::Base

  default_scope { order('position') }

  has_many :category_functions, dependent: :destroy

  validates :key,          presence: true, format: { with: /\A\w*\z/ }, uniqueness: true
  validates :display_name, presence: true
  validates :position,     presence: true, numericality: { greater_than_or_equal_to: 0}

end

# == Schema Information
#
# Table name: categories
#
#  id           :integer          not null, primary key
#  key          :string(255)
#  display_name :string(255)
#  position     :integer
#
