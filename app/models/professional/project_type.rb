class ProjectType < ActiveRecord::Base

  default_scope { order('position') }

  has_many :sub_project_types, dependent: :destroy
  has_and_belongs_to_many :professional_profiles

  validates :key,          presence: true, format: { with: /\A\w*\z/ }, uniqueness: true
  validates :display_name, presence: true
  validates :position,     presence: true, numericality: { greater_than_or_equal_to: 0}

end

# == Schema Information
#
# Table name: project_types
#
#  id           :integer          not null, primary key
#  key          :string(255)
#  display_name :string(255)
#  position     :integer
#
