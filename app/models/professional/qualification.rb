class Qualification < ActiveRecord::Base
  include ProfessionalProfileComponent
  include ProgressTracking

  acts_as_taggable_on :skills, :certs

  def progress_conditions
    [ skills.present? || additional_skills.present?,
      certs.present? || additional_certs.present? ]
  end

end

# == Schema Information
#
# Table name: qualifications
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  additional_skills       :text
#  additional_certs        :text
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_qualifications_on_professional_profile_id  (professional_profile_id)
#
