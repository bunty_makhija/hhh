class WorkHistory < ActiveRecord::Base
  extend Enumerize
  include ProfessionalProfileComponent
  include ProgressTracking

  belongs_to :professional_profile

  def self.from_linkedin(history)
    options = {}
    options[:from_date] = Date.new(history.startDate.year, (history.startDate.month || 1)) if history.startDate
    options[:to_date]   = Date.new(history.endDate.year, (history.endDate.month || 1)) if history.endDate
    options[:company]   = history.company.try(:name)
    options[:industry]  = UserLinkedinClient.industry_for_company(history.company.try(:id))
    options[:position]  = history.title
    options[:current]   = history.isCurrent
    options[:summary]   = history.summary
    create(options)
  end

  def progress_conditions
    [company.present?,
        industry.present? || industry_other.present?,
        from_date.present? && to_date.present?,
        position.present?]
  end

  def to_s
    desc = []
    desc << "Company: #{company}" if company
    desc << "Industry: #{industry_other.presence || industry}" if industry_other || industry
    desc << "Started on: #{I18n.l from_date, format: :short}" if from_date
    desc << "Ended on: #{I18n.l to_date, format: :short}" if to_date
    desc << "Position: #{position}" if position
    desc << "Current? #{I18n.t current ? 'affirmative' : 'negative'}"
    desc << "Summary: #{summary}" if summary.presence
    desc.join ', '
  end
end

# == Schema Information
#
# Table name: work_histories
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  company                 :string(255)
#  industry                :string(255)      default("anything"), not null
#  industry_other          :string(255)
#  from_date               :date
#  to_date                 :date
#  position                :string(255)
#  summary                 :text
#  created_at              :datetime
#  updated_at              :datetime
#  current                 :boolean
#
# Indexes
#
#  index_work_histories_on_professional_profile_id  (professional_profile_id)
#
