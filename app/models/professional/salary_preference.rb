class SalaryPreference < ActiveRecord::Base
  include ProfessionalProfileComponent
  include ProgressTracking

  def progress_conditions
    [ base_salary.present? || hourly_rate.present? ]
  end
end

# == Schema Information
#
# Table name: salary_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  base_salary             :string(255)
#  hourly_rate             :string(255)
#  daily_travel_comp       :string(255)
#  other_concerns          :text
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_salary_preferences_on_professional_profile_id  (professional_profile_id)
#
