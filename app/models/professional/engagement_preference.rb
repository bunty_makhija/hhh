class EngagementPreference < ActiveRecord::Base
  include ProfessionalProfileComponent
  include ProgressTracking

  def progress_conditions
    [ starting_date.present?,
      [term_quick, term_small, term_large, term_permanent, term_various].any?,
      professional_profile.status.present?,
      [professional_profile.civil,
        professional_profile.infrastructure,
        professional_profile.energy,
        professional_profile.building,
        professional_profile.petroleum,
        professional_profile.anything,
        professional_profile.industry_other.present?].any?,
      professional_profile.resume.present?,
      user.confirmed?
    ]
  end
end

# == Schema Information
#
# Table name: engagement_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  starting_date           :date
#  term_quick              :boolean          default(FALSE), not null
#  term_small              :boolean          default(FALSE), not null
#  term_large              :boolean          default(FALSE), not null
#  term_permanent          :boolean          default(FALSE), not null
#  other_concerns          :text
#  created_at              :datetime
#  updated_at              :datetime
#  term_various            :boolean
#
# Indexes
#
#  index_engagement_preferences_on_professional_profile_id  (professional_profile_id)
#
