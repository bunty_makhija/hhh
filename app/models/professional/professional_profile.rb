class ProfessionalProfile < ActiveRecord::Base
  extend Enumerize
  include ProgressTracking
  include ActionView::Helpers::TranslationHelper

  belongs_to :user, inverse_of: :professional_profile
  belongs_to :category
  has_one :engagement_preference, dependent: :destroy
  has_one :role_preference, dependent: :destroy
  has_one :travel_preference, dependent: :destroy
  has_one :schedule_preference, dependent: :destroy
  has_one :salary_preference, dependent: :destroy
  has_one :qualification, dependent: :destroy
  has_many :work_histories, dependent: :destroy
  has_many :education_histories, dependent: :destroy
  has_and_belongs_to_many :sub_project_types, after_add: :add_project_type
  has_and_belongs_to_many :category_functions, after_add: :validate_and_add_category
  has_and_belongs_to_many :project_types, after_remove: :remove_sub_project_types
  has_and_belongs_to_many :metro_areas

  accepts_nested_attributes_for(
    :engagement_preference,
    :role_preference,
    :travel_preference,
    :schedule_preference,
    :salary_preference,
    :qualification)

  accepts_nested_attributes_for(
    :work_histories,
    :education_histories,
    {allow_destroy: true})

  enumerize :status,
    in: %i(active passive inactive),
    default: :active,
    predicates: true

  enumerize :experience,
    in: ["Student", "0-3 years", "4-10 years", "11-20 years", "21+ years"]

  validates :status, presence: true
  validates :user, presence: true

  validate :validate_category_is_consistent_with_functions

  after_create :create_components

  mount_uploader :resume, ResumeUploader

  delegate :full_name, to: :user

  before_save :remove_hubspot_resume, if: Proc.new{|model| model.resume?}

  def progress_conditions
    [ true,
      engagement_preference.try(:complete?),
      role_preference.try(:complete?),
      travel_preference.try(:complete?),
      schedule_preference.try(:complete?),
      salary_preference.try(:complete?),
      qualification.try(:complete?),
      work_histories.any?(&:complete?),
      education_histories.any?(&:complete?)]
  end

  def work_history_from_linkedin(linkedin_work_histories)
    return unless linkedin_work_histories
    self.work_histories = linkedin_work_histories.map do |job|
      WorkHistory.from_linkedin(job)
    end
  end

  def education_history_from_linkedin(linkedin_education_histories)
    return unless linkedin_education_histories
    self.education_histories = linkedin_education_histories.map do |education|
      EducationHistory.from_linkedin(education)
    end
  end

  def hubspot_resume_filename
    return nil unless hubspot_resume_url
    URI.unescape(hubspot_resume_url).split("/").last
  end

  def create_components
    build_engagement_preference.save!
    build_role_preference.save!
    build_travel_preference.save!
    build_schedule_preference.save!
    build_salary_preference.save!
    build_qualification.save!
  end

  def add_skills(skills_hash)
    skills = skills_hash.try(:fetch, "values", []) || []
    skills = skills.map { |skill| skill["skill"]["name"] }
    qualification.update_attributes(additional_skills: skills.join(", "))
  end

  def add_certifications(certifications_hash)
    certs = certifications_hash.try(:fetch, "values", []) || []
    certs = certs.map { |cert| cert["name"] }
    qualification.update_attributes(additional_certs: certs.join(", "))
  end

  def resume_display_name
    return resume_original_filename.truncate(40) unless resume_original_filename.blank?
    resume.path.split("/").last unless resume.file.nil?
  end

  def preferred_industries
    IndustryHelper::TYPES.select { |industry| self.send(industry) }
  end

  private

  def remove_sub_project_types(project_type)
    self.sub_project_types.delete(project_type.sub_project_types)
  end

  def add_project_type(sub_project_type)
    self.project_types << sub_project_type.project_type unless self.project_types.include? sub_project_type.project_type
  end

  def remove_hubspot_resume
    self.hubspot_resume_url = nil
  end

  def validate_category_is_consistent_with_functions
    errors.add(:category, t('views.professional_profiles.category_error')) unless category_consistent_with_functions?
  end

  def category_consistent_with_functions?
    category_functions.blank? || category_functions.pluck(:category_id).uniq == [category_id]
  end

  def remove_non_matching_category_functions
    category_functions.delete(CategoryFunction.where.not(category: category))
  end

  def validate_and_add_category(function)
    update_attributes(category: function.category) unless category == function.category
    remove_non_matching_category_functions
  end
end

# == Schema Information
#
# Table name: professional_profiles
#
#  id                       :integer          not null, primary key
#  user_id                  :integer          not null
#  status                   :string(255)      not null
#  zip_code                 :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  resume                   :string(255)
#  civil                    :boolean
#  energy                   :boolean
#  building                 :boolean
#  anything                 :boolean
#  industry_other           :string(255)
#  completed_at             :datetime
#  associated_with          :text
#  referrals                :string(255)
#  country                  :string(255)
#  state                    :string(255)
#  city                     :string(255)
#  infrastructure           :boolean
#  petroleum                :boolean
#  summary                  :text
#  hubspot_resume_url       :string(255)
#  resume_original_filename :string(255)
#  experience               :string(255)
#  authorization            :boolean
#  authorization_details    :text
#  category_id              :integer
#  wizard_completed_at      :datetime
#
# Indexes
#
#  index_professional_profiles_on_status   (status)
#  index_professional_profiles_on_user_id  (user_id)
#
