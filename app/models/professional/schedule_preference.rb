class SchedulePreference < ActiveRecord::Base
  include ProfessionalProfileComponent
  include ProgressTracking

  def progress_conditions
    [ [ full_time, part_time, contract, internship ].any? ]
  end
end

# == Schema Information
#
# Table name: schedule_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  full_time               :boolean          default(FALSE), not null
#  part_time               :boolean          default(FALSE), not null
#  contract                :boolean          default(FALSE), not null
#  other_concerns          :text
#  internship              :boolean
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_schedule_preferences_on_professional_profile_id  (professional_profile_id)
#
