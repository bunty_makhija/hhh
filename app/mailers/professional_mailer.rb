class ProfessionalMailer < ActionMailer::Base
  layout "email"

  def welcome_user(user)
    @user = user
    mail(
      to: @user.email,
      from: Settings.email.from,
      subject: I18n.t("views.professional_mailer.welcome_user.subject") )
  end

end
