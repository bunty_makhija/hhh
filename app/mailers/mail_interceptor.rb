class MailInterceptor
  def self.delivering_email(message)
    message.to = Settings.email.to_stage unless message.to.all? {|to| to =~ /.+@tablexi\.com/}
    message.subject = "#{message.subject} sent to #{message.to}"
    message.cc = nil
    message.bcc = nil
  end
end
