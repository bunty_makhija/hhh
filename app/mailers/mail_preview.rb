class MailPreview < MailView

  def confirmation_instructions
    Devise::Mailer.confirmation_instructions user, token
  end

  def reset_password_instructions
    Devise::Mailer.reset_password_instructions user, token
  end

  def welcome
    ProfessionalMailer.welcome_user user
  end

  # def unlock_instructions
  #   Devise::Mailer.unlock_instructions user, token
  # end

  private

  def user
    User.find_by_email 'buckles@tablexi.com'
  end

  def token
    "YEt9PJgQsxb3WpJQ7OEXH3YDT8JZMD"
  end

end
