require 'spec_helper'

describe ImageUploader do
  subject(:uploader) { described_class.new }

  it 'whitelists image extensions only' do
    white_list = %w(jpg jpeg gif png)
    expect(uploader.extension_white_list.size).to eq white_list.size
    white_list.each {|ext| expect(uploader.extension_white_list).to include ext }
  end
end
