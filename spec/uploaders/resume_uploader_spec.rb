require 'spec_helper'

describe ResumeUploader do
  let(:profile) { build :professional_profile }
  subject(:uploader) { described_class.new(profile) }

  it 'whitelists resume appropriate extensions only' do
    white_list = %w(doc docx pdf rtf txt odt html)
    expect(uploader.extension_white_list.size).to eq white_list.size
    white_list.each {|ext| expect(uploader.extension_white_list).to include ext }
  end

  it "should not have a filename at initialization" do
    expect(uploader.filename).to be_nil
  end

  it { expect(subject.fog_public).to be_true }

  describe "stored filename and original filename" do
    let(:resume_pdf) { File.new(Rails.root.join('spec/factories/manual.pdf')) }

    before do
      uploader.store!(resume_pdf)
      @unique_filename = uploader.filename
    end

    it { expect(uploader.filename).not_to eq "manual.pdf" }
    it { expect(uploader.filename.end_with?("pdf")).to be_true }
    it "consistently generates the same filename with each call" do
      expect(uploader.filename).to eq @unique_filename
    end
    it { expect(profile.resume_original_filename).to eq "manual.pdf" }

    after do
      FileUtils.rm_rf(Rails.root.join("public/#{uploader.store_path}"))
    end
  end
end
