require 'spec_helper'
require 'pry'

describe "Analytics", type: :request  do

  def analytics_regex(analytics_data_attribute)
    Regexp.new("data-analytics=['\"]#{analytics_data_attribute}['\"]")
  end

  def expect_conversion_scripts_to_render(response)
    twitter_regex = Regexp.new(/twttr.conversion.trackPid..#{Settings.twitter_tracking_id}/)
    expect(response.body).to match(twitter_regex)
    expect(response.body).to match("google_conversion_id = #{Settings.adwords_id}")
    expect(response.body).to match("fb_param.pixel_id = #{Settings.facebook_tracking_id}")
    expect(response.body).to match(analytics_regex("create-profile"))
    expect(response).to render_template(partial: "vendor/_adwords_tracking")
    expect(response).to render_template(partial: "vendor/_facebook_tracking")
    expect(response).to render_template(partial: "vendor/_twitter_tracking")
  end

  def user_params
    {
      first_name: "First",
      last_name: "Last",
      email: "email@hardhathub.com",
      zip_code: "60606",
      country: "USA",
      civil: "1",
      password: "password",
      password_confirmation: "password" }
  end

  def stub_analytics_settings
    allow(Settings).to receive(:adwords_id).and_return 123456
    allow(Settings).to receive(:adwords_conversion_label).and_return "abc123"
    allow(Settings).to receive(:facebook_tracking_id).and_return 123456
    allow(Settings).to receive(:twitter_tracking_id).and_return "abc123"
  end

  context "analytics, adwords, facebook, hubspot scripts" do
    it "renders analytics or adwords remarketing scripts on a normal GET request" do
      get "/"
      expect(response).to render_template(partial: "vendor/_google_analytics")
      expect(response).to render_template(partial: "vendor/_hubspot_analytics")
    end

    it "does not render adwords or facebook conversion scripts on a normal GET request" do
      get "/"
      expect(response).to_not render_template(partial: "vendor/_adwords_tracking")
      expect(response).to_not render_template(partial: "vendor/_facebook_tracking")
    end
  end

  context "Regular signup" do
    before do
      stub_analytics_settings
      post user_registration_path, {
        user_registration_form: user_params }
    end
    it "renders conversion tracking on welcome path once" do
      expect(flash[:render_conversion_scripts]).to eq(true)
      get get_started_path(:welcome)
      expect_conversion_scripts_to_render(response)

      get get_started_path(:welcome)
      expect(flash[:render_conversion_scripts]).to be_nil
      expect(response.body).to_not match(analytics_regex("create-profile"))
    end
  end

  context "Webhook signup" do
    before { stub_analytics_settings }
    it "renders conversion tracking" do
      allow_any_instance_of(WebhookUsersController).to receive(:valid_post?).and_return(true) #testing specific behavior
      post wizard_path, user_params
      expect(response).to redirect_to(get_started_index_path)
      expect(flash[:render_conversion_scripts]).to eq(true)
      get get_started_path(:welcome)
      expect_conversion_scripts_to_render(response)
    end
  end

  context "event tracking elements - devise I18n messages" do
    it "renders data attributes for devise.confirmations.confirmed" do
      message = I18n.t('devise.confirmations.confirmed')
      expect(message).to match(analytics_regex("confirm-profile"))
    end
  end

  context "event tracking elements - external profile pages:" do
    it "/users/sign_in renders with analytics elements" do
      get "/users/sign_in"
      expect(response.body).to match(analytics_regex("sign-in"))
    end

    it "/users/password/new renders with analytics elements" do
      get "/users/password/new"
      expect(response.body).to match(analytics_regex("reset-password"))
    end

    it "/users/confirmation/new renders with analytics elements" do
      get "/users/confirmation/new"
      expect(response.body).to match(analytics_regex("resend-confirmation"))
    end
  end

  context "event tracking elements - external marketing pages:" do
    it "/ renders with with get-started, social elements" do
      get "/"
      expect(response.body).to match(analytics_regex("click-get-started"))
      expect(response.body).to match(analytics_regex("click-twitter"))
      expect(response.body).to match(analytics_regex("click-facebook"))
    end

    it "/pages/professional renders with get-started element" do
      get "/pages/professionals"
      expect(response.body).to match(analytics_regex("click-get-started"))
    end

    it "/company_profiles renders with partner-program element" do
      get "/company_profiles"
      expect(response.body).to match(analytics_regex("click-partner-program"))
    end

    it "/company_profiles/<company> renders with get-started element" do
      company = CompanyProfile.create(
        company_name: "Power", email: "fake@example.com", slug: "power-construction")
      get "/company_profiles/power-construction"
      expect(response.body).to match(analytics_regex("click-get-started"))
    end

    it "/pages/about renders with get-started element" do
      get "/pages/about"
      expect(response.body).to match(analytics_regex("click-join-team"))
    end

  end
  context "event tracking elements - internal profile pages:" do
    describe "analytics data attributes", type: :controller do
      let(:user) { create :user }
      include_context "logged in as user", :user
      let(:professional_profile) {create user.professional_profile}
      let(:travel_preference) { user.professional_profile.travel_preference }
      let(:schedule_preference) { user.professional_profile.schedule_preference }
      let(:qualification) { user.professional_profile.qualification }
      let(:salary_preference) { user.professional_profile.salary_preference }
      let(:education_history) { create :education_history, professional_profile: user.professional_profile }
      let(:work_history) { create :work_history, professional_profile: user.professional_profile }

      describe ProfessionalProfilesController do
        it "renders the professional form with analytics elements" do
          user.professional_profile.update(wizard_completed_at: 5.minutes.ago)
          get 'edit', use_route: :edit_professional_profile, id: user.id

          expect(response.body).to match(analytics_regex("submit-professional"))
          expect(response.body).to match(analytics_regex("add-resume"))
          expect(response.body).to match(analytics_regex("sign-out"))
        end
      end

      describe TravelPreferencesController do
        it "renders the travel form with analytics elements" do
          get 'edit', use_route: :edit_professional_profile_travel_preferences,
              professional_profile_id: user.id,
              id: travel_preference.id
          expect(response.body).to match(analytics_regex("submit-travel"))
        end
      end
      describe SchedulePreferencesController do
        it "renders the schedule form with analytics elements" do
          get 'edit', use_route: :edit_professional_profile_schedule_preferences,
              professional_profile_id: user.id,
              id: schedule_preference.id
          expect(response.body).to match(analytics_regex("submit-schedule"))
        end
      end
      describe SalaryPreferencesController do
        it "renders the salary form with analytics elements" do
         get 'edit', use_route: :edit_professional_profile_salary_preferences,
              professional_profile_id: user.id,
              id: salary_preference.id
          expect(response.body).to match(analytics_regex("submit-salary"))
        end
      end
      describe QualificationsController do
        it "renders the skils form with analytics elements" do
          get 'edit', use_route: :edit_professional_profile_qualifications,
              professional_profile_id: user.id,
              id: qualification.id
          expect(response.body).to match(analytics_regex("submit-qualifications"))
        end
      end
      describe RegistrationsController do
        it "renders the account form with analytics elements" do
          get 'edit', use_route: :edit_user_registration, id: user.id
          expect(response.body).to match(analytics_regex("submit-settings"))
          expect(response.body).to match(analytics_regex("cancel-account"))
        end
      end
    end
  end
end
