shared_context "with clear deliveries array" do
  let(:deliveries) { ActionMailer::Base.deliveries }
  before { deliveries.clear }
end
