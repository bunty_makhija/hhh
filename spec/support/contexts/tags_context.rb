shared_context 'tags' do |count = 3|
  before do
    ActsAsTaggableOn::Tag::PREDEFINED_CONTEXTS.values.each do |context|
      count.times do |i|
        ActsAsTaggableOn::Tag.create! name: Faker::Lorem.word + context + i.to_s, original_context: context
      end
    end
  end
end
