shared_context 'cancan' do
  let(:user) { create :user }
  let(:ability) { described_class.new user }


  %i(read create update destroy manage).each do |permission|
    define_method "can_#{permission}" do |resource, *filters|
      expect(ability).to be_able_to permission, resource, *filters
    end

    define_method "cannot_#{permission}" do |resource, *filters|
      expect(ability).to_not be_able_to permission, resource, *filters
    end
  end


  def can_only_read(resource, *filters)
    can_read resource, *filters
    cannot_manage resource, *filters
  end

end
