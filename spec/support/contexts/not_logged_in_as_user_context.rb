shared_context "not logged in as user" do
  before do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end
end
