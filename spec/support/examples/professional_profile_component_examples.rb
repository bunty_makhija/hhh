shared_examples_for "professional profile component" do
  it { should have_db_column(:professional_profile_id).of_type(:integer).with_options(null: false) }
  it { should have_db_index(:professional_profile_id) }
  it { should belong_to(:professional_profile) }
  it { should respond_to(:professional_profile) }
  it { should validate_presence_of(:professional_profile) }

  it 'saves the professional profile completed_at time the first time the profile is completed' do
    subject.professional_profile = create :professional_profile unless subject.professional_profile
    profile = subject.professional_profile
    expect(profile.completed_at).to be_nil
    profile.stub(:complete?).and_return true
    expect(subject.save).to be_true
    expect(profile.reload.completed_at).to be_present
  end
end
