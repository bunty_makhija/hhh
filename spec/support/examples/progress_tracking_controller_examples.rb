shared_examples_for "progress tracking" do
  it { should respond_to(:complete?) }
  it { should respond_to(:percent_complete) }

  describe "progress" do
    let(:described_instance) { described_class.new }

    it "should define progress" do
      expect(described_instance).to respond_to(:progress_conditions)
    end

    context "incomplete validation" do
      it "has incomplete progress" do
        expect(described_instance).to receive(:progress_conditions).twice.and_return [false]
        expect(described_instance.complete?).to be_false
      end
    end

    context "complete validation" do
      it "has complete progress" do
        expect(described_instance).to receive(:progress_conditions).twice.and_return [true]
        expect(described_instance.complete?).to be_true
      end
    end
  end
end
