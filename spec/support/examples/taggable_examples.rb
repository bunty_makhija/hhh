shared_examples_for 'taggable' do |*tags|
  tags.each do |tag|
    it { should respond_to "#{tag}_list" }
  end
end
