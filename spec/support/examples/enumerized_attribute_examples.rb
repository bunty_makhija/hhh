shared_examples_for "enumerized attribute" do |factory, *attribute_names|
  attribute_names.each do |attribute_name|
    describe "#{attribute_name} attribute" do
      subject { build(factory, {attribute_name => value}) }
      let(:model) { create(factory).class }

      context "set to valid string" do
        let(:value) { model.send(attribute_name).values.first }
        it { should be_valid }
      end

      context "set to invalid string" do
        let(:value) { "this is almost certainly not a valid value" }
        it { should_not be_valid }
      end
    end
  end
end
