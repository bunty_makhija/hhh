shared_examples_for "resource actions" do |resource, actions|
  actions.each do |action|
    describe action do
      let(:current_resource) { send(resource) }

      before(:each) do
        get action,
          id: current_resource.id,
          professional_profile_id: current_resource.professional_profile_id
      end

      it "assigns the resource" do
        expect(assigns(resource)).to eq current_resource
      end

      it "renders the template" do
        expect(response).to render_template(action)
      end
    end
  end
end
