shared_examples_for "professional profile component controller" do
  after(:each) do
    expect(assigns(:professional_profile)).to eq send(:user).professional_profile
  end
end
