FactoryGirl.define do
  factory :user do
    first_name     { Faker::Name.first_name }
    last_name      { Faker::Name.last_name }
    email          { Faker::Internet.email }
    password       { Faker::Lorem.characters(20) }
    sign_in_count  0

    after(:build) {|user| user.confirm! }

    factory :unconfirmed_user do
      after(:build) do |user|
        user.update(confirmed_at: nil)
        user.update(confirmation_token: Faker::Lorem.characters(20))
      end
    end

    factory :admin do
      after(:build) {|user| user.add_role :admin }
    end

    factory :profile_manger do
      after(:build) {|user| user.add_role :profile_manager }
    end

    factory :profile_viewer do
      after(:build) {|user| user.add_role :profile_viewer }
    end
  end
end
