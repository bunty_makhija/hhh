FactoryGirl.define do
  factory :audience_type do
    name Faker::Name.name
    description Faker::Lorem.paragraph
    position { rand(AudienceType::POSITIONS) }
  end
end
