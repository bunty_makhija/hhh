FactoryGirl.define do
  factory :project_type do
    sequence(:key)      { |n| "project_type#{n}" }
    display_name        { Faker::Lorem.word }
    sequence(:position) { |n| n }
  end
end
