FactoryGirl.define do
  factory :category_function do
    category
    sequence(:key)      { |n| "#{category.key}_function#{n}" }
    display_name        { Faker::Lorem.word }
    sequence(:position) { |n| n }
  end
end
