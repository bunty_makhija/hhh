FactoryGirl.define do
  factory :work_history do
    professional_profile
    company Faker::Company.name
    industry IndustryHelper::TYPES.sample
    from_date Time.now - 2.years
    to_date Time.now
    position "#{Faker::Company.catch_phrase} Specialist"
    current true
    summary Faker::Company.bs
  end
end
