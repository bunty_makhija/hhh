FactoryGirl.define do
  factory :audience_profile do
    audience_type
    title Faker::Company.catch_phrase
    bio Faker::Lorem.paragraph
  end
end
