FactoryGirl.define do
  factory :education_history do
    professional_profile
    achievement_level EducationHistory::ACHIEVEMENT_LEVELS.sample
    graduation_date 5.years.ago
    institution "#{Faker::Company.name} University"

    after(:build) do |edu|
      edu.concentration_list = [ Faker::Company.catch_phrase, Faker::Company.catch_phrase]
    end
  end
end
