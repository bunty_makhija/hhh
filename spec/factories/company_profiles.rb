FactoryGirl.define do
  factory :company_profile do
    company_name  { Faker::Company.name }
    email         { Faker::Internet.email }
    consulting_partner true
    employment_partner false
  end
end
