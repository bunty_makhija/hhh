FactoryGirl.define do
  factory :sub_project_type do
    project_type
    sequence(:key)      { |n| "#{project_type.key}_subtype#{n}" }
    display_name        { Faker::Lorem.sentence }
    sequence(:position) { |n| n }
  end
end
