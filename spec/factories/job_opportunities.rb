FactoryGirl.define do
  factory :job_opportunity do
    company_profile
    title           { Faker::Company.catch_phrase }
    job_description { Faker::Lorem.paragraph }
  end
end
