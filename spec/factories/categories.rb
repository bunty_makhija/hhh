FactoryGirl.define do
  factory :category do
    sequence(:key)      { |n| "category#{n}" }
    display_name        { Faker::Lorem.word }
    sequence(:position) { |n| "#{n}" }
  end
end
