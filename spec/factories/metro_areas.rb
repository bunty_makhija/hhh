FactoryGirl.define do
  factory :metro_area do
    area_name "Chibera"
    city "Chicago"
    state "IL"
  end
end
