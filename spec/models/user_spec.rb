require "spec_helper"

describe User do
  let(:user) { create :user }

  it { should have_db_column(:first_name).of_type(:string).with_options(null: false) }
  it { should have_db_column(:last_name).of_type(:string).with_options(null: false) }
  it { should have_db_column(:phone_number).of_type(:string) }

  it { should have_one(:professional_profile).dependent(:destroy) }

  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:phone_number) }

  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should allow_value(nil).for(:uid) }

  describe "unique uid" do
    before { user.update_attribute :uid, 12 }
    it { should validate_uniqueness_of(:uid) }
  end

  describe 'full name' do
    it 'includes the first name' do
      expect(user.full_name).to include user.first_name
    end

    it 'includes the last name' do
      expect(user.full_name).to include user.last_name
    end
  end

  describe "creating" do
    it "creates a professional profile" do
      expect(user.professional_profile).not_to be_nil
    end

    context "with profile attributes for energy industry" do
      before do
        Fog.mock!
        Fog::Mock.reset
      end

      subject(:user_with_built_profile) do
        create(:user, professional_profile_attributes: {energy: true, resume: "resume.pdf"})
      end

      it { expect(user_with_built_profile.valid?).to be_true }
      it { expect(user_with_built_profile.professional_profile.resume).not_to be_nil }
      it { expect(user_with_built_profile.professional_profile.energy).to be_true }
      it { expect(user_with_built_profile.professional_profile.civil).to be_false }
      it { expect(user_with_built_profile.professional_profile.role_preference).not_to be_nil }
    end
  end

  describe "is created by hubspot" do
    let(:is_created) { create(:user, confirmed_at: Time.now, sign_in_count: 0) }
    let(:is_not) { create(:user, confirmed_at: Time.now, confirmation_token: "here-is-a-token") }
    let(:logged_in_already) { create(:user, confirmed_at: Time.now, sign_in_count: 1) }

    it { expect(is_created.hubspot_created?).to be_true }
    it { expect(is_not.hubspot_created?).to be_false }
    it { expect(logged_in_already.hubspot_created?).to be_false }
  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  first_name             :string(255)      not null
#  last_name              :string(255)      not null
#  phone_number           :string(255)
#  provider               :string(255)
#  uid                    :string(255)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
