require 'spec_helper'

describe Ability do
  include_context 'cancan'

  let(:other_user) { create :user }


  it 'allows admins all privileges' do
    user.add_role :admin
    can_manage :all
  end


  describe 'profile managers' do
    before { user.add_role :profile_manager }

    it { can_only_read User }
    it { can_only_read ActiveAdmin::Page, name: 'Dashboard' }

    it 'can manage professional resources' do
      ability.professional_resources.each {|clazz| can_manage clazz }
    end

    it 'can manage company resources' do
      ability.company_resources.each {|clazz| can_manage clazz }
    end
  end


  describe 'profile viewers' do
    before { user.add_role :profile_viewer }

    it { can_only_read User }
    it { can_only_read ActiveAdmin::Page, name: 'Dashboard' }

    it 'can read professional resources' do
      ability.professional_resources.each {|clazz| can_only_read clazz }
    end

    it 'can read company resources' do
      ability.company_resources.each {|clazz| can_only_read clazz }
    end

    it 'can read the resume' do
      expect(ability).to be_able_to(:resume, ProfessionalProfile)
    end
  end


  describe 'ordinary users' do
    it { cannot_manage :all }
    it { cannot_manage ActiveAdmin::Page }

    describe 'a user accessing their own professional profile' do
      let(:profile) { user.professional_profile }
      it { can_read profile }
      it { can_update profile }
      it { expect(ability).to be_able_to(:resume, profile) }
    end

    describe "a user accessing another user's professional profile" do
      let(:profile) { other_user.professional_profile }
      it { cannot_read profile }
      it { cannot_update profile }
      it { expect(ability).not_to be_able_to(:resume, profile)}
    end

    it 'gives a user access to their own profile member components' do
      ability.professional_profile_member_components.each do |clazz|
        component = user.professional_profile.send clazz.name.underscore.to_sym
        can_read component
        can_update component
      end
    end

    it "does not give a user access to another user's profile member components" do
      ability.professional_profile_member_components.each do |clazz|
        component = other_user.professional_profile.send clazz.name.underscore.to_sym
        cannot_read component
        cannot_update component
      end
    end

    it 'gives a user access to their own profile collection components' do
      ability.professional_profile_collection_components.each do |clazz|
        component = create clazz.name.underscore.to_sym, professional_profile: user.professional_profile
        can_read component
        can_update component
        can_create component
        can_destroy component
      end
    end

    it "does not give a user access to another user's profile collection components" do
      ability.professional_profile_collection_components.each do |clazz|
        component = create clazz.name.underscore.to_sym, professional_profile: other_user.professional_profile
        cannot_read component
        cannot_update component
        cannot_create component
        cannot_destroy component
      end
    end
  end

end
