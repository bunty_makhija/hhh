require 'spec_helper'

describe UserLinkedinUpdater do
  let(:user) { build(:user) }
  let(:empty_omni) { OmniAuth::AuthHash.new({_total: 0}) }
  let(:auth) { OmniAuth::AuthHash.new({
    provider: "linkedin",
    uid: "12345",
    extra: {
      raw_info: {
        educations: empty_omni,
        positions: empty_omni,
        skills: empty_omni,
        summary: "No summary",
        phoneNumbers: empty_omni
      }
    }
  }) }

  context "#update_to_linkedin_user" do
    it "overwrites the provider and uid" do
      linkedin_updater = UserLinkedinUpdater.new(user, auth)
      linkedin_updater.update_to_linkedin_user
      expect(user.provider).to eq("linkedin")
      expect(user.uid).to eq("12345")
    end
  end

  context "update" do
    let(:updater) { UserLinkedinUpdater.new(user, auth) }
    it { expect(updater.update).to be_true }
  end

  context "#update_phone_number" do
    context "without a phone number" do
      let(:updater) { UserLinkedinUpdater.new(user, auth) }
      before { updater.update_phone_number }
      it { expect(user.phone_number).to eq nil }
    end

    context "with a phone number" do
      let(:with_phone) { OmniAuth::AuthHash.new({
        provider: "linkedin",
        uid: "12345",
        extra: {
          raw_info: {
            phoneNumbers: OmniAuth::AuthHash.new({
              _total: 1,
              values: [OmniAuth::AuthHash.new({
                phoneNumber: "312-111-2222",
                phoneType: "work"
              })]
            })
          }
        }
      })}
      let(:updater) { UserLinkedinUpdater.new(user, with_phone) }
      before { updater.update_phone_number }
      it { expect(user.phone_number).to eq "312-111-2222" }
    end
  end

  context "update duplicate linkedin user" do
    let(:linkedin_dup) { OmniAuth::AuthHash.new({
      provider: "linkedin",
      uid: "linkedin-uid"
    }) }
    let(:updater) { UserLinkedinUpdater.new(user, linkedin_dup) }

    before { create(:user, uid: 'linkedin-uid') }

    it { expect(updater.update).to be_false }
    it { expect(user.uid).to be_nil }
  end
end
