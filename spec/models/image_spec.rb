require 'spec_helper'

describe Image do
  it { should validate_presence_of :attachment }
end

# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  imageable_type :string(255)
#  imageable_id   :integer
#  attachment     :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#
