require 'spec_helper'

describe MetroArea do
  it { should validate_presence_of(:area_name) }
  it { should validate_presence_of(:city) }
  it { should validate_presence_of(:state) }
  it { should validate_uniqueness_of(:area_name) }

  it { should have_and_belong_to_many(:professional_profiles) }
end

# == Schema Information
#
# Table name: metro_areas
#
#  id         :integer          not null, primary key
#  area_name  :string(255)
#  city       :string(255)
#  state      :string(255)
#  created_at :datetime
#  updated_at :datetime
#
