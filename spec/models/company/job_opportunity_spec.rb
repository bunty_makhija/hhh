require 'spec_helper'

describe JobOpportunity do
  it { should have_db_column(:company_profile_id).of_type(:integer) }
  it { should have_db_column(:title).of_type(:string).with_options(null: false) }
  it { should have_db_column(:industry).of_type(:string) }
  it { should have_db_column(:industry_other).of_type(:string) }
  it { should have_db_column(:schedule).of_type(:string) }
  it { should have_db_column(:job_description).of_type(:text).with_options(null: false) }
  it { should have_db_column(:city).of_type(:string) }
  it { should have_db_column(:state).of_type(:string) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should respond_to(:company_profile) }
  it { should respond_to(:title) }
  it { should respond_to(:industry) }
  it { should respond_to(:industry_other) }
  it { should respond_to(:schedule) }
  it { should respond_to(:job_description) }
  it { should respond_to(:city) }
  it { should respond_to(:state) }

  it { should validate_presence_of(:company_profile) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:job_description) }

  it_should_behave_like 'taggable', :cert, :skill
  it_should_behave_like "enumerized attribute", :job_opportunity, :industry, :schedule, :status
end

# == Schema Information
#
# Table name: job_opportunities
#
#  id                 :integer          not null, primary key
#  company_profile_id :integer
#  title              :string(255)      not null
#  industry           :string(255)
#  industry_other     :string(255)
#  schedule           :string(255)
#  job_description    :text             not null
#  status             :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  city               :string(255)
#  state              :string(255)
#
# Indexes
#
#  index_job_opportunities_on_status  (status)
#
