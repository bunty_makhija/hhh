require "spec_helper"

describe CompanyProfile do
  it { should have_db_column(:company_name).of_type(:string).with_options(null: false) }
  it { should have_db_column(:slug).of_type(:string).with_options(null: false) }
  it { should have_db_column(:contact_name).of_type(:string) }
  it { should have_db_column(:email).of_type(:string).with_options(null: false) }
  it { should have_db_column(:about).of_type(:text) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should respond_to(:company_name) }
  it { should respond_to(:contact_name) }
  it { should respond_to(:email) }
  it { should respond_to(:about) }

  it { should validate_presence_of(:company_name) }
  it { should validate_presence_of(:email) }

  it "validates uniqueness of slug" do
    create :company_profile
    should validate_uniqueness_of(:slug)
  end

  it { should allow_value(Faker::Lorem.paragraph).for :key_facts }
  it { should allow_value(Faker::Lorem.paragraph).for :additional_info }
  it { should allow_value(Faker::Lorem.sentence).for :logo }
  it { should allow_value(true).for :featured}
  it { should allow_value(false).for :featured}

  it { should have_many(:job_opportunities).dependent :destroy }

  it 'gives the featured profiles only' do
    6.times {|i| create :company_profile, featured: (i % 2 == 0) }
    expect(described_class.featured.size).to eq 3
  end

  context "friendly_id" do
    let(:company_profile) { create :company_profile }

    it 'is equivalent to id for find method' do
      expect(CompanyProfile.find(company_profile.id)).to eq CompanyProfile.friendly.find(company_profile.slug)
    end
  end
end

# == Schema Information
#
# Table name: company_profiles
#
#  id                 :integer          not null, primary key
#  company_name       :string(255)      not null
#  contact_name       :string(255)
#  email              :string(255)      not null
#  about              :text
#  created_at         :datetime
#  updated_at         :datetime
#  logo               :string(255)
#  key_facts          :text
#  additional_info    :text
#  featured           :boolean
#  primary_image      :string(255)
#  video_embed        :text
#  slug               :string(255)      not null
#  consulting_partner :boolean          default(FALSE), not null
#  employment_partner :boolean          default(FALSE), not null
#  industries         :string(255)
#
# Indexes
#
#  index_company_profiles_on_slug  (slug) UNIQUE
#
