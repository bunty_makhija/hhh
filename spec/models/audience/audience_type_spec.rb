require 'spec_helper'

describe AudienceType do

  subject(:type) { build :audience_type }

  it { should validate_presence_of :name }
  it { should validate_presence_of :description }
  it { should have_many :audience_profiles }
  it { should ensure_inclusion_of(:position).in_range described_class::POSITIONS }

  it 'is the audience type positions setting' do
    expect(described_class::POSITIONS).to eq 1..Settings.audience_type_positions
  end

  it 'defaults published to false' do
    expect(type.published).to be_false
  end

  context "friendly_id" do
    let(:audience_type) { create :audience_type }

    it { expect(audience_type.slug).not_to be_nil }

    it 'is equivalent to id for find method' do
      expect(AudienceType.find(audience_type.id)).to eq AudienceType.friendly.find(audience_type.slug)
    end
  end
end

# == Schema Information
#
# Table name: audience_types
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  description   :text
#  position      :integer
#  published     :boolean          default(FALSE)
#  primary_image :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  more_about    :text
#  slug          :string(255)      not null
#
# Indexes
#
#  index_audience_types_on_slug  (slug) UNIQUE
#
