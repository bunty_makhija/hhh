require 'spec_helper'

describe AudienceProfile do

  subject(:profile) { build :audience_profile }

  it { should validate_presence_of :audience_type_id }
  it { should validate_presence_of :title }
  it { should validate_presence_of :bio }
  it { should belong_to :audience_type }

  it_should_behave_like 'taggable', :cert, :skill

  it 'defaults featured to false' do
    expect(profile.featured).to be_false
  end

end

# == Schema Information
#
# Table name: audience_profiles
#
#  id               :integer          not null, primary key
#  audience_type_id :integer
#  featured         :boolean          default(FALSE)
#  title            :string(255)
#  bio              :text
#  primary_image    :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  published        :boolean          default(FALSE)
#  name             :string(255)
#
# Indexes
#
#  index_audience_profiles_on_audience_type_id  (audience_type_id)
#
