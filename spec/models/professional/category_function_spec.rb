require 'spec_helper'

describe CategoryFunction do
  describe "db table" do
    it { should have_db_column(:category_id).of_type(:integer) }
    it { should have_db_column(:key).of_type(:string) }
    it { should have_db_column(:display_name).of_type(:string) }
  end

  describe "validations" do
    it { should validate_presence_of(:category) }
    it { should validate_presence_of(:key) }
    it { should validate_presence_of(:display_name) }
    it { should validate_presence_of(:position) }
  it { should_not allow_value("invalid with spaces").for(:key) }
    it { should validate_uniqueness_of(:key).scoped_to(:category_id) }

    it { should_not allow_value("invalid with spaces").for(:key) }
    it { should_not allow_value(-1).for(:position) }
  end

  describe "associations" do
    it { should belong_to(:category) }
  end

  describe "default scope" do
    let!(:category_function_1_position_2) { create(:category_function, position: 2) }
    let!(:category_function_2_position_1) { create(:category_function, position: 1) }

    it 'orders by ascending position' do
      expect(CategoryFunction.all).to eq [category_function_2_position_1, category_function_1_position_2]
    end
  end

end

# == Schema Information
#
# Table name: category_functions
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  key          :string(255)
#  display_name :string(255)
#  position     :integer
#
