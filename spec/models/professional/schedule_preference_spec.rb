require 'spec_helper'

describe SchedulePreference do
  it_should_behave_like "professional profile component"

  it { should have_db_column(:full_time).of_type(:boolean) }
  it { should have_db_column(:part_time).of_type(:boolean) }
  it { should have_db_column(:contract).of_type(:boolean) }
  it { should have_db_column(:internship).of_type(:boolean) }
  it { should have_db_column(:other_concerns).of_type(:text) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should respond_to(:full_time) }
  it { should respond_to(:part_time) }
  it { should respond_to(:contract) }
  it { should respond_to(:internship) }
  it { should respond_to(:other_concerns) }
end

# == Schema Information
#
# Table name: schedule_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  full_time               :boolean          default(FALSE), not null
#  part_time               :boolean          default(FALSE), not null
#  contract                :boolean          default(FALSE), not null
#  other_concerns          :text
#  internship              :boolean
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_schedule_preferences_on_professional_profile_id  (professional_profile_id)
#
