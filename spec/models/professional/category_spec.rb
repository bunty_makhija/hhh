require 'spec_helper'

describe Category do
  describe "db table" do
    it { should have_db_column(:key).of_type(:string) }
    it { should have_db_column(:display_name).of_type(:string) }
    it { should have_db_column(:position).of_type(:integer) }
  end

  describe "validations" do
    it { should validate_presence_of(:key) }
    it { should validate_uniqueness_of(:key) }
    it { should validate_presence_of(:display_name) }
    it { should validate_presence_of(:position) }

    it { should_not allow_value("invalid with spaces").for(:key) }
    it { should_not allow_value(-1).for(:position) }
  end

  describe "associations" do
    it { should have_many(:category_functions) }
  end

  describe "default scope" do
    let!(:category_1_position_2) { create(:category, position: 2) }
    let!(:category_2_position_1) { create(:category, position: 1) }

    it 'orders by ascending position' do
      expect(Category.all).to eq [category_2_position_1, category_1_position_2]
    end
  end

  describe "cascading delete" do
    let!(:profile) { create :professional_profile }
    let!(:category1) { create :category }
    let!(:category2) { create :category }
    let!(:category_function1) { create(:category_function, category: category1) }
    let!(:category_function2) { create(:category_function, category: category2) }

    before do
      profile.category_functions << category_function1
      Category.destroy(category1)
      profile.reload
    end

    it "deletes category 1 from categories" do
      expect(Category.all).to match_array([category2])
    end

    it "deletes category functions of category 1" do
      expect(CategoryFunction.all).to match_array([category_function2])
    end

    it "removes deleted category from profile" do
      expect(profile.category).not_to eq category1
    end

    it "removes deleted category_function from profile" do
      expect(profile.category_functions).not_to include(category_function1)
    end
  end

end

# == Schema Information
#
# Table name: categories
#
#  id           :integer          not null, primary key
#  key          :string(255)
#  display_name :string(255)
#  position     :integer
#
