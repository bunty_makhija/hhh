require 'spec_helper'

describe WorkHistory do
  it_should_behave_like "professional profile component"
  it_should_behave_like "progress tracking"

  it { should have_db_column(:company).of_type(:string) }
  it { should have_db_column(:industry).of_type(:string) }
  it { should have_db_column(:industry_other).of_type(:string) }
  it { should have_db_column(:from_date).of_type(:date) }
  it { should have_db_column(:to_date).of_type(:date) }
  it { should have_db_column(:current).of_type(:boolean) }
  it { should have_db_column(:position).of_type(:string) }
  it { should have_db_column(:summary).of_type(:text) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should belong_to(:professional_profile) }

  it { should respond_to(:professional_profile) }
  it { should respond_to(:company) }
  it { should respond_to(:industry) }
  it { should respond_to(:industry_other) }
  it { should respond_to(:from_date) }
  it { should respond_to(:to_date) }
  it { should respond_to(:current) }
  it { should respond_to(:position) }
  it { should respond_to(:summary) }

  it 'summarizes the work history' do
    history = build :work_history
    desc = history.to_s
    expect(desc).to include history.company
    expect(desc).to include history.industry.to_s
    expect(desc).to include I18n.l history.from_date, format: :short
    expect(desc).to include I18n.l history.to_date, format: :short
    expect(desc).to include history.position
    expect(desc).to include history.summary
  end

  describe "linked in conversion", :vcr do

    describe "convert work history" do

      it "converts a correct linkedin history to a work profile" do
        history = OpenStruct.new(
            startDate: OpenStruct.new(year: 2013, month: 5),
            endDate: OpenStruct.new(year: 2014, month: 3),
            company: OpenStruct.new(name: "DevBootCamp", id: "2539266"),
            title: "Lord High Everything Else",
            isCurrent: true,
            summary: "My job"
        )
        result = WorkHistory.from_linkedin(history)
        expect(result[:from_date].to_s).to eq("2013-05-01")
        expect(result[:to_date].to_s).to eq("2014-03-01")
        expect(result[:company]).to eq("DevBootCamp")
        expect(result[:industry]).to eq("Information Technology & Services")
        expect(result[:position]).to eq("Lord High Everything Else")
        expect(result[:current]).to be_true
        expect(result[:summary]).to eq("My job")
      end

      it "handles missing month information" do
        history = OpenStruct.new(
            startDate: OpenStruct.new(year: 2013),
            endDate: OpenStruct.new(year: 2014),
            company: OpenStruct.new(name: "DevBootCamp", id: "2539266"),
            title: "Lord High Everything Else",
            isCurrent: true,
            summary: "My job"
        )
        result = WorkHistory.from_linkedin(history)
        expect(result[:from_date].to_s).to eq("2013-01-01")
        expect(result[:to_date].to_s).to eq("2014-01-01")
      end

    end

  end

end

# == Schema Information
#
# Table name: work_histories
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  company                 :string(255)
#  industry                :string(255)      default("anything"), not null
#  industry_other          :string(255)
#  from_date               :date
#  to_date                 :date
#  position                :string(255)
#  summary                 :text
#  created_at              :datetime
#  updated_at              :datetime
#  current                 :boolean
#
# Indexes
#
#  index_work_histories_on_professional_profile_id  (professional_profile_id)
#
