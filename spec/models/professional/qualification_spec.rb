require 'spec_helper'

describe Qualification do
  it_should_behave_like 'professional profile component'
  it_should_behave_like 'taggable', :cert, :skill
  it_should_behave_like "progress tracking"

  it { should allow_value(Faker::Lorem.paragraph).for :additional_certs }
  it { should allow_value(Faker::Lorem.paragraph).for :additional_skills }
end

# == Schema Information
#
# Table name: qualifications
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  additional_skills       :text
#  additional_certs        :text
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_qualifications_on_professional_profile_id  (professional_profile_id)
#
