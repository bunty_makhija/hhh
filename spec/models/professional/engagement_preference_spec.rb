require 'spec_helper'
include ActionDispatch::TestProcess

describe EngagementPreference do
  it_should_behave_like "professional profile component"
  it_should_behave_like "progress tracking"

  it { should have_db_column(:starting_date).of_type(:date) }
  it { should have_db_column(:term_quick).of_type(:boolean) }
  it { should have_db_column(:term_small).of_type(:boolean) }
  it { should have_db_column(:term_large).of_type(:boolean) }
  it { should have_db_column(:term_permanent).of_type(:boolean) }
  it { should have_db_column(:term_various).of_type(:boolean) }
  it { should have_db_column(:other_concerns).of_type(:text) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should respond_to(:starting_date) }
  it { should respond_to(:term_quick) }
  it { should respond_to(:term_small) }
  it { should respond_to(:term_large) }
  it { should respond_to(:term_permanent) }
  it { should respond_to(:term_various) }
  it { should respond_to(:other_concerns) }

  context "progress conditions" do
    let(:user) { create :user }
    let(:profile) { create :professional_profile, { user: user, civil: true } }
    let(:engagement_preference) { profile.engagement_preference }

    before do
      engagement_preference.update_attribute(:term_quick, true)
    end

    it { expect(engagement_preference.complete?).to be_false }

    context "complete" do
      let(:resume_pdf) { fixture_file_upload(Rails.root.join('spec/factories/manual.pdf'), 'application/pdf') }

      before do
        profile.update_attribute(:resume, resume_pdf)
        engagement_preference.update_attribute(:starting_date, 1.week.from_now)
        engagement_preference.reload
      end

      it { expect(engagement_preference.complete?).to be_true }
    end

    context "with invalid resume" do
      let(:image_file) { fixture_file_upload(Rails.root.join('spec/factories/image.png'), 'image/png') }

      before do
        profile.update_attribute(:resume, image_file)
        engagement_preference.update_attribute(:starting_date, 1.week.from_now)
        engagement_preference.reload
      end

      it { expect(engagement_preference.complete?).to be_false }
    end
  end
end

# == Schema Information
#
# Table name: engagement_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  starting_date           :date
#  term_quick              :boolean          default(FALSE), not null
#  term_small              :boolean          default(FALSE), not null
#  term_large              :boolean          default(FALSE), not null
#  term_permanent          :boolean          default(FALSE), not null
#  other_concerns          :text
#  created_at              :datetime
#  updated_at              :datetime
#  term_various            :boolean
#
# Indexes
#
#  index_engagement_preferences_on_professional_profile_id  (professional_profile_id)
#
