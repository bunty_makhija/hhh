require "spec_helper"

describe ProfessionalProfile do
  it { should have_db_column(:user_id).of_type(:integer).with_options(null: false) }
  it { should have_db_column(:status).of_type(:string).with_options(null: false) }
  it { should have_db_column(:zip_code).of_type(:string) }
  it { should have_db_column(:associated_with).of_type(:text) }
  it { should have_db_column(:referrals).of_type(:string) }
  it { should have_db_column(:experience).of_type(:string) }
  it { should have_db_column(:authorization).of_type(:boolean) }
  it { should have_db_column(:authorization_details).of_type(:text) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should have_db_index(:user_id) }
  it { should have_db_index(:status) }

  it { should belong_to(:user) }
  it { should have_and_belong_to_many(:category_functions) }
  it { should have_and_belong_to_many(:metro_areas) }
  it { should have_one(:engagement_preference).dependent(:destroy) }
  it { should have_one(:qualification).dependent(:destroy) }
  it { should have_one(:travel_preference).dependent(:destroy) }
  it { should have_one(:schedule_preference).dependent(:destroy) }
  it { should have_one(:salary_preference).dependent(:destroy) }
  it { should have_many(:work_histories).dependent(:destroy) }
  it { should have_many(:education_histories).dependent(:destroy) }
  it { should have_and_belong_to_many(:sub_project_types) }

  it { should respond_to(:status) }
  it { should respond_to(:user) }
  it { should respond_to(:zip_code) }
  it { should respond_to(:associated_with) }
  it { should respond_to(:referrals) }

  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:user) }

  it { should allow_value(Faker::Internet.url).for :resume }
  it { should allow_value(Faker::Internet.url).for :hubspot_resume_url }

  include_examples "enumerized attribute", :professional_profile, :status

  describe "creating" do
    let(:profile) { create :professional_profile }

    it "sets default status to active" do
      expect(profile.status).to eq "active"
    end

    it "creates engagement preferences" do
      expect(profile.engagement_preference).not_to be_nil
    end

    it "creates travel preferences" do
      expect(profile.travel_preference).not_to be_nil
    end

    it "creates schedule preferences" do
      expect(profile.schedule_preference).not_to be_nil
    end

    it "creates salary preferences" do
      expect(profile.salary_preference).not_to be_nil
    end

    it "creates qualification" do
      expect(profile.qualification).not_to be_nil
    end

  end

  describe "profile progress/strength" do
    let(:profile) { create :professional_profile }
    let(:desired_role) { create :audience_type }

    before do
      #stubbing because engagement_preference is complex enough for its own test
      allow(profile.engagement_preference).to receive(:complete?).and_return(true)
    end

    it "shows profile 'strength' progression as preferences are set" do
      expect(profile.progress_conditions.count(true)).to be(2)

      def set_travel_preference(travel_preference)
        travel_preference.home_frequency = "daily"
        travel_preference.will_relocate = false
        travel_preference.region1 = true
      end

      def set_qualification(qualification)
        qualification.additional_skills = Faker::Lorem.sentence
        qualification.additional_certs = Faker::Lorem.sentence
      end

      expect { profile.role_preference.desired_role = desired_role }
      .to change { profile.progress_conditions.count(true) }.by(1)

      expect { set_travel_preference(profile.travel_preference) }
      .to change { profile.progress_conditions.count(true) }.by(1)

      expect { profile.schedule_preference.full_time = true }
      .to change { profile.progress_conditions.count(true) }.by(1)

      expect { profile.salary_preference.hourly_rate = Faker::Number.number(2) }
      .to change { profile.progress_conditions.count(true) }.by(1)

      expect { set_qualification(profile.qualification) }
      .to change { profile.progress_conditions.count(true) }.by(1)

      expect { profile.work_histories << create(:work_history) }
      .to change { profile.progress_conditions.count(true) }.by(1)

      expect { profile.education_histories << create(:education_history) }
      .to change { profile.progress_conditions.count(true) }.by(1)

      expect(profile.complete?).to be_true
    end
  end

  context "#category" do
    let(:profile) { create :professional_profile }

    let(:category) { FactoryGirl.create(:category, key: "engineering", display_name: "Something very descriptive.") }
    let(:alternate_category) { FactoryGirl.create(:category, key: "pokemon_battling", display_name: "Something very descriptive.") }
    let(:category_function) { FactoryGirl.create(:category_function, category_id: category.id, key: "Civil", display_name: "Something very descriptive.", position: 1) }
    let(:alternate_category_function) { create(:category_function, category: alternate_category) }

    before(:each) { profile.category_functions << category_function }

    describe "validation" do

      it "fails when category does not match category_functions" do
        profile.category = alternate_category
        expect(profile).not_to be_valid
      end

      it "succeeds when category matches category_functions" do
        profile.category = category
        expect(profile).to be_valid
      end
    end

    describe "after save" do
      before do
        profile.category_functions << alternate_category_function
      end

      it "changes the category to the category of the new function" do
        expect(profile.category).to eq alternate_category
      end

      it "adds the new category_function and removes non matching category_functions" do
        expect(profile.category_functions).to match_array([alternate_category_function])
      end
    end
  end

  describe "#full_name" do
    let(:profile) { create :professional_profile }

    it "return the user's full name" do
      expect(profile.full_name).to eq profile.user.full_name
    end
  end

  describe "from linked in" do
    let(:profile) { create :professional_profile }
    let(:skills_hash) { {"_total" => 2,
        "values" => [
            {"id" => 12, "skill" => {"name" => "Web Development"}},
            {"id" => 12, "skill" => {"name" => "Ruby on Rails"}}
        ]} }
    let(:certification_hash) { {"_total" => 2,
        "values" => [
            {"id" => 12, "name" => "Web Development"},
            {"id" => 12, "name" => "Ruby on Rails"}
        ]} }

    it "handles skills" do
      profile.add_skills(skills_hash)
      expect(profile.qualification.additional_skills).to eq("Web Development, Ruby on Rails")
    end

    it "handles no skills" do
      profile.add_skills({})
      expect(profile.qualification.additional_skills).to eq("")
    end

    it "handles no values" do
      profile.add_skills({"values" => {}})
      expect(profile.qualification.additional_skills).to eq("")
    end

    it "handles nill skills" do
      profile.add_skills(nil)
      expect(profile.qualification.additional_skills).to eq("")
    end

    it "handles certifications" do
      profile.add_certifications(certification_hash)
      expect(profile.qualification.additional_certs).to eq("Web Development, Ruby on Rails")
    end

    it "handles no certifications" do
      profile.add_certifications({})
      expect(profile.qualification.additional_certs).to eq("")
    end

    it "handles null certifications" do
      profile.add_certifications(nil)
      expect(profile.qualification.additional_certs).to eq("")
    end

    it "handles empty certifications" do
      profile.add_certifications({"values" => {}})
      expect(profile.qualification.additional_certs).to eq("")
    end
  end

  context "resume display name" do
    context "without a resume" do
      let(:profile) { build :professional_profile }
      it { expect(profile.resume_display_name).to be_nil }
    end

    context "without the resume original filename" do
      let(:file) { File.new(Rails.root.join("spec/factories/manual.pdf")) }
      let(:resume) { double("fake resume uploader") }
      let(:profile) { build :professional_profile }

      before do
        expect(profile).to receive(:resume).and_return(resume).twice
        expect(resume).to receive(:file).and_return(file)
        expect(resume).to receive(:path).and_return("uploads/previous_upload.rtf")
      end

      it { expect(profile.resume_display_name).to eq "previous_upload.rtf" }
    end

    context "with a resume original filename" do
      let(:profile) do
        build :professional_profile, resume_original_filename:
          "my_original_filename is longer than 40_characters-and-has punctuation!.pdf"
      end
      it { expect(profile.resume_display_name).to eq "my_original_filename is longer than 4..." }
    end
  end

  context "profile with project type and sub project types" do
    let(:profile) { create :professional_profile }
    let(:typeA) { create(:project_type, position: 1) }
    let(:typeB) { create(:project_type, position: 2) }
    let(:sub_typeBA) { FactoryGirl.create(:sub_project_type, project_type: typeB, key: 'typeBA', display_name: 'sub BA') }

    before do
      FactoryGirl.create(:sub_project_type, project_type: typeA, key: 'typeAA', display_name: 'sub AA')
      FactoryGirl.create(:sub_project_type, project_type: typeA, key: 'typeAB', display_name: 'sub AB')
      profile.update_attribute :sub_project_types, SubProjectType.all
      profile.sub_project_types << sub_typeBA
    end

    describe "initialization" do
      it { expect(profile.project_types.size).to eq 2 }
      it { expect(profile.project_types).to include(typeA) }
      it { expect(profile.project_types).to include(typeB) }
    end

    describe "#remove_sub_project_type" do

      before do
        profile.project_types.delete(typeA)
      end

      it { expect(profile.project_types.size).to eq 1 }
      it { expect(profile.sub_project_types.size).to eq 1 }
      it { expect(profile.sub_project_types).to include(sub_typeBA) }

      it "does not destroy parent Project Types or Sub Project Types" do
        expect(ProjectType.all.count).to eq 2
        expect(SubProjectType.all.count).to eq 3
      end
    end
  end

  describe "#add_project_types" do
    context "with a project type that has two sub project types" do
      let(:profile) { create :professional_profile }
      let(:typeZ) { FactoryGirl.create(:project_type, key: 'typeZ', display_name: 'I am type Z', position: 1) }
      let(:sub_typeZZ) { FactoryGirl.create(:sub_project_type, project_type: typeZ, key: 'typeZZ', display_name: 'sub ZZ') }
      let(:sub_typeZZ_2) { FactoryGirl.create(:sub_project_type, project_type: typeZ, key: 'typeZZ_2', display_name: 'sub ZZ2') }

      before do
        profile.sub_project_types << sub_typeZZ
        profile.sub_project_types << sub_typeZZ_2
      end

      it "adds correct project type and sub project types" do
        expect(profile.project_types).to include(typeZ)
        expect(profile.sub_project_types).to include(sub_typeZZ)
        expect(profile.sub_project_types).to include(sub_typeZZ_2)
      end

      it "only adds the project type once" do
        expect(profile.project_types.size).to eq 1
      end
    end
  end

  describe "hubspot resume url" do
    let(:profile) { create :professional_profile,
      hubspot_resume_url: "https://s3.amazonaws.com/uploads/my%20resume.pdf" }

    it { expect(profile.hubspot_resume_filename).to eq "my resume.pdf" }
    it { expect(profile.resume?).to be_false }

    describe "when a resume is just saved from rails site" do
      before do
        expect(profile).to receive(:resume?).and_return(true)
        profile.save
      end

      it { expect(profile.hubspot_resume_url).to be_nil }
    end
  end

  describe "add metro_areas" do
    let(:profile) { create :professional_profile }
    let(:metro_area) { create :metro_area }

    before do
      profile.metro_areas << metro_area
      profile.reload
    end

    it "adds the metro_area to the profile" do
      expect(profile.metro_areas).to match_array([metro_area])
    end

    context "removes metro_area from profile when the metro_area is destroyed" do
      before do
        metro_area.destroy
      end

      it "removes the metro_area from the profile" do
        expect(profile.metro_areas).to be_empty
      end
    end
  end
end

# == Schema Information
#
# Table name: professional_profiles
#
#  id                       :integer          not null, primary key
#  user_id                  :integer          not null
#  status                   :string(255)      not null
#  zip_code                 :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  resume                   :string(255)
#  civil                    :boolean
#  energy                   :boolean
#  building                 :boolean
#  anything                 :boolean
#  industry_other           :string(255)
#  completed_at             :datetime
#  associated_with          :text
#  referrals                :string(255)
#  country                  :string(255)
#  state                    :string(255)
#  city                     :string(255)
#  infrastructure           :boolean
#  petroleum                :boolean
#  summary                  :text
#  hubspot_resume_url       :string(255)
#  resume_original_filename :string(255)
#  experience               :string(255)
#  authorization            :boolean
#  authorization_details    :text
#  category_id              :integer
#  wizard_completed_at      :datetime
#
# Indexes
#
#  index_professional_profiles_on_status   (status)
#  index_professional_profiles_on_user_id  (user_id)
#
