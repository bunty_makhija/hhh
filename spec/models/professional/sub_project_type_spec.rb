require 'spec_helper'

describe SubProjectType do
  context "db_columns" do
    it { should have_db_column(:key).of_type(:string) }
    it { should have_db_column(:project_type_id).of_type(:integer) }
    it { should have_db_column(:display_name).of_type(:string) }
    it { should have_db_column(:position).of_type(:integer) }
  end

  context "associations" do
    it { should belong_to(:project_type) }
    it { should have_and_belong_to_many(:professional_profiles) }
  end

  context "validations" do
    it { should validate_presence_of(:key) }
    it { should validate_presence_of(:project_type) }
    it { should validate_presence_of(:display_name) }
    it { should validate_presence_of(:position) }
    it { should validate_uniqueness_of(:key).scoped_to(:project_type_id) }

    it { should allow_value("engineer").for(:key) }
    it { should_not allow_value("this is an invalid key").for(:key) }
    it { should_not allow_value(-1).for(:position) }
  end

  describe "default scope" do
    let(:sub_project_type1) { create(:sub_project_type, position: 2) }
    let(:sub_project_type2) { create(:sub_project_type, position: 1) }

    it 'orders by ascending position' do
      expect(SubProjectType.all).to eq [sub_project_type2, sub_project_type1]
    end
  end

end

# == Schema Information
#
# Table name: sub_project_types
#
#  id              :integer          not null, primary key
#  project_type_id :integer
#  key             :string(255)
#  display_name    :string(255)
#  position        :integer
#
