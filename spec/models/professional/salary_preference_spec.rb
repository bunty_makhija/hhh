require 'spec_helper'

describe SalaryPreference do
  it_should_behave_like "professional profile component"
  it_should_behave_like "progress tracking"

  it { should have_db_column(:base_salary).of_type(:string) }
  it { should have_db_column(:hourly_rate).of_type(:string) }
  it { should have_db_column(:daily_travel_comp).of_type(:string) }
  it { should have_db_column(:other_concerns).of_type(:text) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should respond_to(:base_salary) }
  it { should respond_to(:hourly_rate) }
  it { should respond_to(:daily_travel_comp) }
  it { should respond_to(:other_concerns) }

  describe "progress conditions" do
    let(:salary) { create :salary_preference }

    it "is complete with base salary only" do
      salary.update(base_salary: 'value')
      expect(salary).to be_complete
    end

    it "is complete with hourly rate only" do
      salary.update(hourly_rate: 'value')
      expect(salary).to be_complete
    end
  end
end

# == Schema Information
#
# Table name: salary_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  base_salary             :string(255)
#  hourly_rate             :string(255)
#  daily_travel_comp       :string(255)
#  other_concerns          :text
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_salary_preferences_on_professional_profile_id  (professional_profile_id)
#
