require 'spec_helper'

describe ProjectType do
  it { should have_db_column(:key).of_type(:string) }
  it { should have_db_column(:display_name).of_type(:string) }
  it { should have_db_column(:position).of_type(:integer) }

  it { should have_many(:sub_project_types).dependent(:destroy) }

  it { should validate_presence_of(:key) }
  it { should validate_presence_of(:display_name) }
  it { should validate_presence_of(:position) }
  it { should validate_uniqueness_of(:key)}

  it { should allow_value("engineer").for(:key) }
  it { should_not allow_value("this is an invalid key").for(:key) }
  it { should_not allow_value(-1).for(:position) }

  describe "default scope" do
    let(:project_type1) { create(:project_type, position: 2) }
    let(:project_type2) { create(:project_type, position: 1) }

    it 'orders by ascending position' do
      expect(ProjectType.all).to eq [project_type2, project_type1]
    end
  end

  describe "cascading delete" do
    let!(:profile) { create :professional_profile }
    let!(:project_type1) { create :project_type }
    let!(:project_type2) { create :project_type }
    let!(:sub_project_type1) { create(:sub_project_type, project_type: project_type1) }
    let!(:sub_project_type2) { create(:sub_project_type, project_type: project_type2) }

    before do
      profile.sub_project_types << SubProjectType.all
      project_type1.destroy
      profile.reload
    end

    it "deletes project_type1 from project types" do
      expect(ProjectType.all).to match_array([project_type2])
    end

    it "deletes sub project types of project type 1" do
      expect(SubProjectType.all).to match_array([sub_project_type2])
    end

    it "removes deleted project type from profile" do
      expect(profile.project_types).to match_array([project_type2])
    end

    it "removes deleted sub project type from profile" do
      expect(profile.sub_project_types).to match_array([sub_project_type2])
    end
  end

end

# == Schema Information
#
# Table name: project_types
#
#  id           :integer          not null, primary key
#  key          :string(255)
#  display_name :string(255)
#  position     :integer
#
