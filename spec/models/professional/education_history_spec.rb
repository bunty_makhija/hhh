require 'spec_helper'

describe EducationHistory do
  it_should_behave_like "professional profile component"
  it_should_behave_like "taggable", :concentration
  it_should_behave_like "progress tracking"

  it { should have_db_column(:achievement_level).of_type(:string) }
  it { should have_db_column(:graduation_date).of_type(:date) }
  it { should have_db_column(:institution).of_type(:string) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should belong_to(:professional_profile) }

  it { should respond_to(:professional_profile) }
  it { should respond_to(:achievement_level) }
  it { should respond_to(:graduation_date) }
  it { should respond_to(:institution) }

  it 'describes the education history in a sentence' do
    history = build :education_history
    desc = history.to_s
    expect(desc).to include history.achievement_level.to_s
    expect(desc).to include history.graduation_date.year.to_s
    expect(desc).to include history.institution
    history.concentration_list.each { |conc| expect(desc).to include conc }
  end

  it "creates from linkedin data" do
    history = OpenStruct.new(
        endDate: OpenStruct.new(year: 2000),
        schoolName: "Illinois",
        degree: "BA",
        fieldOfStudy: "Computer Science"
    )
    result = EducationHistory.from_linkedin(history)
    expect(result.graduation_date.to_s).to eq("2000-01-01")
    expect(result.institution).to eq("Illinois")
    expect(result.achievement_level).to eq("BA")
    expect(result.concentration_list).to eq(["Computer Science"])
  end

end

# == Schema Information
#
# Table name: education_histories
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  achievement_level       :string(255)
#  graduation_date         :date
#  created_at              :datetime
#  updated_at              :datetime
#  institution             :string(255)
#
# Indexes
#
#  index_education_histories_on_professional_profile_id  (professional_profile_id)
#
