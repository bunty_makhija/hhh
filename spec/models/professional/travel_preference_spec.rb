require 'spec_helper'

describe TravelPreference do
  it_should_behave_like "professional profile component"
  it_should_behave_like "progress tracking"

  it { should have_db_column(:will_travel).of_type(:boolean) }
  it { should have_db_column(:will_relocate).of_type(:boolean) }
  it { should have_db_column(:home_frequency).of_type(:string) }
  it { should have_db_column(:region1).of_type(:boolean) }
  it { should have_db_column(:region2).of_type(:boolean) }
  it { should have_db_column(:region3).of_type(:boolean) }
  it { should have_db_column(:region4).of_type(:boolean) }
  it { should have_db_column(:region5).of_type(:boolean) }
  it { should have_db_column(:region6).of_type(:boolean) }
  it { should have_db_column(:region7).of_type(:boolean) }
  it { should have_db_column(:region8).of_type(:boolean) }
  it { should have_db_column(:region9).of_type(:boolean) }
  it { should have_db_column(:region10).of_type(:boolean) }
  it { should have_db_column(:other_concerns).of_type(:text) }
  it { should have_db_column(:created_at).of_type(:datetime) }
  it { should have_db_column(:updated_at).of_type(:datetime) }

  it { should validate_presence_of(:home_frequency) }

  it { should respond_to(:will_travel) }
  it { should respond_to(:will_relocate) }
  it { should respond_to(:home_frequency) }
  it { should respond_to(:region1) }
  it { should respond_to(:region2) }
  it { should respond_to(:region3) }
  it { should respond_to(:region4) }
  it { should respond_to(:region5) }
  it { should respond_to(:region6) }
  it { should respond_to(:region7) }
  it { should respond_to(:region8) }
  it { should respond_to(:region9) }
  it { should respond_to(:region10) }
  it { should respond_to(:other_concerns) }

  include_examples "enumerized attribute", :travel_preference, :home_frequency
end

# == Schema Information
#
# Table name: travel_preferences
#
#  id                      :integer          not null, primary key
#  professional_profile_id :integer          not null
#  will_travel             :boolean          default(FALSE), not null
#  will_relocate           :boolean          default(FALSE), not null
#  region1                 :boolean          default(FALSE), not null
#  region2                 :boolean          default(FALSE), not null
#  region3                 :boolean          default(FALSE), not null
#  region4                 :boolean          default(FALSE), not null
#  region5                 :boolean          default(FALSE), not null
#  region6                 :boolean          default(FALSE), not null
#  region7                 :boolean          default(FALSE), not null
#  region8                 :boolean          default(FALSE), not null
#  region9                 :boolean          default(FALSE), not null
#  region10                :boolean          default(FALSE), not null
#  other_concerns          :text
#  home_frequency          :string(255)      default("daily"), not null
#  created_at              :datetime
#  updated_at              :datetime
#
# Indexes
#
#  index_travel_preferences_on_professional_profile_id  (professional_profile_id)
#
