require 'spec_helper'

describe Redirect do
  it { should validate_presence_of :old_path }
  it { should validate_presence_of :new_url }

  it 'downcases the uris before saving' do
    old_path = Faker::Internet.url.upcase.gsub /https:\/\/.+\//, '/'
    new_url = Faker::Internet.url.upcase
    redirect = described_class.create old_path: old_path, new_url: new_url
    expect(redirect.old_path).to eq old_path.downcase
    expect(redirect.new_url).to eq new_url.downcase
  end
end

# == Schema Information
#
# Table name: redirects
#
#  id       :integer          not null, primary key
#  old_path :string(255)
#  new_url  :string(255)
#
