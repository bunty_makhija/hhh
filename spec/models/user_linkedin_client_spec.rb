require 'spec_helper'

describe UserLinkedinClient, :vcr do

  describe "company information" do

    it "handles an accurate id" do
      expect(UserLinkedinClient.industry_for_company(2539266)).to eq(
          "Information Technology & Services")
    end

    it "handles an silly id" do
      expect(UserLinkedinClient.industry_for_company(-3234)).to eq("")
    end

  end

end
