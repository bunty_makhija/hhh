require 'spec_helper'

describe ActsAsTaggableOn::Tag do

  it { should validate_uniqueness_of(:name).scoped_to(:original_context) }

  it 'is extended with predefined tag contexts' do
    expect(described_class.included_modules).to include TagContext
  end

end
