require "spec_helper"

describe ProfessionalHelper do

  let(:profile) { create :professional_profile }

  it "was not just completed if there is no completed_at date" do
    expect(profile.completed_at).to be_nil
    expect(helper.profile_just_completed?(profile)).to be_false
  end

  it "was just completed if completed_at exists and was saved within the minute" do
    profile.update_attribute :completed_at, Time.now
    Time.stub(:now).and_return profile.completed_at
    expect(helper.profile_just_completed?(profile)).to be_true
  end

  it "was not just completed if completed_at is older than 1 minute" do
    profile.update_attribute :completed_at, Time.now
    Time.stub(:now).and_return profile.completed_at - 5.minutes
    expect(helper.profile_just_completed?(profile)).to be_false
  end
  
end
