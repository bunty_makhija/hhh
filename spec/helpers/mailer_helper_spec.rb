require "spec_helper"

describe MailerHelper do
  describe "#tracking_param_hash" do

    it "returns a valid hash of parameters" do
      test = MailerHelper::tracking_param_hash("a", "b", "c")
      expect(test).to be_a Hash
      expect(test).to eq({utm_source: "a", utm_medium: "b", utm_campaign: "c"})
    end
  end

  describe "#email_tracking_params" do
    it "returns a hash with the expected values" do
      test = MailerHelper::email_tracking_params("reset-password")
      expect(test).to eq({utm_source: "hhh", utm_medium: "email", utm_campaign: "reset-password"})
    end
  end

  describe "#email_tracking_param_string" do
    it "returns a string of encoded parameters" do
      test = MailerHelper::email_tracking_param_string({value1: "string", value2: "string"})
      expect(test).to be_a String
      expect(test).to eq("value1=string&value2=string")
    end
  end
end
