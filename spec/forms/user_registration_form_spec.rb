require "spec_helper"
include ActionDispatch::TestProcess

describe UserRegistrationForm do
  let(:form) { UserRegistrationForm.new(ActionController::Parameters.new(form_params)) }
  let(:form_params) do
    { user_registration_form:
      { first_name: "MyFirstName",
        last_name: "MyLastName",
        email: "me@me.com",
        zip_code: "60606-0987",
        country: "USA",
        civil: true
      }
    }
  end

  describe "successful registration" do
    include_context "with clear deliveries array"

    describe 'return value' do
      it { expect(form.save).to be_true}
    end

    context "with correct information filled in" do
      let(:hubspot_form) { double("HubspotterInterface::SignupForm") }

      before do
        form.save
      end

      it { expect(form.errors.full_messages).to be_empty }
      it { expect(form.first_name).to eq "MyFirstName" }
      it { expect(form.last_name).to eq "MyLastName" }
      it { expect(form.email).to eq "me@me.com" }
      it { expect(form.zip_code).to eq "60606-0987" }
      it { expect(form.civil).to be_true }

      describe "model creation" do
        it { expect(User.last.first_name).to eq "MyFirstName" }
        it { expect(ProfessionalProfile.last.zip_code).to eq "60606-0987" }
      end

      it 'does not send a confirmation email' do
        expect(Devise::Mailer).not_to receive(:confirmation_instructions)
        expect(deliveries.length).to eq 0
      end
    end
  end

  describe "failure with nested attributes for profile" do
    let(:user_without_profile) { create(:user) }

    before do
      # Force form#create_user_and_profile to fail profile by generating
      # a new User without profile information
      expect(User).to receive(:new).and_return(user_without_profile)
    end

    it 'does not create user' do
      expect { form.save }.not_to change { User.count }
    end

    it 'does not create profile' do
      expect { form.save }.not_to change { ProfessionalProfile.count }
    end
  end

  describe "validation failure" do
    context "with error on form" do
      before do
        form_params[:user_registration_form].delete(:first_name)
        form.save
      end

      it { expect(form.save).to be_false }
      it { expect(form.errors.full_messages).to include "First name can't be blank" }
      it { expect(form.first_name).to be_nil }
      it { expect(form.last_name).to eq "MyLastName" }
      it { expect(form.email).to eq "me@me.com" }
      it { expect(form.zip_code).to eq "60606-0987" }
      it { expect(form.country).to eq "USA" }
      it { expect(form.civil).to be_true }
    end

    context "with error on delegated model" do
      let!(:duplicate_user) { create(:user, email: form.email) }

      before do
        form.save
      end

      it { expect(form.save).to be_false }
      it { expect(form.errors.full_messages).to include "Email has already been taken" }
    end
  end
end
