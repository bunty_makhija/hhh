require 'spec_helper'

describe GetStarted::LastThingStep do
  let(:professional_profile) { create :professional_profile }
  let(:params) { ActionController::Parameters.new(get_started_last_thing_step: form_params) }
  let(:form_params) { { experience: nil, authorization: nil, authorization_details: nil } }
  subject(:last_thing_form) { described_class.new(professional_profile.user, params) }

  it { should delegate(:resume).to :professional_profile }

  describe "#save" do
    describe "success" do
      let (:form_params) { {resume: '/some/fake/file'} }

      it "should update the professional profile resume" do
        last_thing_form.save
        expect(professional_profile.resume).to be_instance_of ResumeUploader
      end

      it "should update the professional profile resume while ignoring other params" do
        params[:city] = "Not a city"
        last_thing_form.save
        expect(professional_profile.city).not_to eq(params[:city])
      end
    end

    describe "failure" do
      before do
        last_thing_form.save
      end

      it "should not update resume" do
        expect(professional_profile.resume_display_name).to eq(nil)
      end
    end
  end

end
