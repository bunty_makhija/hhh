require "spec_helper"

describe ProfessionalProfileForm do
  let(:professional_profile) { create :professional_profile }
  let(:params) { {} }
  subject(:professional_profile_form) { ProfessionalProfileForm.new(professional_profile, params) }

  it { should delegate(:status).to :professional_profile }
  it { should delegate(:civil).to :professional_profile }
  it { should delegate(:infrastructure).to :professional_profile }
  it { should delegate(:energy).to :professional_profile }
  it { should delegate(:building).to :professional_profile }
  it { should delegate(:petroleum).to :professional_profile }
  it { should delegate(:anything).to :professional_profile }
  it { should delegate(:industry_other).to :professional_profile }
  it { should delegate(:zip_code).to :professional_profile }
  it { should delegate(:resume).to :professional_profile }
  it { should delegate(:engagement_preference).to :professional_profile }
  it { should delegate(:first_name).to :user }
  it { should delegate(:last_name).to :user }
  it { should delegate(:phone_number).to :user }

  context "updating user values" do
    let(:params) do
      { first_name:   Faker::Name.first_name,
        last_name:    Faker::Name.last_name,
        phone_number: Faker::PhoneNumber.phone_number }
    end
    it "should update the user" do
      professional_profile_form.process
      params.each do |attribute, value|
        expect(professional_profile.user.send(attribute)).to eq(value)
      end
    end
  end

  context "updating professional_profile values" do
    let(:resume_param) do {
      resume: '/some/fake/file'
    } end

    let(:engagement_preference_params) do {
      engagement_preference_attributes: {
        term_quick: true,
        term_small: false,
        term_large: true
      }
    } end

    let(:pro_profile_params) do {
      status: ProfessionalProfile.status.values.sample,
      civil: true,
      infrastructure: true,
      energy: true,
      building: true,
      petroleum: true,
      anything: true,
      industry_other: Faker::Company.name,
      zip_code: Faker::Address.zip_code
    } end

    let(:params) do
      pro_profile_params.merge(resume_param).merge(engagement_preference_params)
    end

    it "should update the professional_profile" do
      professional_profile_form.process
      pro_profile_params.each do |attribute, value|
        expect(professional_profile.send(attribute)).to eq(value)
      end

      expect(professional_profile.resume).to be_instance_of ResumeUploader
      engagement_preference_params[:engagement_preference_attributes].each do |attribute, value|
        expect(professional_profile.engagement_preference.send(attribute)).to eq(value)
      end
    end
  end

  context "with incorrect inputs" do
    let(:params) { {last_name: '', status: false} }
    it "should collect errors" do
      professional_profile_form.process
      expect(professional_profile_form.errors.size).to be 3
    end
  end
end
