require "spec_helper"

describe GetStarted::IndustrySkipLogic do
  let(:user) { create(:user) }
  let(:professional_profile) { user.professional_profile }

  subject(:industry_skip_logic) { described_class.new(user) }

  context "#has_industry_but_not_linkedin?" do
    before { professional_profile.civil = true }

    it "should skip the page" do
      expect(industry_skip_logic).to be_skip_page
    end
  end

  context "has an inudstry and linkedin" do
    before do
      professional_profile.civil = true
      user.provider = "linkedin"
    end

    it "should not skip the page" do
      expect(industry_skip_logic).not_to be_skip_page
    end
  end

  context "has no industry or linkedin" do
    it "should not skip the page" do
      expect(industry_skip_logic).not_to be_skip_page
    end
  end
end
