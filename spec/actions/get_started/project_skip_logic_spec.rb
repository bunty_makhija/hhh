require "spec_helper"

describe GetStarted::ProjectSkipLogic do
  let(:user) { create(:user) }

  let!(:facilities_category) { FactoryGirl.create(:category, key: 'facilities_management') }
  let!(:facilities_project_type) { FactoryGirl.create(:project_type, key: 'buildings_facilities') }
  let(:project_types) { user.professional_profile.project_types }

  subject(:project_skip_logic) { described_class.new(user) }

  shared_context "when the category is correct" do
    before { user.professional_profile.update(category: facilities_category) }
  end

  shared_context "and the project type is in the list" do
    before { user.professional_profile.update(project_types: [facilities_project_type]) }
  end

  describe "#attach_project_type_if_needed" do
    context "when the category is correct" do
      include_context "when the category is correct"

      before(:each) { project_skip_logic.attach_project_type_if_needed }

      context "and the project type is not in the list" do
        it "should add the project type" do
          project_skip_logic.attach_project_type_if_needed
          expect(project_types).to include facilities_project_type
        end

        it "should not add the project type if called twice" do
          project_skip_logic.attach_project_type_if_needed
          project_skip_logic.attach_project_type_if_needed
          expect(project_types.length).to be 1
          expect(project_types).to include facilities_project_type
        end
      end

      context "and the project type is in list" do
        include_context "and the project type is in the list"

        it "should not add an additional project type" do
          project_skip_logic.attach_project_type_if_needed
          expect(project_types.length).to be 1
          expect(project_types).to include facilities_project_type
        end
      end
    end

    context "when the category is incorrect" do
      before(:each) { project_skip_logic.attach_project_type_if_needed }

      context "and the project type is not in the list" do
        it "should not add the project type" do
          project_skip_logic.attach_project_type_if_needed
          expect(project_types).not_to include facilities_project_type
        end
      end

      context "and the project type is in list" do
        include_context "and the project type is in the list"

        before(:each) { project_skip_logic.attach_project_type_if_needed }

        it "should not add an additional project type" do
          project_skip_logic.attach_project_type_if_needed
          expect(project_types.length).to be 1
          expect(project_types).to include facilities_project_type
        end
      end
    end
  end

  describe "#attach_project_type_if_needed" do
    context "in the exact situation of tihs category and only this project type" do
      include_context "when the category is correct"
      include_context "and the project type is in the list"

      it "should skip the page" do
        expect(project_skip_logic).to be_skip_page
      end
    end

    context "if the category is incorrect" do
      include_context "and the project type is in the list"

      it "should not skip the page" do
        expect(project_skip_logic).not_to be_skip_page
      end
    end

    context "if there is more than one project type" do
      include_context "when the category is correct"

      before { user.professional_profile.update(project_types: [facilities_project_type, FactoryGirl.create(:project_type)]) }

      it "should not skip the page" do
        expect(project_skip_logic).not_to be_skip_page
      end
    end
  end
end
