require "spec_helper"

describe GetStarted::StepManager do
  let(:user) { create(:user) }
  let(:step_name) { :welcome }
  subject(:step_manager) { described_class.new(user, step_name, {}) }

  describe "#current_step" do
    context "when the user has not completed anything" do
      it "should return the starting_step" do
        expect(step_manager.starting_step.slug).to eq :welcome
      end
    end

    context "when the user has partially completed of the wizard" do
      xit "should return the first applicable step" do
      end
    end

    context "when the user has completed the wizard" do
      xit "should redirect to some page" do
      end
    end
  end

  describe ".step_for" do
    it "returns a step class that matches the step_name" do
      expect(described_class.step_for(:welcome)).to eq GetStarted::WelcomeStep
    end

    it "returns nil if step with provided name does not exist" do
      expect(described_class.step_for(:alakazam)).to eq nil
    end
  end

  describe "#skip_current_step?" do
    it "returns true if the step should be skipped" do
      expect(step_manager.current_step).to receive(:force_skip?).and_return(true)
      expect(step_manager.skip_current_step?).to eq(true)
    end

    it "returns false if the step cannot be skipped" do
      expect(step_manager.current_step).to receive(:force_skip?).and_return(false)
      expect(step_manager.skip_current_step?).to eq(false)
    end
  end

  describe "#starting_step" do
    context "when nothing has been completed" do
      it "returns the first step in the list" do
        expect(step_manager.starting_step).to be_a(described_class.step_list.first)
      end
    end

    context "with no current step" do
      let(:step_name) { nil }
      it "returns the starting step as the current step" do
        expect(step_manager.current_step).to be_a(described_class.step_list.first)
      end
    end
  end

  describe "#previous_valid" do
    context "when the previous step is not skippable" do
      let(:step_name) { :function }

      it "returns the previous step" do
        expect(step_manager.previous_valid_step).to be_a(GetStarted::IndustryStep)
      end
    end

    context "when the previous step is skippable" do
      let(:step_name) { :specialties }

      it "returns the step before the previous step" do
        expect_any_instance_of(GetStarted::ProjectsStep).to receive(:force_skip?).and_return true
        expect(step_manager.previous_valid_step).to be_a(GetStarted::FunctionStep)
      end
    end
  end

  describe "#next_valid_step" do
    context "when the next step is not skippable" do
      let(:step_name) { :welcome }

      it "returns the next step" do
        expect(step_manager.next_valid_step).to be_a(GetStarted::IndustryStep)
      end
    end

    context "when the next step is skippable" do
      let(:step_name) { :function }

      it "returns the step after the next step" do
        expect_any_instance_of(GetStarted::ProjectsStep).to receive(:force_skip?).and_return true
        expect(step_manager.next_valid_step).to be_a(GetStarted::SpecialtiesStep)
      end
    end
  end
end
