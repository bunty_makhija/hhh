require 'spec_helper'

describe HubspotterInterface::ProfileWorkForm do
  let(:user) { create(:user) }
  let(:profile) { create(:professional_profile, user: user) }
  let(:work_from_1) { (Date.today - 4.years).strftime('%m/%Y') }
  let(:work_to_1)   { (Date.today - 2.years).strftime('%m/%Y') }

  let(:work1) { create(:work_history,
    company: "Orbitz",
    industry: "civil",
    from_date: Date.today - 4.years,
    to_date: Date.today - 2.years,
    position: "Civil Engineer Level1",
    summary: "I worked here")}

  before do
    user.update_attribute :professional_profile, profile
    profile.work_histories = [work1]
    profile.save
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        email: user.email,
        work_history: "#{work_from_1} - #{work_to_1}\nOrbitz\nCivil Services\nCivil Engineer Level1\nI worked here\n===="
      }
    end

    it 'builds expected form data' do
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end
end
