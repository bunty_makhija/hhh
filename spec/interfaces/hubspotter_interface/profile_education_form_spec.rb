require 'spec_helper'

describe HubspotterInterface::ProfileEducationForm do
  let(:user) { create(:user, email: "test@tablexi.com", professional_profile: profile )}
  let(:profile) { create(:professional_profile, education_histories: [edu]) }

  let(:grad_date) { (Date.today - 2.years).strftime('%m/%Y') }
  let(:edu) { create(:education_history,
    graduation_date: Date.today - 2.years,
    institution: "University of Chicago",
    achievement_level: "bachelors") }

  before do
    edu.concentration_list = ["C1", "C2"]
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        email: "test@tablexi.com",
        education_history: "#{grad_date}\nUniversity of Chicago\nbachelors\nC1, C2\n===="
      }
    end

    it 'builds expected form data' do
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end
end
