require 'spec_helper'

describe HubspotterInterface::SignupForm do
  let(:user) { create(:user) }
  let(:role_group) { create(:audience_type) }
  let(:profile) do
    create(:professional_profile,
      user: user,
      zip_code: "11111",
      referrals: "Mary Referrer",
      civil: true)
  end

  before do
    user.update_attribute :professional_profile, profile
    profile.role_preference.desired_role = role_group
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_created_at: user.created_at.strftime("%m/%d/%Y"),
        firstname: user.first_name,
        lastname:  user.last_name,
        email:     user.email,
        zip:       "11111",
        referred_by_name_: "Mary Referrer",
        function: role_group.name,
        preferred_industries: ["Civil Services"],
        resume:    "https://resume_url"
      }
    end

    it 'builds expected form data' do
      expect(profile).to receive(:resume_url).and_return("https://resume_url")
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end

end
