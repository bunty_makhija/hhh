require 'spec_helper'

describe HubspotterInterface::ProfileSalaryForm do
  let(:user) { create(:user) }
  let(:profile) { create(:professional_profile, user: user) }

  before do
    user.update_attribute :professional_profile, profile
    salary = profile.salary_preference
    salary.base_salary = "$150,000"
    salary.hourly_rate = "$100/hr"
    salary.daily_travel_comp = "$50"
    salary.other_concerns = "Here are salary concerns"
    salary.save
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        email: user.email,
        base_salary: "$150,000",
        hourly_rate: "$100/hr",
        travel_per_diem: "$50",
        salary_concerns: "Here are salary concerns"
      }
    end

    it 'builds expected form data' do
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end
end
