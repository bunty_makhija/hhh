require 'spec_helper'

describe HubspotterInterface::WebhookParser do
  let(:parser) { HubspotterInterface::WebhookParser.new(CONTACT_PAYLOAD) }

  before do
    create(:audience_type, name: "Engineers", id: 2)
  end

  describe '#parse_and_create_user' do
    subject { parser.parse_and_create_user }
    its(:first_name) { should eq "Webhook" }
    its(:last_name) { should eq "Test" }
    its(:email) { should eq "webhook+test@tablexi.com" }
    its(:password) { should_not be_nil }
    its(:confirmed_at) { should_not be_nil }
    it { expect(subject.valid?).to be_true }
    it { expect(subject.errors.full_messages).to eq [] }
    it { expect(subject.persisted?).to be_true }
    it { expect(subject.professional_profile.zip_code).to eq "11111" }
    it { expect(subject.professional_profile.referrals).to eq "referral@referrals.com" }
    it { expect(subject.professional_profile.building).to be_true }
    it { expect(subject.professional_profile.infrastructure).to be_true }
    it { expect(subject.professional_profile.role_preference.audience_type_id).to eq 2 }
    it { expect(subject.professional_profile.hubspot_resume_url).to eq "https://s3.amazonaws.com/uploads/my%20resume%20with%20(spaces).rtf" }

    context "user with all fields other than email unpopulated" do
      let(:empty_parser) { HubspotterInterface::WebhookParser.new(EMPTY_CONTACT_PAYLOAD) }
      it { expect(empty_parser.parse_and_create_user).to be_nil }
    end

    context "user with minimum fields to be valid" do
      let(:minimal_parser) { HubspotterInterface::WebhookParser.new(MINIMAL_CONTACT_PAYLOAD) }
      subject { minimal_parser.parse_and_create_user }
      its(:first_name) { should eq "Webhook" }
      its(:last_name) { should eq "Test" }
      its(:email) { should eq "webhook+test@tablexi.com" }
      its(:password) { should_not be_nil }
      its(:confirmed_at) { should_not be_nil }
      it { expect(subject.valid?).to be_true }
      it { expect(subject.errors.full_messages).to eq [] }
      it { expect(subject.persisted?).to be_true }
      it { expect(subject.professional_profile.zip_code).to be_nil }
      it { expect(subject.professional_profile.referrals).to be_nil }
      it { expect(subject.professional_profile.role_preference.audience_type_id).to be_nil }
    end
  end

  describe '#parse_and_find_or_create_user' do
    context "user already exists" do
      let(:minimal_parser) { HubspotterInterface::WebhookParser.new(MINIMAL_CONTACT_PAYLOAD) }
      let!(:user) { create(:user, email: "webhook+test@tablexi.com") }

      it 'finds and returns the existing user' do
        expect(minimal_parser.parse_and_find_or_create_user).to eq user
      end
    end
  end
end

CONTACT_PAYLOAD =
%Q[{"vid":20239,"canonical-vid":20239,"merged-vids":[],
  "properties":{
    "firstname":{"value":"Webhook","versions":[{"value":"Webhook","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408673485008,"selected":false}]},
    "lastname":{"value":"Test","versions":[{"value":"Test","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408673485008,"selected":false}]},
    "email":{"value":"webhook+test@tablexi.com","versions":[{"value":"webhook+test@tablexi.com","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408675504008,"selected":false}]},
    "zip":{"value":"11111","versions":[{"value":"60606","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408673641008,"selected":false}]},
    "preferred_industries":{"value":"Buildings & Facilities; Infrastructure","versions":[{"value":"Buildings & Facilities","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408673641008,"selected":false}]},
    "function":{"value":"Engineers","versions":[{"value":"Engineers","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408673641008,"selected":false}]},
    "referred_by_name_":{"value":"referral@referrals.com"},"resume":{"value":"https://s3.amazonaws.com/uploads/my resume with (spaces).rtf"}
  }
}]

EMPTY_CONTACT_PAYLOAD =
%Q[{"vid":20239,"canonical-vid":20239,"merged-vids":[],
  "properties":{
    "email":{"value":"webhook+test@tablexi.com","versions":[{"value":"webhook+test@tablexi.com","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408675504008,"selected":false}]}
  }
}]

MINIMAL_CONTACT_PAYLOAD =
%Q[{"vid":20239,"canonical-vid":20239,"merged-vids":[],
  "properties":{
    "firstname":{"value":"Webhook","versions":[{"value":"Webhook","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408673485008,"selected":false}]},
    "lastname":{"value":"Test","versions":[{"value":"Test","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408673485008,"selected":false}]},
    "email":{"value":"webhook+test@tablexi.com","versions":[{"value":"webhook+test@tablexi.com","source-type":"CONTACTS_WEB","source-id":"webhook@tablexi.com","source-label":null,"timestamp":1408675504008,"selected":false}]}
  }
}]
