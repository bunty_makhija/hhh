require 'spec_helper'

describe HubspotterInterface::ProfileTravelForm do
  let(:user) { create(:user) }
  let(:profile) { create(:professional_profile, user: user) }

  before do
    user.update_attribute :professional_profile, profile
    travel = profile.travel_preference
    travel.will_travel = true
    travel.home_frequency = "monthly"
    travel.will_relocate = false
    region_idx = 1
    10.times do
      travel.send("region#{region_idx}=", true)
      region_idx = region_idx + 1
    end
    travel.other_concerns = "Travel concerns"
    travel.save
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        email: user.email,
        will_travel: true,
        home_frequency: "monthly",
        will_relocate: false,
        regions: ["CT-ME-MA-NH-RI-VT",
          "NJ-NY",
          "DE-MD-PA-VA-WV-DC",
          "AL-FL-GA-KY-MS-NC-SC-TN",
          "IL-IN-MI-MN-OH-WI",
          "AR-LA-NM-OK-TX",
          "IA-KS-MO-NE",
          "CO-MT-ND-SD-UT-WY",
          "AZ-CA-HI-NV",
          "AK-ID-OR-WA"],
        travel_concerns: "Travel concerns"
      }
    end

    it 'builds expected form data' do
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end
end
