require 'spec_helper'

describe HubspotterInterface::ProfileWebhookForm do
  let(:user) { create(:user) }

  subject(:form) { described_class.new(user) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_created_at: user.created_at.strftime("%m/%d/%Y"),
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        email: user.email,
      }
    end

    it 'builds expected form data' do
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end
end
