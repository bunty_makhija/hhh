require 'spec_helper'

describe HubspotterInterface::ProfileEngagementForm do
  let(:user) { create(:user, phone_number: "111-222-3333") }
  let(:role_group) { create(:audience_type) }
  let(:starting_date) { Date.today + 1.day }
  let(:profile) do
    create(:professional_profile,
      user: user,
      zip_code: "11111",
      civil: true,
      country: "USA",
      state: "IL",
      city: "Chicago",
      industry_other: "Another industry",
      status: "passive")
  end

  before do
    user.update_attribute :professional_profile, profile
    profile.role_preference.desired_role = role_group
    profile.engagement_preference.starting_date = starting_date
    profile.engagement_preference.term_quick = true
    profile.engagement_preference.other_concerns = "Other concerns"
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        firstname: user.first_name,
        lastname: user.last_name,
        email: user.email,
        phone: "111-222-3333",
        zip: "11111",
        country: "USA",
        state: "IL",
        city: "Chicago",
        preferred_industries: ["Civil Services"],
        industry: "Another industry",
        professional_status: "passive",
        starting_date: starting_date.strftime("%m/%d/%Y"),
        engagement_term: ["Quick help (anywhere from 1 day to less than 1 month)"],
        availability_concerns: "Other concerns",
        function: role_group.name,
        resume: "https://resume_url"
      }
    end

    it 'builds expected form data' do
      expect(profile).to receive(:resume_url).and_return("https://resume_url")
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end

end
