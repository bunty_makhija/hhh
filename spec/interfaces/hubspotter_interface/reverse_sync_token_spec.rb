require 'spec_helper'

describe HubspotterInterface::ReverseSyncToken do
  it { should validate_presence_of :token_digest }
  it { expect(HubspotterInterface::ReverseSyncToken.retrieve_email("bad@email.com")).to be_nil }

  context "generate and retrieve token" do
    let(:token_message) { HubspotterInterface::ReverseSyncToken.generate('test@tablexi.com') }

    before do
      @email = HubspotterInterface::ReverseSyncToken.retrieve_email(token_message)
    end

    it { expect(@email).to eq 'test@tablexi.com' }

    context "after it has been retrieved once" do
      subject { HubspotterInterface::ReverseSyncToken.retrieve_email(token_message) }
      it { expect(subject).to be_nil }
    end
  end
end
