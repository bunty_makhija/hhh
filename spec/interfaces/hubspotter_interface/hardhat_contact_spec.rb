require 'spec_helper'

describe HubspotterInterface::HardhatContact do
  let(:user) { create(:user, {phone_number: "111-222-3333"}) }

  subject(:hardhat_contact) { HubspotterInterface::HardhatContact.new(user) }
  it { should delegate(:email).to :user }
  it { should delegate(:country).to :profile }
  it { should delegate(:state).to :profile }
  it { should delegate(:city).to :profile }
  it { should delegate(:authorization_details).to :profile }
  it { should delegate(:base_salary).to :salary_data }
  it { should delegate(:hourly_rate).to :salary_data }
  it { should delegate(:additional_skills).to :qualifications }

  it "hardhat_created_at date is in Hubspot-dictated data format" do
    expect(hardhat_contact.hardhat_created_at).to eq user.created_at.strftime("%m/%d/%Y")
  end

  it "hardhat_updated_at date is in Hubspot-dictated data format" do
    expect(hardhat_contact.hardhat_updated_at).to eq user.updated_at.strftime("%m/%d/%Y")
  end

  its(:category) { should eq nil }
  its(:category_function) { should eq [] }
  its(:project_type) { should eq [] }
  its(:sub_project_type) { should eq [] }
  its(:are_you_authorized_to_work_in_the_u_s_) { should eq "" }
  its(:other_metro_areas) { should eq "" }

  context "category and functions present" do
    let(:cat) { create :category, display_name: "Displayed Category"}
    let(:func1) { create :category_function, display_name: "Function 1", category: cat }
    let(:func2) { create :category_function, display_name: "Function 2", category: cat }

    before do
      user.professional_profile.update_attributes({ category: cat,
        category_functions: [func1, func2] })
    end

    subject { hardhat_contact }
    its(:category) { should eq "Displayed Category"}
    its(:category_function) { should eq ["Function 1", "Function 2"] }
  end

  context "project and sub project types" do
    let(:type) { create :project_type, display_name: "Displayed Project" }
    let(:subtype1) { create :sub_project_type, display_name: "Subtype 1", project_type: type }
    let(:subtype2) { create :sub_project_type, display_name: "Subtype 2", project_type: type }

    before do
      user.professional_profile.update_attributes({ project_types: [type],
        sub_project_types: [subtype1, subtype2] })
    end

    subject { hardhat_contact }
    its(:project_type) { should eq ["Displayed Project"] }
    its(:sub_project_type) { should eq ["Subtype 1", "Subtype 2"] }
  end

  context "when contact is authorized to work in the U.S." do
    before { user.professional_profile.update_attribute :authorization, true }
    subject { hardhat_contact }
    its(:are_you_authorized_to_work_in_the_u_s_) { should eq "Yes" }
  end

  context "when contact is authorized to work in the U.S." do
    before { user.professional_profile.update_attribute :authorization, false }
    subject { hardhat_contact }
    its(:are_you_authorized_to_work_in_the_u_s_) { should eq "No" }
  end

  context "total work experience" do
    before { user.professional_profile.update_attribute :experience, "Student" }
    subject { hardhat_contact }
    its(:total_work_experience) { should eq "Student" }
  end

  context "other metro areas" do
    let(:metro1) { create(:metro_area, area_name: "Greater metro1 area") }
    let(:metro2) { create(:metro_area, area_name: "Greater metro2 area") }
    before { user.professional_profile.update_attribute :metro_areas, [metro1, metro2] }
    subject { hardhat_contact }
    its(:other_metro_areas) { should eq "Greater metro1 area, Greater metro2 area" }
  end

  context "profile and engagement preferences" do
    let(:starting_date) { Date.today + 1.week }
    let(:role_group) { create(:audience_type) }

    before do
      profile = hardhat_contact.profile
      profile.status = "passive"
      profile.civil = true
      profile.infrastructure = true
      profile.zip_code = "60606"
      profile.industry_other = "Other Industry"
      profile.referrals = "referral@referrals.com"

      engagement = profile.engagement_preference
      engagement.other_concerns = "Engagement other concerns"
      engagement.starting_date = starting_date
      engagement.term_quick = true
      engagement.term_various = true

      profile.role_preference.desired_role = role_group
    end

    its(:firstname) { should eq user.first_name }
    its(:lastname) { should eq user.last_name }
    its(:professional_status) { should eq "passive" }
    its(:preferred_industries) { should eq ["Civil Services", "Infrastructure"] }
    its(:zip) { should eq "60606" }
    its(:industry) { should eq "Other Industry" }
    its(:starting_date) { should eq starting_date.strftime("%m/%d/%Y") }
    its(:engagement_term) { should eq ["Quick help (anywhere from 1 day to less than 1 month)", "I'm a consultant looking for various engagements"] }
    its(:availability_concerns) { should eq "Engagement other concerns" }
    its(:function) { should eq role_group.name }
    its(:phone) { should eq "111-222-3333" }
    its(:referred_by_name_) { should eq "referral@referrals.com" }

    context "resume" do
      before do
        expect(hardhat_contact.profile).to receive(:resume_url).and_return("https://resume_url")
      end

      its(:resume) { should eq "https://resume_url" }
    end
  end

  context "travel preferences" do
    before do
      travel = hardhat_contact.profile.travel_preference
      travel.will_travel = true
      travel.home_frequency = "daily"
      travel.will_relocate = false
      travel.region1 = true
      travel.region5 = true
      travel.other_concerns = "Other travel concerns"
    end

    its(:will_travel) { should be_true }
    its(:will_relocate) { should be_false }
    its(:home_frequency) { should eq "daily" }
    its(:regions) { should eq ["CT-ME-MA-NH-RI-VT", "IL-IN-MI-MN-OH-WI"] }
    its(:travel_concerns) { should eq "Other travel concerns" }
  end

  context "schedule preferences" do
    before do
      schedule = hardhat_contact.profile.schedule_preference
      schedule.contract = true
      schedule.full_time = true
      schedule.other_concerns = "Other schedule concerns"
    end

    its(:work_type) { should eq ["Full-time", "Hourly/Contract"] }
    its(:schedule_concerns) { should eq "Other schedule concerns" }
  end

  context "salary preferences" do
    before do
      salary = hardhat_contact.salary_data
      salary.base_salary = "55,000"
      salary.hourly_rate = "90/hr"
      salary.daily_travel_comp = "$30/day"
      salary.other_concerns = "Other salary concerns"
    end

    its(:base_salary) { should eq "55,000" }
    its(:hourly_rate) { should eq "90/hr" }
    its(:travel_per_diem) { should eq "$30/day" }
    its(:salary_concerns) { should eq "Other salary concerns" }
  end

  context "qualifications" do
    before do
      quals = hardhat_contact.qualifications
      quals.skill_list.add("Skill1")
      quals.skill_list.add("Skill2")
      quals.additional_skills = "Additional skills"
      quals.cert_list.add("Cert1")
      quals.cert_list.add("Cert2")
      quals.additional_certs = "Additional certs"
      quals.save
    end

    its(:skills) { should eq "Skill1, Skill2" }
    its(:additional_skills) { should eq "Additional skills" }
    its(:certifications) { should eq "Cert1, Cert2" }
    its(:additional_certifications) { should eq "Additional certs" }
  end

  context "education history" do
    let(:grad_date_1) { (Date.today - 2.years).strftime('%m/%Y') }
    let(:grad_date_2) { (Date.today - 1.years).strftime('%m/%Y') }

    let(:edu1) { create(:education_history,
      graduation_date: Date.today - 2.years,
      institution: "University of Chicago",
      achievement_level: "bachelors") }
    let(:edu2) { create(:education_history,
      graduation_date: Date.today - 1.year,
      institution: "UIUC",
      achievement_level: "masters") }

    before do
      edu1.concentration_list = ["C1", "C2"]
      edu2.concentration_list = ["C3", "C4"]
      hardhat_contact.profile.education_histories = [edu1, edu2]
      hardhat_contact.profile.save
    end
    its(:education_history) { should eq "#{grad_date_2}\nUIUC\nmasters\nC3, C4\n====\n#{grad_date_1}\nUniversity of Chicago\nbachelors\nC1, C2\n====" }
  end

  context "work history" do
    let(:work_from_1) { (Date.today - 4.years).strftime('%m/%Y') }
    let(:work_to_1)   { (Date.today - 2.years).strftime('%m/%Y') }

    let(:work_from_2) { (Date.today - 2.years).strftime('%m/%Y') }
    let(:work_to_2)   { "present" }

    let(:work1) { create(:work_history,
      company: "Orbitz",
      industry: "civil",
      from_date: Date.today - 4.years,
      to_date: Date.today - 2.years,
      position: "Civil Engineer Level1",
      summary: "I worked here")}
    let(:work2) { create(:work_history,
      company: "TableXI",
      industry: "civil",
      from_date: Date.today - 2.year,
      to_date: nil,
      current: true,
      position: "Civil Engineer Level1",
      summary: "I worked here next")}

    before do
      hardhat_contact.profile.work_histories = [work1, work2]
      hardhat_contact.profile.save
    end

    its(:work_history) { should eq "#{work_from_2} - #{work_to_2}\nTableXI\nCivil Services\nCivil Engineer Level1\nI worked here next\n====\n#{work_from_1} - #{work_to_1}\nOrbitz\nCivil Services\nCivil Engineer Level1\nI worked here\n====" }
  end
end
