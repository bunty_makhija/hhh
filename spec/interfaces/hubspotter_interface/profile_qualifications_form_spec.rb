require 'spec_helper'

describe HubspotterInterface::ProfileQualificationsForm do
  let(:user) { create(:user) }
  let(:profile) { create(:professional_profile, user: user) }

  before do
    user.update_attribute :professional_profile, profile
    quals = profile.qualification
    quals.skill_list.add("Skill1")
    quals.skill_list.add("Skill2")
    quals.additional_skills = "Additional skills"
    quals.cert_list.add("Cert1")
    quals.cert_list.add("Cert2")
    quals.additional_certs = "Additional certs"
    quals.save
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        email: user.email,
        skills: "Skill1, Skill2",
        additional_skills: "Additional skills",
        certifications: "Cert1, Cert2",
        additional_certifications: "Additional certs"
      }
    end

    it 'builds expected form data' do
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end
end
