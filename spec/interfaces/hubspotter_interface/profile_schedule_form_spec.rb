require 'spec_helper'

describe HubspotterInterface::ProfileScheduleForm do
  let(:user) { create(:user) }
  let(:profile) { create(:professional_profile, user: user) }

  before do
    user.update_attribute :professional_profile, profile
    schedule = profile.schedule_preference
    schedule.full_time = true
    schedule.part_time = true
    schedule.other_concerns = "Schedule concerns"
    schedule.save
  end

  subject(:form) { described_class.new(user, nil, nil) }

  describe '#submit' do
    let(:expected_data) do
      {
        hardhat_updated_at: user.updated_at.strftime("%m/%d/%Y"),
        email: user.email,
        work_type: ["Full-time", "Part-time"],
        schedule_concerns: "Schedule concerns"
      }
    end

    it 'builds expected form data' do
      data = form.form_data
      expect(data).to eq expected_data
    end

    it 'submits successfully' do
      expect { form.submit }.not_to raise_error
    end
  end
end
