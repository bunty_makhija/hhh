require "spec_helper"

describe GetStarted::IndustryStep do
  let(:professional_profile) { create :professional_profile }
  let(:params) { ActionController::Parameters.new(get_started_industry_step: form_params) }
  let(:form_params) { { } }
  subject(:industry_step) { described_class.new(professional_profile.user, params) }

  it { should delegate(:civil).to :professional_profile }
  it { should delegate(:energy).to :professional_profile }
  it { should delegate(:building).to :professional_profile }
  it { should delegate(:petroleum).to :professional_profile }
  it { should delegate(:infrastructure).to :professional_profile }

  describe "#valid?" do
    before(:each) { industry_step.save }

    context "industry is present" do
      let(:form_params) { { civil: true } }
      it { should be_valid }
    end

    context "industry is missing" do
      let(:form_params) { { civil: false, energy: false, building: false, petroleum: false, infrastructure: false } }
      it { should_not be_valid }
    end
  end

  describe "#save" do
    context "with expected params" do
      let(:form_params) { { civil: true, petroleum: true } }

      it "should update the user's associated category types" do
        industry_step.save
        expect(professional_profile.civil).to eq form_params[:civil]
        expect(professional_profile.petroleum).to eq form_params[:petroleum]
      end
    end

    context "with incorrectly specified params" do
      let(:form_params) { { civil: "Kyogre" } }

      it "should not update the user's civil flag" do
        expect { industry_step.save }.not_to change { professional_profile.reload.civil }
      end
    end
  end
end
