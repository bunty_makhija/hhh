require 'spec_helper'

describe GetStarted::SetPasswordStep do
  let(:user) { create :user }
  let(:params) { ActionController::Parameters.new(get_started_set_password_step: form_params) }
  let(:form_params) { {} }

  subject(:password_form) { described_class.new(user, params) }
  it { should delegate(:password).to :user }
  it { should delegate(:password_confirmation).to :user }

  describe "successful save" do
    let(:form_params) { {password: 'good-password', password_confirmation: 'good-password'} }
    before do
      @old_password = user.encrypted_password
      @saved = password_form.save
      user.reload
    end
    it { expect(@saved).to be_true }
    it { expect(user.encrypted_password).not_to eq @old_password }
    it { expect(password_form).to be_valid }
  end

  describe "if passwords don't match" do
    let(:form_params) { {password: 'my_password', password_confirmation: 'not_my_password'} }
    before do
      @old_password = user.encrypted_password
      @saved = password_form.save
      user.reload
    end
    it { expect(@saved).to be_false }
    it { expect(user.encrypted_password).to eq @old_password }
    it { expect(password_form).not_to be_valid }
    it { expect(password_form.errors[:password_confirmation]).to eq ["doesn't match Password"]}
  end

  describe "if no values are given" do
    let(:form_params) { {password: '', password_confirmation: ''} }
    before do
      @old_password = user.encrypted_password
      @saved = password_form.save
      user.reload
    end
    it { expect(@saved).to be_true }
    it { expect(user.encrypted_password).to eq @old_password }
    it { expect(password_form).to be_valid }
    it { expect(password_form.errors.full_messages).not_to include "Password can't be blank" }
  end
end
