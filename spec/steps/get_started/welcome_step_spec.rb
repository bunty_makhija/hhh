require "spec_helper"

describe GetStarted::WelcomeStep do
  let(:professional_profile) { create :professional_profile }
  let(:category) { create :category }
  let(:params) { ActionController::Parameters.new(get_started_welcome_step: form_params) }
  let(:form_params) { { } }
  subject(:welcome_step) { described_class.new(professional_profile.user, params) }

  let!(:facilities_category) { FactoryGirl.create(:category, key: 'facilities_management') }
  let!(:facilities_project_type) { FactoryGirl.create(:project_type, key: 'buildings_facilities') }

  it { should delegate(:category_id).to :professional_profile }

  describe "#valid?" do
    before(:each) { welcome_step.save }

    context "category_id is present" do
      let(:form_params) { { category_id: category.id } }
      it { should be_valid }
    end

    context "category_id is missing" do
      let(:form_params) { { category_id: nil } }
      it { should_not be_valid }
    end
  end

  describe "#save" do
    context "with expected params" do
      let(:form_params) { { category_id: category.id } }

      it "should update the user's associated category types" do
        welcome_step.save
        expect(professional_profile.reload.category_id).to eq(form_params[:category_id])
      end
    end

    context "with incorrectly specified params" do
      let(:form_params) { { category_id: "STENDEC" } }

      it "should not update the user's associated category" do
        expect { welcome_step.save }.not_to change { professional_profile.reload.category }
      end
    end
  end
end
