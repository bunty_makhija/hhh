require 'spec_helper'

describe GetStarted::NextStepsStep do
  let (:user) { create :user }
  let (:step) { described_class.new(user) }

  it "returns the last industry selected" do
    user.professional_profile.update(civil: true)
    user.professional_profile.update(petroleum: true)

    expect(step.preferred_industry).to eq(:petroleum)
  end

  describe "#preferred industry" do
    context "when professional_profile has civil selected" do
      it "should return civil" do
        user.professional_profile.update(civil: true)

        expect(step.preferred_industry).to eq(:civil)
      end
    end

    context "when professional_profile has infrastructure selected" do
      it "should return infrastructure" do
        user.professional_profile.update(infrastructure: true)

        expect(step.preferred_industry).to eq(:infrastructure)
      end
    end

    context "when professional_profile has petroleum selected" do
      it "should return petroleum" do
        user.professional_profile.update(petroleum: true)

        expect(step.preferred_industry).to eq(:petroleum)
      end
    end

    context "when professional_profile has building selected" do
      it "should return building" do
        user.professional_profile.update(building: true)

        expect(step.preferred_industry).to eq(:building)
      end
    end

    context "when professional_profile has energy selected" do
      it "should return energy" do
        user.professional_profile.update(energy: true)

        expect(step.preferred_industry).to eq(:energy)
      end
    end
  end
end
