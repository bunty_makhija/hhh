require "spec_helper"

describe GetStarted::SpecialtiesStep do
  let(:professional_profile) { create :professional_profile }
  let(:sub_project_type) { create :sub_project_type }
  let(:params) { ActionController::Parameters.new(get_started_specialties_step: form_params) }
  let(:form_params) { { } }
  subject(:specialties_step) { described_class.new(professional_profile.user, params) }

  it { should delegate(:sub_project_types).to :professional_profile }
  it { should delegate(:sub_project_type_ids).to :professional_profile }

  describe "#valid?" do
    before(:each) { specialties_step.save }

    context "sub_project_type_ids is present" do
      let(:form_params) { { sub_project_type_ids: [sub_project_type.id] } }
      it { should be_valid }
    end

    context "sub_project_type_ids is missing" do
      let(:form_params) { { sub_project_type_ids: nil } }
      it { should_not be_valid }
    end
  end

  describe "#save" do
    context "with expected params" do
      let(:form_params) { { sub_project_type_ids: [sub_project_type.id] } }

      it "should update the user's associated sub-project types" do
        specialties_step.save
        expect(professional_profile.reload.sub_project_types).to eq([sub_project_type])
      end
    end

    context "with incorrectly specified params" do
      let(:form_params) { { sub_project_type_ids: "STENDEC" } }

      it "should not update the user's associated sub-project types" do
        expect { specialties_step.save }.not_to change { professional_profile.reload.sub_project_types }
      end
    end
  end
end
