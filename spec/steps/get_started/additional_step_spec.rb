require 'spec_helper'

describe GetStarted::AdditionalStep do
  let(:professional_profile) { create :professional_profile }
  let(:metro_area) { create(:metro_area) }
  let(:params) { ActionController::Parameters.new(get_started_additional_step: form_params) }
  let(:form_params) { { experience: nil, metro_area_ids: nil, authorization: nil, authorization_details: nil } }
  subject(:additional_step) { described_class.new(professional_profile.user, params) }

  it { should delegate(:experience).to :professional_profile }
  it { should delegate(:authorization).to :professional_profile }

  describe "#save" do
    describe "success" do
      let(:experience_value) { ProfessionalProfile.experience.values.sample }
      let (:form_params) { { experience: experience_value, metro_area_ids: [metro_area.id], authorization: true } }

      it "should update the professional profile experience and authorization" do
        additional_step.save
        expect(professional_profile.experience).to eq(experience_value)
        expect(professional_profile.authorization).to eq(true)
        expect(professional_profile.metro_areas).to match_array([metro_area])
      end

      it "should update the professional profile with authorization details" do
        form_params[:authorization] = false
        form_params[:authorization_details] = "Applying for my visa."

        additional_step.save

        expect(professional_profile.authorization_details).to eq("Applying for my visa.")
      end

      it "should update the professional profile experience and authorization while ignoring other params" do
        params[:city] = "Not a city"
        additional_step.save
        expect(professional_profile.city).not_to eq(params[:city])
      end
    end

    describe "failure" do
      before do
        additional_step.save
      end

      it "should not update without experience" do
        expect(professional_profile.experience).to eq(nil)
      end

      it "should not update without authorization" do
        expect(professional_profile.authorization).to eq(nil)
      end

      it "should have errors added to it" do
        expect(additional_step.errors).to include(:experience)
        expect(additional_step.errors).to include(:authorization)
      end
    end
  end

end
