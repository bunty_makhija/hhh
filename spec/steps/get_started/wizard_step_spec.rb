require "spec_helper"

class TestWizardStep < GetStarted::WizardStep
  validates :summary, presence: true

  delegate :summary, to: :professional_profile

  private

  def apply_attributes
    professional_profile.update_attributes(safe_params)
  end

  def safe_params
    # For the sake of this test, allow updating of anything on the profile
    profile_params = @params[:get_started_wizard_step]
    profile_params.permit(ProfessionalProfile.attribute_names)
  end
end


describe GetStarted::WizardStep do
  let(:professional_profile) { create :professional_profile }
  let(:params) { ActionController::Parameters.new(get_started_wizard_step: form_params) }
  let(:form_params) { { } }
  subject(:wizard_step) { TestWizardStep.new(professional_profile.user, params) }

  describe "#save" do
    context "with expected params" do
      let(:form_params) { { city: Faker::Address.city, summary: Faker::Lorem.paragraph } }

      it "should update the user's profile and send the hubspot form" do
        expect_any_instance_of(HubspotterInterface::WizardForm).to receive(:submit)
        wizard_step.save
        expect(professional_profile.city).to eq form_params[:city]
        expect(professional_profile.summary).to eq form_params[:summary]
      end
    end

    context "with unexpected params" do
      let(:form_params) { { gym_leader: Faker::Name.name } }

      it "should not update the user's profile or the hubspot form" do
        expect_any_instance_of(HubspotterInterface::WizardForm).not_to receive(:submit)
        expect { wizard_step.save }.not_to change { professional_profile.reload }
      end
    end

    context "with incorrectly specified params" do
      let(:form_params) { { summary: 1e-10, category_id: -1 } }

      it "should not update the user's profile" do
        expect { wizard_step.save }.not_to change { professional_profile.reload }
      end
    end

    describe "transaction behavior" do
      before { wizard_step.stub(:apply_attributes) { FactoryGirl.create(:category) } }

      it "persists changes if validation succeeds" do
        expect_any_instance_of(HubspotterInterface::WizardForm).to receive(:submit)
        expect(wizard_step).to receive(:valid?).and_return(true)

        expect { wizard_step.save }.to change { Category.count }.by(1)
      end

      it "rolls back changes if validation fails" do
        expect_any_instance_of(HubspotterInterface::WizardForm).not_to receive(:submit)
        expect(wizard_step).to receive(:valid?).and_return(false)

        expect { wizard_step.save }.not_to change { Category.count }
      end
    end
  end

  describe "#slug" do
    it 'asdf' do
      allow(TestWizardStep).to receive(:name).and_return('GetStarted::SlugNameStep')
      expect(wizard_step.slug).to eq :slug_name
    end
  end
end
