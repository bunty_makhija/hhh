require "spec_helper"

describe GetStarted::FunctionStep do
  let(:professional_profile) { create :professional_profile }
  let(:category_function) { create :category_function }
  let(:params) { ActionController::Parameters.new(get_started_function_step: form_params) }
  let(:form_params) { { } }
  subject(:function_step) { described_class.new(professional_profile.user, params) }

  it { should delegate(:category_functions).to :professional_profile }
  it { should delegate(:category_function_ids).to :professional_profile }

  describe "#valid?" do
    before(:each) { function_step.save }

    context "category_functions are present" do
      let(:form_params) { { category_function_ids: [category_function.id] } }
      it { should be_valid }
    end

    context "category_functions are missing" do
      let(:form_params) { { category_function_ids: [] } }
      it { should_not be_valid }
    end
  end

  describe "#save" do
    context "with expected params" do
      let(:form_params) { { category_function_ids: [category_function.id] } }

      it "should update the user's associated category functions" do
        function_step.save
        expect(professional_profile.reload.category_functions).to eq([category_function])
      end
    end

    context "with incorrectly specified params" do
      let(:form_params) { { category_function_ids: [] } }

      it "should not update the user's associated category functions" do
        expect { function_step.save }.not_to change { professional_profile.reload.category_functions }
      end
    end
  end
end
