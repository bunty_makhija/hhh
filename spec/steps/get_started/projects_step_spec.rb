require "spec_helper"

describe GetStarted::ProjectsStep do
  let(:professional_profile) { create :professional_profile }
  let(:project_type) { create :project_type }
  let(:params) { ActionController::Parameters.new(get_started_projects_step: form_params) }
  let(:form_params) { { project_type_ids: [] } }
  subject(:projects_step) { described_class.new(professional_profile.user, params) }

  it { should delegate(:project_types).to :professional_profile }
  it { should delegate(:project_type_ids).to :professional_profile }

  describe "#valid?" do
    before(:each) { projects_step.save }

    context "project_type_ids is present" do
      let(:form_params) { { project_type_ids: [project_type.id] } }
      it { should be_valid }
    end

    context "project_type_ids is missing" do
      let(:form_params) { { project_type_ids: nil } }
      it { should_not be_valid }
    end
  end

  describe "#save" do
    context "with expected params" do
      let(:form_params) { { project_type_ids: [project_type.id] } }

      it "should update the user's associated project types" do
        projects_step.save
        expect(professional_profile.reload.project_types).to eq([project_type])
      end
    end

    context "with incorrectly specified params" do
      let(:form_params) { { project_type_ids: "DEADBEEF" } }

      it "should not update the user's associated project types" do
        expect { projects_step.save }.not_to change { professional_profile.reload.project_types }
      end
    end
  end
end
