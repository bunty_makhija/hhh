require 'spec_helper'

describe "Flash Messages", type: :request do
  it 'finds all the whitelisted flash messages' do
    flash_keys = %i(alert error notice success)
    flash_hash = flash_keys.each_with_object({}) { |key, hash| hash[key] = key.to_s.capitalize }

    expect_any_instance_of(ActionDispatch::Flash::FlashHash).to receive(:to_hash).and_return(flash_hash)

    get "/"

    expect(response.body).to have_content("Alert")
    expect(response.body).to have_content("Notice")
    expect(response.body).to have_content("Error")
    expect(response.body).to have_content("Success")
  end
end
