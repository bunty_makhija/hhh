require 'spec_helper'

feature "Create company profile" do
  let (:admin) { create :admin, password: "password" }
  scenario "with minimum information" do
    visit new_user_session_path
    fill_in "Email", with: admin.email
    fill_in "Password", with: admin.password
    click_button "Login"
    click_link "Company Profiles"
    click_link "New Company Profile"
    fill_in "Company name", with: "Exelon"
    fill_in "Email", with: "exelon@example.com"
    click_button "Create Company profile"

    expect(page).to have_content("Company profile was successfully created.")
  end

  scenario "with an image" do
    visit new_user_session_path
    fill_in "Email", with: admin.email
    fill_in "Password", with: "password"
    click_button "Login"
    click_link "Company Profiles"
    click_link "New Company Profile"
    fill_in "Company name", with: "Exelon"
    fill_in "Email", with: "exelon@example.com" 
    find("input[type='file']").set("spec/factories/image.png")
    click_button "Create Company profile"

    expect(page).to have_content("Company profile was successfully created.")
    expect(CompanyProfile.find_by(email: "exelon@example.com").logo.to_s).to eq("/uploads/image.png")
  end
end
