require 'spec_helper'

describe "linkedin data sync with existing account" do
  before do
    OmniAuth.config.test_mode = true
    OmniAuth.config.add_mock(
      :linkedin,
      provider: "linkedin",
      extra: {
        raw_info: { positions:  { values: [{company: { name: "Table XI"} }] },
                    educations: { values:
                      [{ schoolName: "School of Hard Knocks", fieldOfStudy: "Gangsta" }] },
                    skills:     { values: [{skill: { name: "Rapping"} }] }
        }
      })
    visit(new_user_session_path)
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: user.password
    find(:css, ".simple_form.new_user .button.button--large").click
  end

  after do
    OmniAuth.config.test_mode = false
    OmniAuth.config.mock_auth[:linkedin] = nil
  end

  describe "importing data", type: :feature do
    let(:user) { create :user }

    it "successfully imports everything" do
      user.professional_profile.update(wizard_completed_at: 5.minutes.ago)
      visit(edit_professional_profile_path(user))
      expect(user.professional_profile.work_histories.length).to eq(0)
      expect(user.professional_profile.education_histories.length).to eq(0)
      expect(user.professional_profile.qualification.additional_skills).to be_nil
      click_link("Connect with LinkedIn")

      expect(user.professional_profile.reload.work_histories.first).to be_persisted
      expect(user.professional_profile.reload.education_histories.first).to be_persisted
      expect(user.professional_profile.reload.qualification.additional_skills).to eq("Rapping")
    end

    it "successfully imports work history" do
      visit(professional_profile_work_histories_path(user))
      expect(user.professional_profile.work_histories.length).to eq(0)
      click_link("Import From LinkedIn")

      expect(user.professional_profile.reload.work_histories.first).to be_persisted
      expect(user.professional_profile.work_histories.length).to eq(1)
    end

    it "successfully creates education history" do
      visit(professional_profile_education_histories_path(user))
      expect(user.professional_profile.education_histories.length).to eq(0)
      click_link("Import From LinkedIn")

      expect(user.professional_profile.reload.education_histories.first).to be_persisted
      expect(user.professional_profile.education_histories.length).to eq(1)
    end

    it "successfully creates skills" do
      visit(edit_professional_profile_qualification_path(user, user.professional_profile.qualification))
      expect(user.professional_profile.qualification.additional_skills).to be_nil
      click_link("Import From LinkedIn")

      expect(user.professional_profile.reload.qualification.additional_skills).to eq("Rapping")
      expect(user.professional_profile.qualification.additional_skills.length).to be > 1
    end
  end
end
