require 'spec_helper'

describe "wizard signup process" do

  def create_new_user
    visit(new_user_registration_path)
    form_name = "user_registration_form"
    fill_in "First name",                          with: "John"
    fill_in "Last name",                           with: "Jameson"
    fill_in "Email",                               with: "john@example.com"
    fill_in "Zip code",                            with: "60610"
    select('USA', from: 'Country')
    check "Civil"
    click_button "Get Started"
  end

  describe "a first timer goes down...", type: :feature do
    let!(:category)          { create :category }
    let!(:category_function) { create :category_function, category: category }
    let!(:project_type)      { create :project_type }
    let!(:sub_project_type)  { create :sub_project_type, project_type: project_type }

    it "Gandalf the Grey's happy path" do
      expect{create_new_user}.to change{User.count}.by(1)
      user = User.last

      # Welcome Step
      expect(current_path).to eq(get_started_path(:welcome))
      page.choose(category.display_name)
      find(".button--wizardNext").click
      expect(user.reload.professional_profile.category).to eq(category)

      # Function Step
      expect(current_path).to eq(get_started_path(:function))
      page.check(category_function.display_name)
      find(".button--wizardNext").click
      expect(user.reload.professional_profile.category.category_functions).to include(category_function)

      # Projects Step
      expect(current_path).to eq(get_started_path(:projects))
      page.check(project_type.display_name)
      find(".button--wizardNext").click
      expect(user.reload.professional_profile.project_types).to include(project_type)

      # Specialties
      expect(current_path).to eq(get_started_path(:specialties))
      page.check(sub_project_type.display_name)
      find(".button--wizardNext").click
      expect(user.reload.professional_profile.sub_project_types).to include(sub_project_type)

      # Additional
      expect(current_path).to eq(get_started_path(:additional))
      select("Student")
      page.choose("Yes")
      find(".button--wizardNext").click
      expect(user.reload.professional_profile.experience).to eq("Student")
      expect(user.reload.professional_profile.authorization).to eq(true)

      # Last Thing
      expect(current_path).to eq(get_started_path(:last_thing))
      click_button "Next"

      # Set Password
      expect(current_path).to eq(get_started_path(:set_password))
      form_name = "get_started_set_password_step"
      fill_in "#{form_name}[password]",              with: "password"
      fill_in "#{form_name}[password_confirmation]", with: "password"
      click_button "Next"

      # Drink Beer
      expect(current_path).to eq(get_started_path(:next_steps))
      expect(page).to have_content(text("views.get_started.next_steps.heading"))
    end
  end
end
