require 'spec_helper'

feature "Landing Page Test" do
  scenario "User Visits Project Manager Audience Page" do
    audience_type = FactoryGirl.create(:audience_type, name: "Project Managers & Superintendents", published: true)
    visit audience_type_path(audience_type)

    expect(page).to have_field(:user_registration_form_first_name)
    expect(page).to have_text("COMMON JOBS")

    expect(page).to_not have_link('Create your CONFIDENTIAL profile', href: new_user_registration_path)
  end

  scenario "User Visits Facility Manager Audience Page" do
    audience_type = FactoryGirl.create(:audience_type, name: "Facility Managers & Building Engineers", published: true)
    visit audience_type_path(audience_type)

    expect(page).to have_field(:user_registration_form_first_name)
    expect(page).to have_text("COMMONLY USED JOB TITLES")

    expect(page).to_not have_link('Create your CONFIDENTIAL profile', href: new_user_registration_path)
  end

  scenario "User Visits Non Landing Page Audience Page" do
    audience_type = FactoryGirl.create(:audience_type, published: true)
    visit audience_type_path(audience_type)

    expect(page).to have_link('Create your CONFIDENTIAL profile', href: new_user_registration_path)

    expect(page).to_not have_field(:user_registration_form_first_name)
  end
end
