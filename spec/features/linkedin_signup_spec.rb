require 'spec_helper'

describe "create account with linkedin" do

  def sign_in_user(user)
    visit(new_user_session_path)
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: user.password
    find(:css, ".simple_form.new_user .button.button--large").click
  end

  def sign_out_user
    browser = Capybara.current_session.driver.browser
    browser.clear_cookies
  end

  before do
    OmniAuth.config.test_mode = true
    OmniAuth.config.add_mock(
      :linkedin,
      provider: "linkedin",
      uid: "abc123",
      extra: {
        raw_info: {
          emailAddress: "john.jameson@example.com",
          firstName:    "John",
          lastName:     "Jameson",
          positions:  { values: [{company: { name: "Table XI"} }] },
          educations: { values:
            [{ schoolName: "School of Hard Knocks", fieldOfStudy: "Gangsta" }] },
          skills:     { values: [{skill: { name: "Rapping"} }] }
        }
      })
  end

  after do
    OmniAuth.config.test_mode = false
    OmniAuth.config.mock_auth[:linkedin] = nil
  end

  describe "first time signup", type: :feature do
    it "successfully creates a new user" do
      visit(new_user_registration_path)
      click_link("Sign Up With LinkedIn")
      user = User.last

      expect(user).to be_persisted
      expect(user.professional_profile).to be_persisted
      expect(user.provider).to eq("linkedin")
      expect(user.professional_profile.work_histories.length).to eq(1)
      expect(user.professional_profile.education_histories.length).to eq(1)
      expect(current_path).to eq(get_started_path(:welcome)) #Re-directs to Wizard
    end
  end

  describe "syncing account" do
    let(:user_1) { create :user}
    let(:user_2) { create :user}

    it "prevents two users having the same linkedin account" do
      user_1.professional_profile.update(wizard_completed_at: 5.minutes.ago)
      sign_in_user(user_1)
      visit(edit_professional_profile_path(user_1))
      expect(user_1.professional_profile.work_histories.length).to eq(0)
      click_link("Connect with LinkedIn")

      expect(user_1.professional_profile.reload.work_histories.first).to be_persisted
      expect(user_1.reload.uid).to be_present
      sign_out_user

      #User 2
      user_2.professional_profile.update(wizard_completed_at: 5.minutes.ago)
      sign_in_user(user_2)
      visit(edit_professional_profile_path(user_2))
      expect(user_2.professional_profile.work_histories.length).to eq(0)

      click_link("Connect with LinkedIn")
      expect(user_2.reload.uid).to be_nil
    end
  end

end
