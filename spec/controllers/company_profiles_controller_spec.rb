require 'spec_helper'

describe CompanyProfilesController do

  let(:profile) { CompanyProfile.first }

  before do
    2.times {|i| create :company_profile, featured: i % 2 == 0}
  end

  it 'loads the company profile and renders the template' do
    get :show, id: profile.slug
    expect(assigns[:company_profile]).to be_present
    expect(response).to render_template :show
  end

  it 'redirects to slug URL when old id URL is used' do
    get :show, id: profile.id
    expect(response).to redirect_to company_profile_path(profile.slug)
  end

  it 'shows the index page' do
    get :index
    expect(response).to render_template :index
  end

end
