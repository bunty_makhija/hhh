require "spec_helper"

describe ConfirmationsController do

  include_context "not logged in as user"

  describe "show" do
    include_context "with clear deliveries array"

    let!(:user) { create :unconfirmed_user }

    context "for an existing confirmation_token" do
      it "sends a welcome email" do
        expect(Devise.token_generator).to receive(:digest).and_return(user.confirmation_token)
        get :show, confirmation_token: "stubbed_token_param"
        expect(deliveries.length).to eq 1
        expect(deliveries.last.to).to eq [user.email]
      end
    end

    context "for a non-existent confirmation_token" do
      it "does not send any emails" do
        get :show, confirmation_token: Faker::Lorem.word
        expect(deliveries).to be_empty
      end
    end

  end

end
