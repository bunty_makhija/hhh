require 'spec_helper'

describe QualificationsController do
  let(:user) { create :user }
  let(:qualification) { user.professional_profile.qualification }
  let(:params) do
    {
      id: qualification.id,
      professional_profile_id: user.professional_profile.id,
      qualification: {}
    }
  end


  include_context "tags", 10
  include_context "logged in as user", :user
  include_examples "resource actions", :qualification, %i(edit)
  include_examples "professional profile component controller"


  it 'adds the given skills to the qualification' do
    params[:qualification][:skill_list] = ActsAsTaggableOn::Tag.skills.limit(5).pluck(:name)
    expect(qualification.skill_list).to be_empty
    patch :update, params
    expect(qualification.reload.skill_list).to eq params[:qualification][:skill_list]
  end

  it 'adds the given certs to the qualification' do
    params[:qualification][:cert_list] = ActsAsTaggableOn::Tag.certs.limit(5).pluck(:name)
    expect(qualification.cert_list).to be_empty
    patch :update, params
    expect(qualification.reload.cert_list).to eq params[:qualification][:cert_list]
  end
end
