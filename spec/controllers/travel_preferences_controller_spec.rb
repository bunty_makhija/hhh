require "spec_helper"

describe TravelPreferencesController do
  let(:user) { create :user }
  let(:travel_preference) { user.professional_profile.travel_preference }

  include_context "logged in as user", :user

  include_examples "resource actions", :travel_preference, %i(edit)
  include_examples "professional profile component controller"

  describe "update" do
    it "updates the resource" do
      opp_val = !travel_preference.will_travel
      get :update,
        id: travel_preference.id,
        professional_profile_id: user.professional_profile.id,
        travel_preference: {will_travel: opp_val}
      expect(travel_preference.reload.will_travel).to eq opp_val
    end
  end
end
