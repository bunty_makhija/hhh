require 'spec_helper'

describe RegistrationsController do
  before do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  context "new registrations redirect to the wizard" do
    before do
      post :create, {
        user_registration_form: {
          first_name: "First",
          last_name: "Last",
          email: "email@hardhathub.com",
          zip_code: "60606",
          country: "USA",
          civil: true } }
    end

    it { expect(response).to redirect_to get_started_index_path }
  end

  context "update user without a current user" do
    before do
      patch :update, {user: {email: "nobody@example.com"}}
    end

    it { expect(response).to redirect_to new_user_session_path }
  end

  context "successfully update linkedin user email without a password" do
    let(:linkedin_user) { create(:user, uid: "12345") }

    before do
      sign_in linkedin_user
      patch :update, {user: { email: "me@example.com" }}
      linkedin_user.reload
    end

    it { expect(linkedin_user.unconfirmed_email).to eq("me@example.com") }
    it { expect(response).to redirect_to edit_user_registration_path }
    it { expect(flash[:notice]).to eq "Confirmation instructions have been sent to your updated email address. You need to confirm before we complete the change." }
  end

  context "non linkedin user requires a password to change email" do
    let(:user) { create(:user, uid: nil) }

    before do
      sign_in user
      patch :update, {user: { email: "me@example.com" }}
      user.reload
    end

    it { expect(user.unconfirmed_email).to eq(nil) }
    it { expect(response).to render_template :edit }
  end

  context "non linkedin user with password can change email" do
    let(:user) { create(:user, uid: nil, password: "password") }

    before do
      sign_in user
      patch :update, {user: { email: "me@example.com", current_password: "password" }}
      user.reload
    end

    it { expect(user.unconfirmed_email).to eq("me@example.com") }
    it { expect(response).to render_template "devise/mailer/confirmation_instructions" }
  end
end
