require 'spec_helper'

describe ApplicationController do

  let(:redirect) do
    old_path = Faker::Internet.url.gsub /https:\/\/.+\//, '/'
    new_url = Faker::Internet.url
    Redirect.create! old_path: old_path, new_url: new_url
  end

  before do
    Rails.application.routes.draw do
      get '/handle_unknown_url' => 'application#handle_unknown_url'
    end
  end
  #
  # Temporarily create a route to ApplicationController#handle_unknown_url so we can test
  # https://github.com/rspec/rspec-rails/issues/817
  #
  after do
    Rails.application.reload_routes!
  end

  it 'redirects to the new url when coming from an old uri' do
    ActionDispatch::Request.any_instance.stub(:fullpath).and_return redirect.old_path
    get :handle_unknown_url
    expect(response).to redirect_to redirect.new_url
  end

end
