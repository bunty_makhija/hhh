require 'spec_helper'

describe Admin::DashboardController do

  let(:user) { create :user }

  it 'does allow admins to access the dashboard' do
    user.add_role :admin
    sign_in user
    get :index
    expect(response).to render_template :index
  end

  it 'does not allow non-admins to access the dashboard' do
    sign_in user
    get :index
    should set_the_flash
    expect(response).to redirect_to new_user_session_path
  end

end
