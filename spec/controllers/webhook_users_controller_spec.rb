require 'spec_helper'

describe WebhookUsersController do
  let(:hs_res) { double("hs-response") }
  let(:res_body) { double("res-body") }
  let(:parser) { double("webhook-parser") }

  describe 'activate unsuccessfully because of bad token' do
    before do
      expect(HubspotterInterface::ReverseSyncToken).to receive(:retrieve_email)
        .with('bad_token').and_return(nil)
      get :activate, token: 'bad_token'
    end
    it { expect(response).to render_template 'activate_error' }
  end

  describe 'activate with valid token' do
    let(:token) { 'reverse_sync_token'}

    before do
      expect(HubspotterInterface::ReverseSyncToken).to receive(:retrieve_email)
        .with('reverse_sync_token').and_return('test@tablexi.com')
      expect(HubspotImport::HubspotClient).to \
        receive(:send).with("fetch_contact_with_email!", 'test@tablexi.com')
          .and_return(hs_res)
      expect(hs_res).to receive(:body).and_return(res_body)
      expect(res_body).to receive(:force_encoding).with("UTF-8")
        .and_return("response_body")
      expect(HubspotterInterface::WebhookParser).to receive(:new)
        .with("response_body").and_return(parser)
    end

    context 'if create user failed' do
      before do
        expect(parser).to receive(:parse_and_create_user).and_return(nil)
        get :activate, token: token
      end

      it { expect(response).to render_template 'activate_error' }
    end

    context 'if create user was successful' do
      let(:user) { create :user, email: 'test@tablexi.com' }

      before do
        expect(parser).to receive(:parse_and_create_user).and_return(user)
        expect(user).to receive(:send_reset_password_instructions)
        get :activate, token: token
      end

      it { expect(response).to render_template 'activate_success' }
    end
  end

  context 'POST to register fails unless the HTTP referrer domain is hardhathub.net or hardhathub.com' do
    before do
      allow(HubspotImport::HubspotClient).to receive(:send)
      request.env['HTTP_REFERER'] = "http://www.something.else/hardhathub.com/hardhathub.net"
      post :register_from_post, {email: "bad@bad.com", pageId: "xxxxxxxx-1111-2222-3333-yyyyyyyyyyyy"}
    end
    it { expect(HubspotImport::HubspotClient).to_not have_received(:send) }
    it { expect(response.status).to eq 400 }
    it { expect(response.body).to be_blank }
  end

  context 'POST to register fails because of the pageId parameter' do
    before do
      allow(HubspotImport::HubspotClient).to receive(:send)
      request.env['HTTP_REFERER'] = "http://www.hardhathub.net"
    end

    context 'is not present' do
      before { post :register_from_post }
      it { expect(HubspotImport::HubspotClient).to_not have_received(:send) }
      it { expect(response.status).to eq 400 }
      it { expect(response.body).to be_blank }
    end

    context 'has the wrong format' do
      before { post :register_from_post, {email: "good@good.com", pageId: "bad-format"} }
      it { expect(HubspotImport::HubspotClient).to_not have_received(:send) }
      it { expect(response.status).to eq 400 }
      it { expect(response.body).to be_blank }
    end
  end

  describe 'valid POST to register a brand new user' do
    before do
      request.env['HTTP_REFERER'] = 'http://jobs.hardhathub.net'
      post :register_from_post, {email: "test@email.com",
        first_name: "First",
        last_name: "Last",
        postal_code: "60606",
        which_industry_are_you_in: "Buildings & Facilities",
        pageId: "xxxxxxxx-1111-2222-3333-yyyyyyyyyyyy"}
    end

    context 'and creates a new user' do
      subject { controller.current_user }
      its(:first_name) { should eq 'First' }
      its(:last_name) { should eq 'Last' }
      its(:email) { should eq 'test@email.com' }
      its(:password) { should_not be_nil }
      its(:password_confirmation) { should_not be_nil }
      it { expect(subject.professional_profile.zip_code).to eq '60606' }
      it { expect(subject.professional_profile.building).to be_true }
    end
  end

  describe 'valid POST to register an already created user' do
    let(:user) { create :user, email: "test@email.com" }

    before do
      user.save
      request.env['HTTP_REFERER'] = 'http://www.hardhathub.com'
    end

    context 'who has not completed the profile' do
      before do
        post :register_from_post, {email: "test@email.com",
          pageId: "xxxxxxxx-1111-2222-3333-yyyyyyyyyyyy"}
      end
      it { expect(response).to redirect_to get_started_index_path }
      it "should be logged in" do
        expect(controller.current_user).to eq user
      end
    end

    context 'who has completed the profile' do
      before do
        user.professional_profile.update_attribute :wizard_completed_at, Time.current
        post :register_from_post, {email: "test@email.com",
          pageId: "xxxxxxxx-1111-2222-3333-yyyyyyyyyyyy"}
      end
      it { expect(response).to redirect_to new_user_session_path }
      it "should not be logged in" do
        expect(controller.current_user).to be_nil
      end
    end
  end

  context 'register without a hubspot cookie' do
    before { get :register }
    it { expect(response).to redirect_to root_path }
    it { expect(request.flash[:error]).to eq "We could not complete your registration process, please <a href='mailto:admin@hardhathub.com'>contact us</a>" }
  end

  context 'register with a hubspot cookie' do
    before do
      request.cookies[:hubspotutk] = "hubspot_cookie"
      expect(HubspotImport::HubspotClient).to \
        receive(:send).with("fetch_contact_with_cookie!", "hubspot_cookie")
          .and_return(hs_res)
      expect(hs_res).to receive(:body).and_return(res_body)
    end

    context 'that is invalid' do
      before do
        expect(res_body).to receive(:force_encoding).with("UTF-8")
          .and_return(nil)
        get :register
      end
      it { expect(response).to redirect_to root_path }
    end

    context 'that is valid' do
      before do
        expect(res_body).to receive(:force_encoding).with("UTF-8")
          .and_return("response_body")
        expect(HubspotterInterface::WebhookParser).to receive(:new)
          .with("response_body").and_return(parser)
      end

      context 'and a user is not created' do
        before do
          expect(parser).to receive(:parse_and_find_or_create_user).and_return(nil)
          get :register
        end
        it { expect(response).to redirect_to root_path }
      end

      context "and a user is found or created" do
        let(:user) { professional_profile.user }
        let(:professional_profile) { create(:professional_profile) }

        before do
          expect(parser).to receive(:parse_and_find_or_create_user).and_return(user)
          get :register
        end

        context 'and the user has not completed the profile' do
          let(:professional_profile) { create(:professional_profile, wizard_completed_at: nil) }
          it { expect(response).to redirect_to get_started_index_path }

          describe "the user" do
            it "should be logged in" do
              expect(controller.current_user).to eq user
            end
          end
        end

        context 'and the user has completed the profile' do
          let(:professional_profile) { create(:professional_profile, wizard_completed_at: Time.current) }
          it { expect(response).to redirect_to new_user_session_path }

          describe "the user" do
            it "should not be logged in" do
              expect(controller.current_user).to be_nil
            end
          end
        end
      end
    end
  end
end
