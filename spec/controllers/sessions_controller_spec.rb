require 'spec_helper'

describe SessionsController do

  let(:user) { create :user }

  before { @request.env["devise.mapping"] = Devise.mappings[:user] }

  it "does not submit form to hubspot if the email is not present" do
    post :create, user: { email: '', password: user.password }
    expect(HubspotterInterface::LoginForm).not_to receive(:new)
  end

  context 'when the email is present' do
    let(:login_form) { double("HubspotterInterface::LoginForm") }

    before do
      expect(HubspotterInterface::LoginForm).to receive(:new).and_return(login_form)
      expect(login_form).to receive(:submit)
    end

    it 'redirects professionals to wizard starting page on successful login' do
      post :create, user: { email: user.email, password: user.password }
      expect(response).to redirect_to get_started_index_path
    end

    it 'redirects admins to admin page on successful login' do
      user.add_role :admin
      post :create, user: { email: user.email, password: user.password }
      expect(response).to redirect_to admin_root_path
    end

    it 'redirects to stored location on successful login' do
      session['user_return_to'] = [user.professional_profile, user.professional_profile.travel_preference]
      post :create, user: { email: user.email, password: user.password }
      expect(response).to redirect_to [user.professional_profile, user.professional_profile.travel_preference]
    end
  end
end
