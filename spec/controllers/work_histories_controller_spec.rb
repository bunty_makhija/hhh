require "spec_helper"

describe WorkHistoriesController do
  let(:user) { create :user }

  include_context "logged in as user", :user

  describe "index" do
    before(:each) do
      get :index, professional_profile_id: user.professional_profile.id
    end

    it "renders the template" do
      expect(response).to render_template(:index)
    end
  end

  describe "create" do
    it "creates a new resource" do
      expect {
        post :create, professional_profile_id: user.professional_profile.id
      }.to change { user.professional_profile.work_histories.count }.by(1)
    end
  end

  describe "update" do
    let(:work_history) { create :work_history, professional_profile: user.professional_profile }

    let(:from_date_params) do {
      'from_date(1i)' => '2009',
      'from_date(2i)' => '2',
      'from_date(3i)' => '1'
    } end

    let(:to_date_params) do {
      'to_date(1i)' => '2012',
      'to_date(2i)' => '6',
      'to_date(3i)' => '1'
    } end

    let(:params) do {
      company: Faker::Company.name,
      industry: IndustryHelper::TYPES.sample.to_s,
      industry_other: Faker::Commerce.department,
      current: true,
      position: Faker::Lorem.word,
      summary: Faker::Lorem.paragraph
    } end

    it "updates the resource" do
      patch :update,
        id: work_history.id,
        professional_profile_id: user.professional_profile.id,
        work_history: params.merge(from_date_params).merge(to_date_params)

      work_history.reload

      params.each do |attribute, value|
        expect(work_history.send(attribute)).to eq value
      end

      from_format = from_date_params.values.join '-'
      expect(work_history.from_date).to eq Date.strptime(from_format)

      to_format = to_date_params.values.join '-'
      expect(work_history.to_date).to eq Date.strptime(to_format)
    end
  end

  describe "destroy" do
    it "removes the resource" do
      temp_history = create :work_history, professional_profile: user.professional_profile

      expect {
        delete :destroy, id: temp_history.id, professional_profile_id: user.professional_profile.id
      }.to change { user.professional_profile.work_histories.count }.by(-1)
    end
  end
end
