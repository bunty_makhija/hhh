require "spec_helper"

describe EducationHistoriesController do
  let(:user) { create :user }

  include_context "logged in as user", :user

  describe "index" do
    it "renders the template" do
      get :index, professional_profile_id: user.professional_profile.id
      expect(response).to render_template(:index)
    end
  end

  describe "create" do
    it "creates a new resource" do
      expect {
        post :create, professional_profile_id: user.professional_profile.id
      }.to change { user.professional_profile.education_histories.count }.by(1)
    end
  end

  describe "update" do
    let(:education_history) { create :education_history, professional_profile: user.professional_profile }

    let(:grad_date_params) do {
      'graduation_date(1i)' => '2014',
      'graduation_date(2i)' => '6',
      'graduation_date(3i)' => '1'
    } end

    let(:history_params) do {
      achievement_level: EducationHistory::ACHIEVEMENT_LEVELS.sample.to_s,
      institution: Faker::Lorem.name
    } end

    let(:params) do {
      id: education_history.id,
      professional_profile_id: user.professional_profile.id,
      education_history: history_params.merge(grad_date_params)
    } end


    it "updates the resource" do
      patch :update, params
      education_history.reload

      history_params.each do |attribute, value|
        expect(education_history.send(attribute)).to eq value
      end

      date_format = grad_date_params.values.join '-'
      expect(education_history.graduation_date).to eq Date.strptime(date_format)
    end

    context "with concentration tags" do
      include_context "tags", 10

      it "adds concentration tags to the entry" do
        params[:education_history][:concentration_list] = ActsAsTaggableOn::Tag.concentrations.limit(5).pluck(:name)
        patch :update, params
        expect(education_history.reload.concentration_list).to eq params[:education_history][:concentration_list]
      end
    end
  end

  describe "destroy" do
    it "removes the resource" do
      temp_history = create :education_history, professional_profile: user.professional_profile

      expect {
        delete :destroy, id: temp_history.id, professional_profile_id: user.professional_profile.id
      }.to change { user.professional_profile.education_histories.count }.by(-1)
    end
  end
end
