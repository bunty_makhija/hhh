require "spec_helper"

describe GetStartedController do
  let(:user) { create :user }

  let!(:facilities_category) { FactoryGirl.create(:category, key: 'facilities_management') }
  let!(:facilities_project_type) { FactoryGirl.create(:project_type, key: 'buildings_facilities') }

  include_context "logged in as user", :user

  describe "#index" do
    it "redirects to the wizard starting page for this user" do
      get :index

      expect(response).to redirect_to get_started_path(:welcome)
    end
  end

  describe "#show" do
    it "allows access if a correct slug is defined" do
      get :show, id: :welcome

      expect(response.status).to eq 200
      expect(response).to render_template(:welcome)
    end

    it "raises invalid step error with an incorrect slug" do
      expect { get :show, id: :arceus }.to raise_error(Wicked::Wizard::InvalidStepError)
    end
  end

  describe "#update" do
    it "moves backwards through the wizard steps" do
      post :update, id: :function, submit_prev: :Previous, get_started_function_step: {}

      expect(response).to redirect_to(get_started_path(:industry))
    end

    it "moves forwards through the wizard steps" do
      post :update, id: :welcome

      expect(response).to render_template(:welcome)
    end

    context "move backwards after set password" do
      before do
        post :update, id: :set_password, submit_prev: :Previous, get_started_set_password_step: { password: "my-password", password_confirmation: "my-password"}
      end

      it { expect(response).to redirect_to get_started_path(:last_thing) }
    end

    context "move forwards from set password" do
      before do
        post :update, id: :set_password, get_started_set_password_step: { password: "my-password", password_confirmation: "my-password" }
      end

      it { expect(response).to redirect_to get_started_path(:next_steps) }
    end
  end
end
