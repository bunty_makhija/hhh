require "spec_helper"

describe ProfessionalProfilesController do
  include_context "logged in as user", :user

  let(:user) { create :user }
  let(:professional_profile) { user.professional_profile }
  let(:engagement_preference) { professional_profile.engagement_preference }
  let(:original_status) { ProfessionalProfile.status.values.first }
  let(:new_status) { ProfessionalProfile.status.values.last }

  let(:start_date_params) do {
    'starting_date(1i)' => '2014',
    'starting_date(2i)' => '6',
    'starting_date(3i)' => '1'
  } end

  let(:engage_pref_params) do {
    term_quick: true,
    term_small: true,
    term_large: true,
    term_permanent: true,
    term_various: true,
    other_concerns: Faker::Lorem.paragraph
  } end

  describe "updating the professional profile" do
    before do
      professional_profile.update(status: original_status)
      expect(professional_profile.reload.status).to eq original_status

      patch(
        :update,
        id: professional_profile.id,
        professional_profile_form: {
          status: new_status,
          engagement_preference_attributes: start_date_params.merge(engage_pref_params)
        }
      )
    end

    it 'updates the status' do
      expect(professional_profile.reload.status).to eq new_status
    end

    it "updates the engagement preferences" do
      engage_pref_params.each do |attribute, value|
        expect(engagement_preference.send(attribute)).to eq value
      end
    end

    it 'updates the engagement preference start date' do
      date_format = start_date_params.values.join '-'
      expect(engagement_preference.starting_date).to eq Date.strptime(date_format)
    end

    describe '#resume' do
      context 'when no resume is present' do
        before { get :resume, professional_profile_id: professional_profile.id }
        it { expect(response.body).to eq " " }
        it { expect(response.status).to be_true }
      end

      context 'when the resume is present' do
        before do
          professional_profile.update_attribute :resume_original_filename, "manual.pdf"
        end

        it "returns a file with original filename" do
          expect(@controller).to receive(:current_user).and_return(user)
          expect(professional_profile).to receive("resume?").and_return(true)
          expect(@controller).to receive(:send_data).with("this is my resume", {filename: "manual.pdf"})
          expect(professional_profile.resume).to receive(:read).and_return("this is my resume")
          expect(@controller).to receive(:render)
          get :resume, professional_profile_id: professional_profile.id
        end
      end
    end

    describe '#send_confirmation' do
      include_context "with clear deliveries array"

      let(:user) { create :unconfirmed_user }

      before { get :send_confirmation, professional_profile_id: professional_profile.id }

      it 'sends a confirmation email' do
        expect(Devise::Mailer).not_to receive(:confirmation_instructions)
        expect(deliveries.length).to eq 1
        expect(deliveries.last.to).to eq [user.email]
      end
    end
  end

  describe "visiting the professional profile" do

    context "without completing the wizard" do
      it "should redirect to wizard" do
        get :edit, id: professional_profile.id
        expect(response).to redirect_to get_started_path(:welcome)
      end
    end

    context "with a complete wizard" do
      it "should continue to the profile page" do
        professional_profile.update(wizard_completed_at: 5.minutes.ago)
        get :edit, id: professional_profile.id
        expect(response).to render_template :edit
      end
    end
  end
end
