require "spec_helper"

describe SchedulePreferencesController do
  let(:user) { create :user }
  let(:schedule_preference) { user.professional_profile.schedule_preference }

  include_context "logged in as user", :user

  include_examples "resource actions", :schedule_preference, %i(edit)
  include_examples "professional profile component controller"

  describe "update" do
    it "updates the resource" do
      opp_val = !schedule_preference.full_time
      get :update,
        id: schedule_preference.id,
        professional_profile_id: user.professional_profile.id,
        schedule_preference: {full_time: opp_val}
      expect(schedule_preference.reload.full_time).to eq opp_val
    end
  end
end
