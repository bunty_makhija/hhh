require "spec_helper"

describe SalaryPreferencesController do
  let(:user) { create :user }
  let(:salary_preference) { user.professional_profile.salary_preference }

  include_context "logged in as user", :user

  include_examples "resource actions", :salary_preference, %i(edit)
  include_examples "professional profile component controller"

  describe "update" do
    it "updates the resource" do
      new_salary = "An arm and a leg"
      get :update,
        id: salary_preference.id,
        professional_profile_id: user.professional_profile.id,
        salary_preference: {base_salary: new_salary}
      expect(salary_preference.reload.base_salary).to eq new_salary
    end
  end
end
