source "https://rubygems.org"

gem "rails", "4.0.5"
gem "pg"

gem "activeadmin", github: "gregbell/active_admin"
gem "aws-ses"
gem "cancan"
gem "daemons-rails"
gem "delayed_job_active_record"
gem "devise"
gem "eye"
gem "fog"
gem "hubspotter", github: "tablexi/hubspotter"
gem "jbuilder", "~> 1.2"
gem "newrelic_rpm"
gem "rails_config"
gem "turbolinks"
gem "omniauth-linkedin-oauth2"
gem "linkedin"
gem "sitemap_generator"
gem "whenever", require: false
# prevent fog warning
# http://stackoverflow.com/questions/19666226/warning-with-fog-and-aws-unable-to-load-the-unf-gem
gem "unf"
gem "wicked"

## deployment
gem "capistrano", require: false
gem "capistrano-bundler", require: false
gem "capistrano-rails", require: false
gem "capistrano-rvm", require: false
gem "capistrano3-unicorn", "~> 0.1.0", require: false
gem "eye-patch", require: false
gem "unicorn", require: false

## views
gem "haml-rails"
gem "simple_form"
gem "text_helpers"
gem "high_voltage"
gem "tinymce-rails"
gem "placeholder-gem"
gem "nokogiri"

# style
gem "sass-rails", "~> 4.0.0"
gem "autoprefixer-rails"
gem "foundation-rails", github: "rpontes/foundation-rails"
gem "compass-rails"
gem "sprockets-media_query_combiner"
gem "scut"

# email
gem "mail_view", "~> 2.0.4"
gem "premailer-rails"

# behavior
gem "coffee-rails", "~> 4.0.0"
gem "uglifier", ">= 1.3.0"
gem "jquery-rails"
gem "jquery-ui-rails"
gem "jquery-turbolinks"
gem "chosen-rails"
gem "therubyracer"

# models
gem "phonelib"
gem "enumerize"
gem "rolify", github: "EppO/rolify"
gem "acts-as-taggable-on"
gem "carrierwave"
gem "carmen-rails"
gem "friendly_id"

# controllers
gem "inherited_resources"

group :development do
  gem "annotate"
  gem "better_errors"
  gem "binding_of_caller"
  gem "quiet_assets"
end

group :test do
  gem "rspec-rails"
  gem "shoulda-matchers"
  gem "vcr"
  gem "webmock"
end

group :development, :test do
  gem "factory_girl_rails"
  gem "faker"
  gem "pry"
  gem "pry-rails"
  gem "awesome_print"
  gem "capybara"
end

group :doc do
  gem "sdoc", require: false
end
