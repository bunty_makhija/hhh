Hardhathub::Application.routes.draw do
  # https://github.com/basecamp/mail_view#routing
  if Rails.env.development?
    mount MailPreview => 'mail_view'
  end

  devise_for :users, controllers: { sessions: :sessions, confirmations: :confirmations, registrations: :registrations, omniauth_callbacks: :omniauth_callbacks }
  ActiveAdmin.routes(self)

  root to: 'audience_types#index'

  resources :audience_types, only: %i(index show)

  resources :professional_profiles, only: %i(edit update) do
    get '/resume' => 'professional_profiles#resume'
    get '/send_confirmation' => 'professional_profiles#send_confirmation'
    resources :travel_preferences, only: %i(edit update)
    resources :schedule_preferences, only: %i(edit update)
    resources :salary_preferences, only: %i(edit update)
    resources :qualifications, only: %i(edit update)
    resources :work_histories, only: %i(index create update destroy)
    resources :education_histories, only: %i(index create update destroy)
  end

  resources :company_profiles, only: %i(index show)

  resources :get_started

  # hubspot reverse sync calls
  resources :webhook_users, only: %i(create)
  get '/activate/:token' => 'webhook_users#activate', as: :activate
  get '/wizard' => 'webhook_users#register'
  post '/wizard' => 'webhook_users#register_from_post'

  get '/pages/*id' => 'high_voltage/pages#show', as: :page, format: false

  # redirects
  get '*path' => 'application#handle_unknown_url'
end

# == Route Map (Updated 2014-11-25 07:46)
#
#                                        Prefix Verb     URI Pattern                                                                             Controller#Action
#                                                        /mail_view                                                                              MailPreview
#                              new_user_session GET      /users/sign_in(.:format)                                                                sessions#new
#                                  user_session POST     /users/sign_in(.:format)                                                                sessions#create
#                          destroy_user_session DELETE   /users/sign_out(.:format)                                                               sessions#destroy
#                       user_omniauth_authorize GET|POST /users/auth/:provider(.:format)                                                         omniauth_callbacks#passthru {:provider=>/linkedin/}
#                        user_omniauth_callback GET|POST /users/auth/:action/callback(.:format)                                                  omniauth_callbacks#(?-mix:linkedin)
#                                 user_password POST     /users/password(.:format)                                                               devise/passwords#create
#                             new_user_password GET      /users/password/new(.:format)                                                           devise/passwords#new
#                            edit_user_password GET      /users/password/edit(.:format)                                                          devise/passwords#edit
#                                               PATCH    /users/password(.:format)                                                               devise/passwords#update
#                                               PUT      /users/password(.:format)                                                               devise/passwords#update
#                      cancel_user_registration GET      /users/cancel(.:format)                                                                 registrations#cancel
#                             user_registration POST     /users(.:format)                                                                        registrations#create
#                         new_user_registration GET      /users/sign_up(.:format)                                                                registrations#new
#                        edit_user_registration GET      /users/edit(.:format)                                                                   registrations#edit
#                                               PATCH    /users(.:format)                                                                        registrations#update
#                                               PUT      /users(.:format)                                                                        registrations#update
#                                               DELETE   /users(.:format)                                                                        registrations#destroy
#                             user_confirmation POST     /users/confirmation(.:format)                                                           confirmations#create
#                         new_user_confirmation GET      /users/confirmation/new(.:format)                                                       confirmations#new
#                                               GET      /users/confirmation(.:format)                                                           confirmations#show
#                                    admin_root GET      /admin(.:format)                                                                        admin/dashboard#index
#          batch_action_admin_audience_profiles POST     /admin/audience_profiles/batch_action(.:format)                                         admin/audience_profiles#batch_action
#                       admin_audience_profiles GET      /admin/audience_profiles(.:format)                                                      admin/audience_profiles#index
#                                               POST     /admin/audience_profiles(.:format)                                                      admin/audience_profiles#create
#                    new_admin_audience_profile GET      /admin/audience_profiles/new(.:format)                                                  admin/audience_profiles#new
#                   edit_admin_audience_profile GET      /admin/audience_profiles/:id/edit(.:format)                                             admin/audience_profiles#edit
#                        admin_audience_profile GET      /admin/audience_profiles/:id(.:format)                                                  admin/audience_profiles#show
#                                               PATCH    /admin/audience_profiles/:id(.:format)                                                  admin/audience_profiles#update
#                                               PUT      /admin/audience_profiles/:id(.:format)                                                  admin/audience_profiles#update
#                                               DELETE   /admin/audience_profiles/:id(.:format)                                                  admin/audience_profiles#destroy
#             batch_action_admin_audience_types POST     /admin/audience_types/batch_action(.:format)                                            admin/audience_types#batch_action
#                          admin_audience_types GET      /admin/audience_types(.:format)                                                         admin/audience_types#index
#                                               POST     /admin/audience_types(.:format)                                                         admin/audience_types#create
#                       new_admin_audience_type GET      /admin/audience_types/new(.:format)                                                     admin/audience_types#new
#                      edit_admin_audience_type GET      /admin/audience_types/:id/edit(.:format)                                                admin/audience_types#edit
#                           admin_audience_type GET      /admin/audience_types/:id(.:format)                                                     admin/audience_types#show
#                                               PATCH    /admin/audience_types/:id(.:format)                                                     admin/audience_types#update
#                                               PUT      /admin/audience_types/:id(.:format)                                                     admin/audience_types#update
#                                               DELETE   /admin/audience_types/:id(.:format)                                                     admin/audience_types#destroy
#                 batch_action_admin_categories POST     /admin/categories/batch_action(.:format)                                                admin/categories#batch_action
#                              admin_categories GET      /admin/categories(.:format)                                                             admin/categories#index
#                                               POST     /admin/categories(.:format)                                                             admin/categories#create
#                            new_admin_category GET      /admin/categories/new(.:format)                                                         admin/categories#new
#                           edit_admin_category GET      /admin/categories/:id/edit(.:format)                                                    admin/categories#edit
#                                admin_category GET      /admin/categories/:id(.:format)                                                         admin/categories#show
#                                               PATCH    /admin/categories/:id(.:format)                                                         admin/categories#update
#                                               PUT      /admin/categories/:id(.:format)                                                         admin/categories#update
#                                               DELETE   /admin/categories/:id(.:format)                                                         admin/categories#destroy
#         batch_action_admin_category_functions POST     /admin/category_functions/batch_action(.:format)                                        admin/category_functions#batch_action
#                      admin_category_functions GET      /admin/category_functions(.:format)                                                     admin/category_functions#index
#                                               POST     /admin/category_functions(.:format)                                                     admin/category_functions#create
#                   new_admin_category_function GET      /admin/category_functions/new(.:format)                                                 admin/category_functions#new
#                  edit_admin_category_function GET      /admin/category_functions/:id/edit(.:format)                                            admin/category_functions#edit
#                       admin_category_function GET      /admin/category_functions/:id(.:format)                                                 admin/category_functions#show
#                                               PATCH    /admin/category_functions/:id(.:format)                                                 admin/category_functions#update
#                                               PUT      /admin/category_functions/:id(.:format)                                                 admin/category_functions#update
#                                               DELETE   /admin/category_functions/:id(.:format)                                                 admin/category_functions#destroy
#             batch_action_admin_certifications POST     /admin/certifications/batch_action(.:format)                                            admin/certifications#batch_action
#                          admin_certifications GET      /admin/certifications(.:format)                                                         admin/certifications#index
#                                               POST     /admin/certifications(.:format)                                                         admin/certifications#create
#                       new_admin_certification GET      /admin/certifications/new(.:format)                                                     admin/certifications#new
#                      edit_admin_certification GET      /admin/certifications/:id/edit(.:format)                                                admin/certifications#edit
#                           admin_certification PATCH    /admin/certifications/:id(.:format)                                                     admin/certifications#update
#                                               PUT      /admin/certifications/:id(.:format)                                                     admin/certifications#update
#                                               DELETE   /admin/certifications/:id(.:format)                                                     admin/certifications#destroy
#           batch_action_admin_company_profiles POST     /admin/company_profiles/batch_action(.:format)                                          admin/company_profiles#batch_action
#                        admin_company_profiles GET      /admin/company_profiles(.:format)                                                       admin/company_profiles#index
#                                               POST     /admin/company_profiles(.:format)                                                       admin/company_profiles#create
#                     new_admin_company_profile GET      /admin/company_profiles/new(.:format)                                                   admin/company_profiles#new
#                    edit_admin_company_profile GET      /admin/company_profiles/:id/edit(.:format)                                              admin/company_profiles#edit
#                         admin_company_profile GET      /admin/company_profiles/:id(.:format)                                                   admin/company_profiles#show
#                                               PATCH    /admin/company_profiles/:id(.:format)                                                   admin/company_profiles#update
#                                               PUT      /admin/company_profiles/:id(.:format)                                                   admin/company_profiles#update
#                                               DELETE   /admin/company_profiles/:id(.:format)                                                   admin/company_profiles#destroy
#                               admin_dashboard GET      /admin/dashboard(.:format)                                                              admin/dashboard#index
#          batch_action_admin_job_opportunities POST     /admin/job_opportunities/batch_action(.:format)                                         admin/job_opportunities#batch_action
#                       admin_job_opportunities GET      /admin/job_opportunities(.:format)                                                      admin/job_opportunities#index
#                                               POST     /admin/job_opportunities(.:format)                                                      admin/job_opportunities#create
#                     new_admin_job_opportunity GET      /admin/job_opportunities/new(.:format)                                                  admin/job_opportunities#new
#                    edit_admin_job_opportunity GET      /admin/job_opportunities/:id/edit(.:format)                                             admin/job_opportunities#edit
#                         admin_job_opportunity GET      /admin/job_opportunities/:id(.:format)                                                  admin/job_opportunities#show
#                                               PATCH    /admin/job_opportunities/:id(.:format)                                                  admin/job_opportunities#update
#                                               PUT      /admin/job_opportunities/:id(.:format)                                                  admin/job_opportunities#update
#                                               DELETE   /admin/job_opportunities/:id(.:format)                                                  admin/job_opportunities#destroy
#                batch_action_admin_metro_areas POST     /admin/metro_areas/batch_action(.:format)                                               admin/metro_areas#batch_action
#                             admin_metro_areas GET      /admin/metro_areas(.:format)                                                            admin/metro_areas#index
#                                               POST     /admin/metro_areas(.:format)                                                            admin/metro_areas#create
#                          new_admin_metro_area GET      /admin/metro_areas/new(.:format)                                                        admin/metro_areas#new
#                         edit_admin_metro_area GET      /admin/metro_areas/:id/edit(.:format)                                                   admin/metro_areas#edit
#                              admin_metro_area GET      /admin/metro_areas/:id(.:format)                                                        admin/metro_areas#show
#                                               PATCH    /admin/metro_areas/:id(.:format)                                                        admin/metro_areas#update
#                                               PUT      /admin/metro_areas/:id(.:format)                                                        admin/metro_areas#update
#                                               DELETE   /admin/metro_areas/:id(.:format)                                                        admin/metro_areas#destroy
#      batch_action_admin_professional_profiles POST     /admin/professional_profiles/batch_action(.:format)                                     admin/professional_profiles#batch_action
#                   admin_professional_profiles GET      /admin/professional_profiles(.:format)                                                  admin/professional_profiles#index
#                                               POST     /admin/professional_profiles(.:format)                                                  admin/professional_profiles#create
#                new_admin_professional_profile GET      /admin/professional_profiles/new(.:format)                                              admin/professional_profiles#new
#               edit_admin_professional_profile GET      /admin/professional_profiles/:id/edit(.:format)                                         admin/professional_profiles#edit
#                    admin_professional_profile GET      /admin/professional_profiles/:id(.:format)                                              admin/professional_profiles#show
#                                               PATCH    /admin/professional_profiles/:id(.:format)                                              admin/professional_profiles#update
#                                               PUT      /admin/professional_profiles/:id(.:format)                                              admin/professional_profiles#update
#                                               DELETE   /admin/professional_profiles/:id(.:format)                                              admin/professional_profiles#destroy
#              batch_action_admin_project_types POST     /admin/project_types/batch_action(.:format)                                             admin/project_types#batch_action
#                           admin_project_types GET      /admin/project_types(.:format)                                                          admin/project_types#index
#                                               POST     /admin/project_types(.:format)                                                          admin/project_types#create
#                        new_admin_project_type GET      /admin/project_types/new(.:format)                                                      admin/project_types#new
#                       edit_admin_project_type GET      /admin/project_types/:id/edit(.:format)                                                 admin/project_types#edit
#                            admin_project_type GET      /admin/project_types/:id(.:format)                                                      admin/project_types#show
#                                               PATCH    /admin/project_types/:id(.:format)                                                      admin/project_types#update
#                                               PUT      /admin/project_types/:id(.:format)                                                      admin/project_types#update
#                                               DELETE   /admin/project_types/:id(.:format)                                                      admin/project_types#destroy
#                     batch_action_admin_skills POST     /admin/skills/batch_action(.:format)                                                    admin/skills#batch_action
#                                  admin_skills GET      /admin/skills(.:format)                                                                 admin/skills#index
#                                               POST     /admin/skills(.:format)                                                                 admin/skills#create
#                               new_admin_skill GET      /admin/skills/new(.:format)                                                             admin/skills#new
#                              edit_admin_skill GET      /admin/skills/:id/edit(.:format)                                                        admin/skills#edit
#                                   admin_skill PATCH    /admin/skills/:id(.:format)                                                             admin/skills#update
#                                               PUT      /admin/skills/:id(.:format)                                                             admin/skills#update
#                                               DELETE   /admin/skills/:id(.:format)                                                             admin/skills#destroy
#          batch_action_admin_sub_project_types POST     /admin/sub_project_types/batch_action(.:format)                                         admin/sub_project_types#batch_action
#                       admin_sub_project_types GET      /admin/sub_project_types(.:format)                                                      admin/sub_project_types#index
#                                               POST     /admin/sub_project_types(.:format)                                                      admin/sub_project_types#create
#                    new_admin_sub_project_type GET      /admin/sub_project_types/new(.:format)                                                  admin/sub_project_types#new
#                   edit_admin_sub_project_type GET      /admin/sub_project_types/:id/edit(.:format)                                             admin/sub_project_types#edit
#                        admin_sub_project_type GET      /admin/sub_project_types/:id(.:format)                                                  admin/sub_project_types#show
#                                               PATCH    /admin/sub_project_types/:id(.:format)                                                  admin/sub_project_types#update
#                                               PUT      /admin/sub_project_types/:id(.:format)                                                  admin/sub_project_types#update
#                                               DELETE   /admin/sub_project_types/:id(.:format)                                                  admin/sub_project_types#destroy
#                      batch_action_admin_users POST     /admin/users/batch_action(.:format)                                                     admin/users#batch_action
#                                   admin_users GET      /admin/users(.:format)                                                                  admin/users#index
#                                               POST     /admin/users(.:format)                                                                  admin/users#create
#                                new_admin_user GET      /admin/users/new(.:format)                                                              admin/users#new
#                               edit_admin_user GET      /admin/users/:id/edit(.:format)                                                         admin/users#edit
#                                    admin_user GET      /admin/users/:id(.:format)                                                              admin/users#show
#                                               PATCH    /admin/users/:id(.:format)                                                              admin/users#update
#                                               PUT      /admin/users/:id(.:format)                                                              admin/users#update
#                                               DELETE   /admin/users/:id(.:format)                                                              admin/users#destroy
#                                          root GET      /                                                                                       audience_types#index
#                                audience_types GET      /audience_types(.:format)                                                               audience_types#index
#                                 audience_type GET      /audience_types/:id(.:format)                                                           audience_types#show
#                   professional_profile_resume GET      /professional_profiles/:professional_profile_id/resume(.:format)                        professional_profiles#resume
#        professional_profile_send_confirmation GET      /professional_profiles/:professional_profile_id/send_confirmation(.:format)             professional_profiles#send_confirmation
#   edit_professional_profile_travel_preference GET      /professional_profiles/:professional_profile_id/travel_preferences/:id/edit(.:format)   travel_preferences#edit
#        professional_profile_travel_preference PATCH    /professional_profiles/:professional_profile_id/travel_preferences/:id(.:format)        travel_preferences#update
#                                               PUT      /professional_profiles/:professional_profile_id/travel_preferences/:id(.:format)        travel_preferences#update
# edit_professional_profile_schedule_preference GET      /professional_profiles/:professional_profile_id/schedule_preferences/:id/edit(.:format) schedule_preferences#edit
#      professional_profile_schedule_preference PATCH    /professional_profiles/:professional_profile_id/schedule_preferences/:id(.:format)      schedule_preferences#update
#                                               PUT      /professional_profiles/:professional_profile_id/schedule_preferences/:id(.:format)      schedule_preferences#update
#   edit_professional_profile_salary_preference GET      /professional_profiles/:professional_profile_id/salary_preferences/:id/edit(.:format)   salary_preferences#edit
#        professional_profile_salary_preference PATCH    /professional_profiles/:professional_profile_id/salary_preferences/:id(.:format)        salary_preferences#update
#                                               PUT      /professional_profiles/:professional_profile_id/salary_preferences/:id(.:format)        salary_preferences#update
#       edit_professional_profile_qualification GET      /professional_profiles/:professional_profile_id/qualifications/:id/edit(.:format)       qualifications#edit
#            professional_profile_qualification PATCH    /professional_profiles/:professional_profile_id/qualifications/:id(.:format)            qualifications#update
#                                               PUT      /professional_profiles/:professional_profile_id/qualifications/:id(.:format)            qualifications#update
#           professional_profile_work_histories GET      /professional_profiles/:professional_profile_id/work_histories(.:format)                work_histories#index
#                                               POST     /professional_profiles/:professional_profile_id/work_histories(.:format)                work_histories#create
#             professional_profile_work_history PATCH    /professional_profiles/:professional_profile_id/work_histories/:id(.:format)            work_histories#update
#                                               PUT      /professional_profiles/:professional_profile_id/work_histories/:id(.:format)            work_histories#update
#                                               DELETE   /professional_profiles/:professional_profile_id/work_histories/:id(.:format)            work_histories#destroy
#      professional_profile_education_histories GET      /professional_profiles/:professional_profile_id/education_histories(.:format)           education_histories#index
#                                               POST     /professional_profiles/:professional_profile_id/education_histories(.:format)           education_histories#create
#        professional_profile_education_history PATCH    /professional_profiles/:professional_profile_id/education_histories/:id(.:format)       education_histories#update
#                                               PUT      /professional_profiles/:professional_profile_id/education_histories/:id(.:format)       education_histories#update
#                                               DELETE   /professional_profiles/:professional_profile_id/education_histories/:id(.:format)       education_histories#destroy
#                     edit_professional_profile GET      /professional_profiles/:id/edit(.:format)                                               professional_profiles#edit
#                          professional_profile PATCH    /professional_profiles/:id(.:format)                                                    professional_profiles#update
#                                               PUT      /professional_profiles/:id(.:format)                                                    professional_profiles#update
#                              company_profiles GET      /company_profiles(.:format)                                                             company_profiles#index
#                               company_profile GET      /company_profiles/:id(.:format)                                                         company_profiles#show
#                             get_started_index GET      /get_started(.:format)                                                                  get_started#index
#                                               POST     /get_started(.:format)                                                                  get_started#create
#                               new_get_started GET      /get_started/new(.:format)                                                              get_started#new
#                              edit_get_started GET      /get_started/:id/edit(.:format)                                                         get_started#edit
#                                   get_started GET      /get_started/:id(.:format)                                                              get_started#show
#                                               PATCH    /get_started/:id(.:format)                                                              get_started#update
#                                               PUT      /get_started/:id(.:format)                                                              get_started#update
#                                               DELETE   /get_started/:id(.:format)                                                              get_started#destroy
#                                 webhook_users POST     /webhook_users(.:format)                                                                webhook_users#create
#                                      activate GET      /activate/:token(.:format)                                                              webhook_users#activate
#                                        wizard GET      /wizard(.:format)                                                                       webhook_users#wizard
#                                          page GET      /pages/*id                                                                              high_voltage/pages#show
#                                               GET      /*path(.:format)                                                                        application#handle_unknown_url
#
