# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'hardhathub'
set :repo_url, 'git@github.com:tablexi/hardhathub.git'
set :stages, %w(prod staging release-target)

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/settings.yml config/eye.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/sitemaps}

set :eye_env, -> {{ rails_env: fetch(:rails_env) }}

after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end
end

set :whenever_command,      ->{ [:bundle, :exec, :whenever] }
set :whenever_environment,  ->{ fetch :rails_env }
set :whenever_identifier,   ->{ fetch :application }
