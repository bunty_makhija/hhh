# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.hardhathub.com"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do

  # Sign-in / Sign-up
  add(new_user_registration_path, priority: 0.9, changefreq: "monthly")
  add(new_user_session_path,      priority: 0.5, changefreq: "monthly")

  # Company Profile Pages
  add(company_profiles_path, priority: 0.7, changefreq: "weekly")

  CompanyProfile.all.each do |co|
    add(company_profile_path(co), priority: 0.7, changefreq: "monthly")
  end

  # Audience Types
  add(audience_types_path, priority: 0.5, changefreq: "monthly")

  AudienceType.all.each do |audience|
    add(audience_type_path(audience), priority: 0.5, changefreq: "monthly")
  end

  # Static Pages created through High Voltage
  pages = Dir.entries(Rails.root.join('app', 'views', 'pages')).select{|p| !p.match(/^(\.|\..)$/)}.map do |f|
    f.sub(/\.html\.[a-z]+$/,"")
  end

  pages.each do |page|
    add("/pages/#{page}", priority: 0.5, changefreq: "monthly")
  end


  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
end
