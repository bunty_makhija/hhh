ActionMailer::Base.add_delivery_method :ses, AWS::SES::Base,
  :access_key_id      => Settings.send(Rails.env).aws_access_key,
  :secret_access_key  => Settings.send(Rails.env).aws_secret_access_key

ActionMailer::Base.register_interceptor(MailInterceptor) if Rails.env.stage?
