if Rails.env.development?
  BetterErrors.use_pry!
  BetterErrors.editor = :sublime
  BetterErrors::Middleware.allow_ip! ENV['TRUSTED_IP'] if ENV['TRUSTED_IP']
end
