# Further configs at https://github.com/mbleigh/acts-as-taggable-on#configuration

ActsAsTaggableOn::Tag.send :include, TagContext

class ActsAsTaggableOn::Tag
  validates :name, uniqueness: { scope: :original_context }

  # ActsAsTaggableOn requires this duck punch to remove unique validation on names
  def validates_name_uniqueness?
    false
  end
end
