Hubspotter.configure do |config|
  config.api_key = Settings.hubspot.api_key
  config.portal_id = Settings.hubspot.portal_id
end
