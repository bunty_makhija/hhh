CarrierWave.configure do |config|

  config.enable_processing = false if Rails.env.test?
  config.fog_directory = Settings.send(Rails.env).aws_bucket
  config.fog_public = false
  config.fog_credentials = {
    provider: 'AWS',
    aws_access_key_id: Settings.send(Rails.env).aws_access_key,
    aws_secret_access_key: Settings.send(Rails.env).aws_secret_access_key,
  }

end
