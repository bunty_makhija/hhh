#
# Don't let high voltage draw its routes.
# We'll do that for high voltage in our routes.rb
# so we can make sure HV routes come before our catchall
HighVoltage.configure do |config|
  config.routes = false
end
