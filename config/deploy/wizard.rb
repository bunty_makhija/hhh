set :deploy_to, '/home/hardhathub/hardhathub.stage.tablexi.com'
set :rails_env, 'stage'
set :branch, 'wizard'

server 'ec2-shared.txihosting.com', user: 'hardhathub', roles: %w(web app db)
