set :deploy_to, '/home/hardhathub/www.hardhathub.com'
set :rails_env, 'production'
set :branch, 'master'
set :deploy_via, :export

server 'hardhathub-app01.txihosting.com', user: 'hardhathub', roles: %w(web app db)
