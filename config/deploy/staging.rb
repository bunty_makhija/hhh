set :deploy_to, '/home/hardhathub/hardhathub.stage.tablexi.com'
set :rails_env, 'stage'
set :branch, ENV['branch'] || 'develop'

server 'ec2-shared.txihosting.com', user: 'hardhathub', roles: %w(web app db)
