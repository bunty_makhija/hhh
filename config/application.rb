require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Hardhathub
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set frameworks
    config.generators do |g|
      g.orm :active_record
      g.template_engine :haml
      g.test_framework :rspec
    end

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Load model subdirs
    config.autoload_paths += Dir[Rails.root.join('app', 'models', '**')]

    # Raise an exception if an invalid locale is referenced.
    config.i18n.enforce_available_locales = true

    # For use with text_helpers gem - https://github.com/ahorner/text-helpers#suggested-use
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]

    config.action_mailer.default_url_options = { :host => "hardhathub.com" }

    # compile email stylesheet by itself; don't add to concatenated application.css
    config.assets.precompile += %w(email.css)

    # Set email template for Devise mailers
    # https://github.com/plataformatec/devise/wiki/How-To%3a-Create-custom-layouts#define-in-config
    config.to_prepare do
      Devise::Mailer.layout "email" # email.haml or email.erb
    end
  end
end
