case @environment
when "production"
  every 14.days, at: '1:00 am' do
    rake "sitemap:refresh"
  end
else
  every 14.days, at: '1:00 am' do
    rake "sitemap:refresh:no_ping"
  end
end
