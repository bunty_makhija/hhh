/*
  Either tinymce-rails gem or tinymce itself outputs <script src="/javascripts/tinymce-jquery.js"></script>
  in the <head> when using precompiled assets. That's not necessary for tinymce to work for us, and I can't
  figure out how to stop it, so let's just satisfy the URL and be done.
 */
